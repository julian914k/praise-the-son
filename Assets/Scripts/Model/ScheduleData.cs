﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScheduleData
{
    public ScheduleData()
    {

    }
    public string scheduleName;
    public Day dayType;
}
public enum ScheduleName
{
    학교가기, 학원가기, 과외, 스터디그룹, 도서관,

    봉사활동,
    레스토랑, 카페, 편의점, 피시방, 패스트푸드, 전단지, 인형탈, 주유소, 택배상하차,


    집에서휴식, 친구만나기, 게임하기, 운동하기,
    영화보기, 그림그리기, 음악듣기, 독서하기,
    맛집탐방, 종교활동, 없음
}
public enum Day
{
    None,
    Morning,
    Evening,
    Night,
    Holiday
}