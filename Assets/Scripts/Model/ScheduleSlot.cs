﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScheduleSlot : MonoBehaviour
{
    public ScheduleName ScheduleName;
    public Image ScheduleImage;
    public Image TalentSlot1;
    public Image TalentSlot2;
}
