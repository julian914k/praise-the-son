﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BayatGames.SaveGameFree;

public class SonData
{
    [Header("Info")]
    public string Name;
    public int BirthMonth;
    public int BirthDay;
    public Job CurrentJob;
    public Dream CurrentDream;
    public Emotion CurrentEmotion = Emotion.평범함;

    [Header("Info Detail")]
    public bool MilitaryServiceStatus = false;

    [Header("Attributes")]
    public int Physical = 50;
    public int Intelligence = 50;
    public int Sensibility = 50;
    public int Craft = 50;
    public int Sociability = 50;
    public int Charm = 50;
    public int Notion = 50;

    [Header("Last Attributes")]
    public int LastPhysical;
    public int LastIntelligence;
    public int LastSensibility;
    public int LastCraft;
    public int LastSociability;
    public int LastCharm;
    public int LastNotion;

    [Header("Battery Attributes")]
    public int Intimacy = 10;
    public int Stress = 0;
    public int Wealth = 3;

    [Header("Rating Attributes")]
    public int Athletic = 0;
    public int Education = 0;
    public int Art = 0;
    public int Technic = 0;

    [Header("Traits")]
    public PersonalityTrait CurrentPersonalityTrait1 = PersonalityTrait.없음;
    public PersonalityTrait CurrentPersonalityTrait2 = PersonalityTrait.없음;
    public PersonalityTrait CurrentPersonalityTrait3 = PersonalityTrait.없음;
    public PersonalityTrait CurrentPersonalityTrait4 = PersonalityTrait.없음;

    [Header("Talent & Interest")]
    /** Talant & Interest Schedule Value [ 1: good, 0: normal, -1: bad ] */
    public Dictionary<ScheduleName, int> TalentSchedules;
    public Dictionary<ScheduleName, int> InterestSchedules;

    public SonData()
    {
        TalentSchedules = new Dictionary<ScheduleName, int>();
        InterestSchedules = new Dictionary<ScheduleName, int>();
    }
    public void SaveSonTalentAndInterest()
    {
        SaveGame.Save<Dictionary<ScheduleName, int>>("TalentSchedules", SonManager.SonData.TalentSchedules);
        SaveGame.Save<Dictionary<ScheduleName, int>>("InterestSchedules", SonManager.SonData.InterestSchedules);
    }
    public void SaveSonLastAttributes()
    {
        SaveGame.Save<int>("LastPhysical", Physical);
        SaveGame.Save<int>("LastIntelligence", Intelligence);
        SaveGame.Save<int>("LastSensibility", Sensibility);
        SaveGame.Save<int>("LastCraft", Craft);
        SaveGame.Save<int>("LastSociability", Sociability);
        SaveGame.Save<int>("LastCharm", Charm);
        SaveGame.Save<int>("LastNotion", Notion);
    }
    public void LoadSonLastAttributes()
    {
        LastPhysical = SaveGame.Load<int>("LastPhysical");
        LastIntelligence = SaveGame.Load<int>("LastIntelligence");
        LastSensibility = SaveGame.Load<int>("LastSensibility");
        LastCraft = SaveGame.Load<int>("LastCraft");
        LastSociability = SaveGame.Load<int>("LastSociability");
        LastCharm = SaveGame.Load<int>("LastCharm");
        LastNotion = SaveGame.Load<int>("LastNotion");
    }
    public void LimitedSonAllAttributes()
    {
        LimitedSonAttributes();
        LimitedSonBatteryAttributes();
        LimitedSonRatingAttributes();
    }
    public void LimitedSonAttributes()
    {
        if (Physical >= 2000)
        {
            Physical = 2000;
        }
        if (Intelligence >= 2000)
        {
            Intelligence = 2000;
        }
        if (Sensibility >= 2000)
        {
            Sensibility = 2000;
        }
        if (Craft >= 2000)
        {
            Craft = 2000;
        }
        if (Sociability >= 2000)
        {
            Sociability = 2000;
        }
        if (Charm >= 2000)
        {
            Charm = 2000;
        }
        if (Notion >= 2000)
        {
            Notion = 2000;
        }
    }
    public void LimitedSonBatteryAttributes()
    {
        if (Intimacy >= 1000)
        {
            Intimacy = 1000;
        }
        if (Stress >= 1000)
        {
            Stress = 1000;
        }
        if (Wealth >= 1000)
        {
            Wealth = 1000;
        }
    }
    public void LimitedSonRatingAttributes()
    {
        if (Athletic >= 20)
        {
            Athletic = 20;
        }
        if (Education >= 20)
        {
            Education = 20;
        }
        if (Art >= 20)
        {
            Art = 20;
        }
        if (Technic >= 20)
        {
            Technic = 20;
        }
    }
    public void IncreaseSonAttribute(string attribute, int amount)
    {
        switch (attribute)
        {
            case "Physical":
                Physical += amount;
                break;
            case "Intelligence":
                Intelligence += amount;
                break;
            case "Sensibility":
                Sensibility += amount;
                break;
            case "Craft":
                Craft += amount;
                break;
            case "Sociability":
                Sociability += amount;
                break;
            case "Charm":
                Charm += amount;
                break;
            case "Notion":
                Notion += amount;
                break;
            case "Intimacy":
                Intimacy += amount;
                break;
            case "Stress":
                Stress += amount;
                break;
            case "Wealth":
                Wealth += amount;
                break;
            case "Athletic":
                Athletic += amount;
                break;
            case "Education":
                Education += amount;
                break;
            case "Art":
                Art += amount;
                break;
            case "Technic":
                Technic += amount;
                break;
        }
    }

    public void SaveAllSonDatas()
    {
        SaveSonInfo();
        SaveSonInfoDetail();
        SaveSonAttributes();
        SaveSonEmotion();
        SaveSonPersonalityTrait();
    }
    public void LoadAllSonDatas()
    {
        LoadSonInfo();
        LoadSonInfoDetail();
        LoadSonAttributes();
        LoadSonEmotion();
        LoadSonPersonalityTrait();
    }
    public void SaveSonInfo()
    {
        SaveGame.Save<string>("SonName", Name);
        SaveGame.Save<int>("SonBirthMonth", BirthMonth);
        SaveGame.Save<int>("SonBirthDay", BirthDay);
        SaveGame.Save<string>("CurrentJob", CurrentJob.ToString());
        SaveGame.Save<string>("CurrentDream", CurrentDream.ToString());
    }
    public void SaveSonInfoDetail()
    {
        SaveGame.Save<bool>("MilitaryServiceStatus", MilitaryServiceStatus);
    }
    public void SaveSonAttributes()
    {
        LimitedSonAllAttributes();
        SaveGame.Save<int>("Physical", Physical);
        SaveGame.Save<int>("Intelligence", Intelligence);
        SaveGame.Save<int>("Sensibility", Sensibility);
        SaveGame.Save<int>("Craft", Craft);
        SaveGame.Save<int>("Sociability", Sociability);
        SaveGame.Save<int>("Charm", Charm);
        SaveGame.Save<int>("Notion", Notion);
        SaveGame.Save<int>("Intimacy", Intimacy);
        SaveGame.Save<int>("Stress", Stress);
        SaveGame.Save<int>("Wealth", Wealth);
        SaveGame.Save<int>("Athletic", Athletic);
        SaveGame.Save<int>("Education", Education);
        SaveGame.Save<int>("Art", Art);
        SaveGame.Save<int>("Technic", Technic);
    }
    public void SaveSonEmotion()
    {
        SaveGame.Save<string>("Emotion", CurrentEmotion.ToString());
    }
    public void SaveSonPersonalityTrait()
    {
        SaveGame.Save<PersonalityTrait>("PersonalityTrait1", CurrentPersonalityTrait1);
        SaveGame.Save<PersonalityTrait>("PersonalityTrait2", CurrentPersonalityTrait2);
        SaveGame.Save<PersonalityTrait>("PersonalityTrait3", CurrentPersonalityTrait3);
        SaveGame.Save<PersonalityTrait>("PersonalityTrait4", CurrentPersonalityTrait4);
    }
    public void LoadSonInfo()
    {
        Name = SaveGame.Load<string>("SonName");
        BirthMonth = SaveGame.Load<int>("SonBirthMonth");
        BirthDay = SaveGame.Load<int>("SonBirthDay");
        switch (SaveGame.Load<string>("CurrentJob"))
        {
            case "초등학생":
                CurrentJob = Job.초등학생;
                break;
            case "중학생":
                CurrentJob = Job.중학생;
                break;
            case "고등학생":
                CurrentJob = Job.고등학생;
                break;
            case "대학생":
                CurrentJob = Job.대학생;
                break;
            case "군인":
                CurrentJob = Job.군인;
                break;
            case "백수":
                CurrentJob = Job.백수;
                break;
            case "직장인":
                CurrentJob = Job.직장인;
                break;
        }
        switch (SaveGame.Load<string>("CurrentDream"))
        {
            case "가수":
                CurrentDream = Dream.가수;
                break;
            case "공무원":
                CurrentDream = Dream.공무원;
                break;
            case "농구선수":
                CurrentDream = Dream.농구선수;
                break;
            case "변호사":
                CurrentDream = Dream.변호사;
                break;
            case "사진작가":
                CurrentDream = Dream.사진작가;
                break;
            case "선생님":
                CurrentDream = Dream.선생님;
                break;
            case "야구선수":
                CurrentDream = Dream.야구선수;
                break;
            case "요리사":
                CurrentDream = Dream.요리사;
                break;
            case "웹툰작가":
                CurrentDream = Dream.웹툰작가;
                break;
            case "의사":
                CurrentDream = Dream.의사;
                break;
            case "축구선수":
                CurrentDream = Dream.축구선수;
                break;
            case "프로그래머":
                CurrentDream = Dream.프로그래머;
                break;
        }
        if (SaveGame.Exists("TalentSchedules"))
        {
            SonManager.SonData.TalentSchedules = SaveGame.Load<Dictionary<ScheduleName, int>>("TalentSchedules");
        }
        if (SaveGame.Exists("InterestSchedules"))
        {
            SonManager.SonData.InterestSchedules = SaveGame.Load<Dictionary<ScheduleName, int>>("InterestSchedules");
        }
    }
    public void LoadSonInfoDetail()
    {
        MilitaryServiceStatus = SaveGame.Load<bool>("MilitaryServiceStatus");
    }
    public void LoadSonAttributes()
    {
        Physical = SaveGame.Load<int>("Physical");
        Intelligence = SaveGame.Load<int>("Intelligence");
        Sensibility = SaveGame.Load<int>("Sensibility");
        Craft = SaveGame.Load<int>("Craft");
        Sociability = SaveGame.Load<int>("Sociability");
        Charm = SaveGame.Load<int>("Charm");
        Notion = SaveGame.Load<int>("Notion");

        Stress = SaveGame.Load<int>("Stress");
        Intimacy = SaveGame.Load<int>("Intimacy");
        Wealth = SaveGame.Load<int>("Wealth");

        Athletic = SaveGame.Load<int>("Athletic");
        Education = SaveGame.Load<int>("Education");
        Art = SaveGame.Load<int>("Art");
        Technic = SaveGame.Load<int>("Technic");
    }
    public void LoadSonEmotion()
    {
        switch (SaveGame.Load<string>("Emotion"))
        {
            case "평범함":
                CurrentEmotion = Emotion.평범함;
                break;
            case "화남":
                CurrentEmotion = Emotion.화남;
                break;
            case "행복함":
                CurrentEmotion = Emotion.행복함;
                break;
            case "슬픔":
                CurrentEmotion = Emotion.슬픔;
                break;
            case "아픔":
                CurrentEmotion = Emotion.아픔;
                break;
            case "삐짐":
                CurrentEmotion = Emotion.삐짐;
                break;
        }
    }
    public void LoadSonPersonalityTrait()
    {
        switch (SaveGame.Load<PersonalityTrait>("PersonalityTrait1"))
        {
            case PersonalityTrait.없음:
                CurrentPersonalityTrait1 = PersonalityTrait.없음;
                SonManager.Instance.PersonalityTraitImage1.sprite = SonManager.Instance.없음;
                break;
            case PersonalityTrait.고집센:
                CurrentPersonalityTrait1 = PersonalityTrait.고집센;
                SonManager.Instance.PersonalityTraitImage1.sprite = SonManager.Instance.고집센;
                break;
            case PersonalityTrait.순종적:
                CurrentPersonalityTrait1 = PersonalityTrait.순종적;
                SonManager.Instance.PersonalityTraitImage1.sprite = SonManager.Instance.순종적;
                break;
            case PersonalityTrait.다정한:
                CurrentPersonalityTrait1 = PersonalityTrait.다정한;
                SonManager.Instance.PersonalityTraitImage1.sprite = SonManager.Instance.다정한;
                break;
            case PersonalityTrait.직설적:
                CurrentPersonalityTrait1 = PersonalityTrait.직설적;
                SonManager.Instance.PersonalityTraitImage1.sprite = SonManager.Instance.직설적;
                break;
            case PersonalityTrait.내성적:
                CurrentPersonalityTrait1 = PersonalityTrait.내성적;
                SonManager.Instance.PersonalityTraitImage1.sprite = SonManager.Instance.내성적;
                break;
            case PersonalityTrait.쾌활한:
                CurrentPersonalityTrait1 = PersonalityTrait.쾌활한;
                SonManager.Instance.PersonalityTraitImage1.sprite = SonManager.Instance.쾌활한;
                break;
            case PersonalityTrait.성실한:
                CurrentPersonalityTrait1 = PersonalityTrait.성실한;
                SonManager.Instance.PersonalityTraitImage1.sprite = SonManager.Instance.성실한;
                break;
            case PersonalityTrait.나태한:
                CurrentPersonalityTrait1 = PersonalityTrait.나태한;
                SonManager.Instance.PersonalityTraitImage1.sprite = SonManager.Instance.나태한;
                break;
        }
        switch (SaveGame.Load<PersonalityTrait>("PersonalityTrait2"))
        {
            case PersonalityTrait.없음:
                CurrentPersonalityTrait2 = PersonalityTrait.없음;
                SonManager.Instance.PersonalityTraitImage2.sprite = SonManager.Instance.없음;
                break;
            case PersonalityTrait.고집센:
                CurrentPersonalityTrait2 = PersonalityTrait.고집센;
                SonManager.Instance.PersonalityTraitImage2.sprite = SonManager.Instance.고집센;
                break;
            case PersonalityTrait.순종적:
                CurrentPersonalityTrait2 = PersonalityTrait.순종적;
                SonManager.Instance.PersonalityTraitImage2.sprite = SonManager.Instance.순종적;
                break;
            case PersonalityTrait.다정한:
                CurrentPersonalityTrait2 = PersonalityTrait.다정한;
                SonManager.Instance.PersonalityTraitImage2.sprite = SonManager.Instance.다정한;
                break;
            case PersonalityTrait.직설적:
                CurrentPersonalityTrait2 = PersonalityTrait.직설적;
                SonManager.Instance.PersonalityTraitImage2.sprite = SonManager.Instance.직설적;
                break;
            case PersonalityTrait.내성적:
                CurrentPersonalityTrait2 = PersonalityTrait.내성적;
                SonManager.Instance.PersonalityTraitImage2.sprite = SonManager.Instance.내성적;
                break;
            case PersonalityTrait.쾌활한:
                CurrentPersonalityTrait2 = PersonalityTrait.쾌활한;
                SonManager.Instance.PersonalityTraitImage2.sprite = SonManager.Instance.쾌활한;
                break;
            case PersonalityTrait.성실한:
                CurrentPersonalityTrait2 = PersonalityTrait.성실한;
                SonManager.Instance.PersonalityTraitImage2.sprite = SonManager.Instance.성실한;
                break;
            case PersonalityTrait.나태한:
                CurrentPersonalityTrait2 = PersonalityTrait.나태한;
                SonManager.Instance.PersonalityTraitImage2.sprite = SonManager.Instance.나태한;
                break;
        }
        switch (SaveGame.Load<PersonalityTrait>("PersonalityTrait3"))
        {
            case PersonalityTrait.없음:
                CurrentPersonalityTrait3 = PersonalityTrait.없음;
                SonManager.Instance.PersonalityTraitImage3.sprite = SonManager.Instance.없음;
                break;
            case PersonalityTrait.고집센:
                CurrentPersonalityTrait3 = PersonalityTrait.고집센;
                SonManager.Instance.PersonalityTraitImage3.sprite = SonManager.Instance.고집센;
                break;
            case PersonalityTrait.순종적:
                CurrentPersonalityTrait3 = PersonalityTrait.순종적;
                SonManager.Instance.PersonalityTraitImage3.sprite = SonManager.Instance.순종적;
                break;
            case PersonalityTrait.다정한:
                CurrentPersonalityTrait3 = PersonalityTrait.다정한;
                SonManager.Instance.PersonalityTraitImage3.sprite = SonManager.Instance.다정한;
                break;
            case PersonalityTrait.직설적:
                CurrentPersonalityTrait3 = PersonalityTrait.직설적;
                SonManager.Instance.PersonalityTraitImage3.sprite = SonManager.Instance.직설적;
                break;
            case PersonalityTrait.내성적:
                CurrentPersonalityTrait3 = PersonalityTrait.내성적;
                SonManager.Instance.PersonalityTraitImage3.sprite = SonManager.Instance.내성적;
                break;
            case PersonalityTrait.쾌활한:
                CurrentPersonalityTrait3 = PersonalityTrait.쾌활한;
                SonManager.Instance.PersonalityTraitImage3.sprite = SonManager.Instance.쾌활한;
                break;
            case PersonalityTrait.성실한:
                CurrentPersonalityTrait3 = PersonalityTrait.성실한;
                SonManager.Instance.PersonalityTraitImage3.sprite = SonManager.Instance.성실한;
                break;
            case PersonalityTrait.나태한:
                CurrentPersonalityTrait3 = PersonalityTrait.나태한;
                SonManager.Instance.PersonalityTraitImage3.sprite = SonManager.Instance.나태한;
                break;
        }
        switch (SaveGame.Load<PersonalityTrait>("PersonalityTrait4"))
        {
            case PersonalityTrait.없음:
                CurrentPersonalityTrait4 = PersonalityTrait.없음;
                SonManager.Instance.PersonalityTraitImage4.sprite = SonManager.Instance.없음;
                break;
            case PersonalityTrait.고집센:
                CurrentPersonalityTrait4 = PersonalityTrait.고집센;
                SonManager.Instance.PersonalityTraitImage4.sprite = SonManager.Instance.고집센;
                break;
            case PersonalityTrait.순종적:
                CurrentPersonalityTrait4 = PersonalityTrait.순종적;
                SonManager.Instance.PersonalityTraitImage4.sprite = SonManager.Instance.순종적;
                break;
            case PersonalityTrait.다정한:
                CurrentPersonalityTrait4 = PersonalityTrait.다정한;
                SonManager.Instance.PersonalityTraitImage4.sprite = SonManager.Instance.다정한;
                break;
            case PersonalityTrait.직설적:
                CurrentPersonalityTrait4 = PersonalityTrait.직설적;
                SonManager.Instance.PersonalityTraitImage4.sprite = SonManager.Instance.직설적;
                break;
            case PersonalityTrait.내성적:
                CurrentPersonalityTrait4 = PersonalityTrait.내성적;
                SonManager.Instance.PersonalityTraitImage4.sprite = SonManager.Instance.내성적;
                break;
            case PersonalityTrait.쾌활한:
                CurrentPersonalityTrait4 = PersonalityTrait.쾌활한;
                SonManager.Instance.PersonalityTraitImage4.sprite = SonManager.Instance.쾌활한;
                break;
            case PersonalityTrait.성실한:
                CurrentPersonalityTrait4 = PersonalityTrait.성실한;
                SonManager.Instance.PersonalityTraitImage4.sprite = SonManager.Instance.성실한;
                break;
            case PersonalityTrait.나태한:
                CurrentPersonalityTrait4 = PersonalityTrait.나태한;
                SonManager.Instance.PersonalityTraitImage4.sprite = SonManager.Instance.나태한;
                break;
        }
    }
}

public enum Emotion
{
    평범함 = 0,
    화남 = 1,
    행복함 = 2,
    슬픔 = 3,
    아픔 = 4,
    삐짐 = 5
}
public enum GeneticTrait
{
    건강함 = 0,
    나약함 = 1,
    천재 = 2,
    영재 = 3,
    평범함 = 4,
    둔재 = 5
}
public enum PersonalityTrait
{
    없음,
    나태한,
    성실한,
    쾌활한,
    내성적,
    다정한,
    직설적,
    순종적,
    고집센
}
public enum Job
{
    유치원생,
    초등학생,
    중학생,
    고등학생,
    대학생,
    군인,
    백수,
    직장인
}
public enum Dream
{
    회사원, 선생님, 변호사, 의사, 공무원, 야구선수, 축구선수, 농구선수, 가수, 웹툰작가, 사진작가, 요리사, 프로그래머
}