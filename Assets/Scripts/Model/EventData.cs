﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventData
{
    public EventData()
    {
        increaseAttributes = new Dictionary<string, int>();
        decreaseAttributes = new Dictionary<string, int>();
        addTrait = PersonalityTrait.없음;
        removeTrait = PersonalityTrait.없음;
        confirmText = "확인";
    }
    public int seq;

    public EventType eventType = EventType.Default;
    // 이벤트 이미지 Assets/Resources  Load 기능을 이용해 호출
    public string imagePath;
    // 제목
    public string subject;
    // 내용
    public string context;
    // 선택지 갯수
    public int choiceLength;
    // 선택지 갯수만큼의 버튼명(선택지 이름)    
    public string choiceText1;
    public string choiceText2;
    public string choiceText3;
    public string choiceText4;
    public int choiceSeq1;
    public int choiceSeq2;
    public int choiceSeq3;
    public int choiceSeq4;
    public int choiceDelayedDay1;
    public int choiceDelayedDay2;
    public int choiceDelayedDay3;
    public int choiceDelayedDay4;
    public string confirmText;
    public int resultSeq;
    public int resultDelayedDay;
    public int probability;
    // Attribute String, In/Decrease Value
    public Dictionary<string, int> increaseAttributes;
    public Dictionary<string, int> decreaseAttributes;
    public PersonalityTrait addTrait;
    public PersonalityTrait removeTrait;
}

public enum EventType
{
    Default, Result
}