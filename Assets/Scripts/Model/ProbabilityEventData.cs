﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProbabilityEventData
{
    public int Seq;
    public int Probability;

    public ProbabilityEventData(int seq, int probability)
    {
        this.Seq = seq;
        this.Probability = probability;
    }
}
