﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerEventData
{

    public int EventSeq;
    public int DelayedDay;

    public TriggerEventData(int eventSeq)
    {
        EventSeq = eventSeq;
        DelayedDay = 0;
    }
    public TriggerEventData(int eventSeq, int delayedDay)
    {
        EventSeq = eventSeq;
        DelayedDay = delayedDay;
    }

}
