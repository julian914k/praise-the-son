﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BayatGames.SaveGameFree;
using UnityEngine.SceneManagement;

public class EventDB : MonoBehaviour
{
    public static EventDB Instance = null;
    public EventData eventData;
    private string choiceText = null;
    private int choiceSeq = 0;
    private int choiceDelayedDay = 0;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }

    // 모든 이벤트 정보 입력
    public EventData FindEvent(int eventSeq)
    {
        eventData = new EventData();

        eventData.seq = eventSeq;
        bool _shouldFindEvent = false;

        switch (SonManager.SonData.CurrentJob)
        {
            case Job.유치원생:
                switch (eventSeq)
                {
                    case 10101:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Default;
                        eventData.subject = "입학식";
                        eventData.context = "오늘은 아들의 초등학교 입학식이 있는 날입니다.";
                        eventData.choiceLength = 4;
                        eventData.choiceText1 = "입학식에 따라간다.";
                        eventData.choiceSeq1 = 10102;
                        eventData.choiceDelayedDay1 = 0;
                        eventData.choiceText2 = "선물을 준다.";
                        eventData.choiceSeq2 = 10103;
                        eventData.choiceDelayedDay2 = 0;
                        eventData.choiceText3 = "입학을 축하한다고 말한다.";
                        eventData.choiceSeq3 = 10104;
                        eventData.choiceDelayedDay3 = 0;
                        eventData.choiceText4 = "아무것도 하지 않는다.";
                        eventData.choiceSeq4 = 10105;
                        eventData.choiceDelayedDay4 = 0;
                        ScheduleManager.Instance.SetSchedule(Day.Morning, ScheduleName.학교가기, true);
                        SonManager.SonData.InterestSchedules[ScheduleName.운동하기] = 1;
                        break;
                    case 10102:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "입학식";
                        eventData.context = "아들의 입학식에 따라갔습니다.";
                        SonManager.SonData.CurrentJob = Job.초등학생;
                        eventData.increaseAttributes.Add("친밀도", 100);
                        //성격추가 테스트
                        EventManager.Instance.AddTrait(PersonalityTrait.순종적);
                        break;
                    case 10103:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "입학식";
                        eventData.context = "아들에게 선물을 주었습니다.";
                        SonManager.SonData.CurrentJob = Job.초등학생;
                        eventData.increaseAttributes.Add("친밀도", 200);
                        EventManager.Instance.AddTrait(PersonalityTrait.다정한);
                        break;
                    case 10104:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "입학식";
                        eventData.context = "아들의 입학을 축하해 주었습니다.";
                        SonManager.SonData.CurrentJob = Job.초등학생;
                        eventData.increaseAttributes.Add("친밀도", 50);
                        EventManager.Instance.AddTrait(PersonalityTrait.다정한);
                        break;
                    case 10105:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "입학식";
                        eventData.context = "아무것도 하지 않았습니다.";
                        SonManager.SonData.CurrentJob = Job.초등학생;
                        eventData.increaseAttributes.Add("친밀도", -100);
                        EventManager.Instance.AddTrait(PersonalityTrait.고집센);
                        break;
                    default:
                        _shouldFindEvent = true;
                        break;
                }
                break;
            case Job.초등학생:
                switch (eventSeq)
                {
                    case 10201:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "방학";
                        eventData.context = "오늘부터 여름방학입니다!";
                        //학교가기 해제 (오전)                        
                        ScheduleManager.Instance.RemoveSchedule(Day.Morning);
                        break;
                    case 10301:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "개학";
                        eventData.context = "여름방학이 끝났습니다. 이제는 다시 학교로 돌아갈 시간입니다.";
                        //학교가기 스케쥴 할당 (오전)        
                        ScheduleManager.Instance.SetSchedule(Day.Morning, ScheduleName.학교가기, true);
                        break;
                    case 10401:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Default;
                        eventData.subject = "졸업식";
                        eventData.context = "오늘은 아들의 졸업식이 있는 날입니다.";
                        eventData.choiceLength = 2;
                        eventData.choiceText1 = "졸업식에 간다.";
                        eventData.choiceSeq1 = 10402;
                        eventData.choiceDelayedDay1 = 0;
                        eventData.choiceText2 = "졸업식에 가지 않는다.";
                        eventData.choiceSeq2 = 10403;
                        eventData.choiceDelayedDay2 = 0;
                        //학교가기 해제
                        ScheduleManager.Instance.RemoveSchedule(Day.Morning);
                        break;
                    case 10402:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "졸업식";
                        eventData.context = "아들의 졸업식에 참석했습니다.";
                        eventData.increaseAttributes.Add("친밀도", 100);
                        break;
                    case 10403:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "졸업식";
                        eventData.context = "아들의 졸업식에 참석하지 않았습니다.";
                        eventData.increaseAttributes.Add("친밀도", -100);
                        break;
                    case 20101:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Default;
                        eventData.subject = "입학식";
                        eventData.context = "오늘은 아들의 중학교 입학식이 있는 날입니다.";
                        eventData.choiceLength = 4;
                        eventData.choiceText1 = "입학식에 따라간다.";
                        eventData.choiceSeq1 = 20102;
                        eventData.choiceDelayedDay1 = 0;
                        eventData.choiceText2 = "선물을 준다.";
                        eventData.choiceSeq2 = 20103;
                        eventData.choiceDelayedDay2 = 0;
                        eventData.choiceText3 = "입학을 축하한다고 말한다.";
                        eventData.choiceSeq3 = 20104;
                        eventData.choiceDelayedDay3 = 0;
                        eventData.choiceText4 = "아무것도 하지 않는다.";
                        eventData.choiceSeq4 = 20105;
                        eventData.choiceDelayedDay4 = 0;
                        break;
                    case 20102:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "입학식";
                        eventData.context = "아들의 입학식에 따라갔습니다.";
                        SonManager.SonData.CurrentJob = Job.중학생;
                        eventData.increaseAttributes.Add("친밀도", 50);
                        //학교가기 스케쥴 강제 할당(오전/오후)
                        ScheduleManager.Instance.SetSchedule(Day.Morning, ScheduleName.학교가기, true);
                        ScheduleManager.Instance.SetSchedule(Day.Evening, ScheduleName.학교가기, true);
                        //ScheduleManager.Instance.RemoveSchedule(Day.Morning);                
                        //ScheduleManager.Instance.RemoveSchedule(Day.Evening);                
                        break;
                    case 20103:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "입학식";
                        eventData.context = "아들에게 선물을 주었습니다.";
                        SonManager.SonData.CurrentJob = Job.중학생;
                        eventData.increaseAttributes.Add("친밀도", 100);
                        //학교가기 스케쥴 강제 할당(오전/오후)
                        ScheduleManager.Instance.SetSchedule(Day.Morning, ScheduleName.학교가기, true);
                        ScheduleManager.Instance.SetSchedule(Day.Evening, ScheduleName.학교가기, true);
                        //ScheduleManager.Instance.RemoveSchedule(Day.Morning);                
                        //ScheduleManager.Instance.RemoveSchedule(Day.Evening);             
                        break;
                    case 20104:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "입학식";
                        eventData.context = "아들의 입학을 축하해 주었습니다.";
                        SonManager.SonData.CurrentJob = Job.중학생;
                        //학교가기 스케쥴 강제 할당(오전/오후)
                        ScheduleManager.Instance.SetSchedule(Day.Morning, ScheduleName.학교가기, true);
                        ScheduleManager.Instance.SetSchedule(Day.Evening, ScheduleName.학교가기, true);
                        //ScheduleManager.Instance.RemoveSchedule(Day.Morning);                
                        //ScheduleManager.Instance.RemoveSchedule(Day.Evening);             
                        break;
                    case 20105:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "입학식";
                        eventData.context = "아무것도 하지 않았습니다.";
                        SonManager.SonData.CurrentJob = Job.중학생;
                        eventData.increaseAttributes.Add("친밀도", -50);
                        //학교가기 스케쥴 강제 할당(오전/오후)
                        ScheduleManager.Instance.SetSchedule(Day.Morning, ScheduleName.학교가기, true);
                        ScheduleManager.Instance.SetSchedule(Day.Evening, ScheduleName.학교가기, true);
                        //ScheduleManager.Instance.RemoveSchedule(Day.Morning);                
                        //ScheduleManager.Instance.RemoveSchedule(Day.Evening);             
                        break;
                    default:
                        _shouldFindEvent = true;
                        break;
                }
                break;
            case Job.중학생:
                switch (eventSeq)
                {
                    case 20201:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Default;
                        eventData.subject = "적성검사";
                        eventData.context = "자신을 이해하는데 도움이 되는 각종 검사들은 진로 의사결정에 유용한 정보를 제공합니다.";
                        eventData.choiceLength = 4;
                        eventData.choiceText1 = "IQ 테스트를 본다.";
                        eventData.choiceSeq1 = 20202;
                        eventData.choiceDelayedDay1 = 0;
                        eventData.choiceText2 = "운동 능력 평가를 한다.";
                        eventData.choiceSeq2 = 20203;
                        eventData.choiceDelayedDay2 = 0;
                        eventData.choiceText3 = "그림 심리 검사를 한다.";
                        eventData.choiceSeq3 = 20204;
                        eventData.choiceDelayedDay3 = 0;
                        eventData.choiceText4 = "종이 접기 검사를 한다.";
                        eventData.choiceSeq4 = 20205;
                        eventData.choiceDelayedDay4 = 0;
                        break;
                    case 20202:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Default;
                        eventData.subject = "적성검사";
                        eventData.context = "지난 주에 본 IQ테스트 결과가 나왔습니다.";
                        eventData.choiceLength = 3;
                        eventData.choiceText1 = SonManager.SonData.CurrentDream.ToString() + "에 소질이 있는 것 같다.";
                        eventData.choiceSeq1 = 20206;
                        eventData.choiceDelayedDay1 = 7;
                        //꿈 선택지를 보여주는 함수 필요
                        eventData.choiceText2 = SonManager.SonData.CurrentDream.ToString() + "에 소질이 있는 것 같다.";
                        eventData.choiceSeq2 = 20206;
                        eventData.choiceDelayedDay2 = 7;
                        eventData.choiceText3 = "테스트 결과가 마음에 들지 않는다.";
                        eventData.choiceSeq3 = 20207;
                        eventData.choiceDelayedDay2 = 7;
                        break;
                    case 20203:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Default;
                        eventData.subject = "적성검사";
                        eventData.context = "지난 주에 본 운동 능력 평가 결과가 나왔습니다.";
                        eventData.choiceLength = 3;
                        eventData.choiceText1 = SonManager.SonData.CurrentDream.ToString() + "에 소질이 있는 것 같다.";
                        eventData.choiceSeq1 = 20206;
                        eventData.choiceDelayedDay1 = 0;
                        eventData.choiceText2 = SonManager.SonData.CurrentDream.ToString() + "에 소질이 있는 것 같다.";
                        eventData.choiceSeq2 = 20206;
                        eventData.choiceDelayedDay2 = 0;
                        eventData.choiceText3 = "테스트 결과가 마음에 들지 않는다.";
                        eventData.choiceSeq3 = 20207;
                        eventData.choiceDelayedDay3 = 0;
                        break;
                    case 20204:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Default;
                        eventData.subject = "적성검사";
                        eventData.context = "지난 주에 본 그림 심리 검사 결과가 나왔습니다.";
                        eventData.choiceLength = 3;
                        eventData.choiceText1 = SonManager.SonData.CurrentDream.ToString() + "에 소질이 있는 것 같다.";
                        eventData.choiceSeq1 = 20206;
                        eventData.choiceDelayedDay1 = 0;
                        eventData.choiceText2 = SonManager.SonData.CurrentDream.ToString() + "에 관심이 있는 것 같다.";
                        eventData.choiceSeq2 = 20206;
                        eventData.choiceDelayedDay2 = 0;
                        eventData.choiceText3 = "테스트 결과가 마음에 들지 않는다.";
                        eventData.choiceSeq3 = 20207;
                        eventData.choiceDelayedDay3 = 0;
                        break;
                    case 20205:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Default;
                        eventData.subject = "적성검사";
                        eventData.context = "지난 주에 본 종이 접기 검사 결과가 나왔습니다.";
                        eventData.choiceLength = 3;
                        eventData.choiceText1 = SonManager.SonData.CurrentDream.ToString() + "에 소질이 있는 것 같다.";
                        eventData.choiceSeq1 = 20206;
                        eventData.choiceDelayedDay1 = 0;
                        eventData.choiceText2 = SonManager.SonData.CurrentDream.ToString() + "에 소질이 있는 것 같다.";
                        eventData.choiceSeq2 = 20206;
                        eventData.choiceDelayedDay2 = 0;
                        eventData.choiceText3 = "테스트 결과가 마음에 들지 않는다.";
                        eventData.choiceSeq3 = 20207;
                        eventData.choiceDelayedDay3 = 0;
                        break;
                    case 20206:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "적성검사";
                        eventData.context = "아들의 꿈이" + "축구선수" + "(으)로 바뀌었습니다.";
                        SonManager.SonData.CurrentDream = Dream.축구선수;
                        SonManager.SonData.InterestSchedules[ScheduleName.운동하기] = 0;
                        //현재의 꿈을 변경
                        break;
                    case 20207:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "적성검사";
                        eventData.context = "아들은 지금의 꿈을 계속 키워나가기로 했습니다.";
                        break;
                    case 20301:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Default;
                        eventData.subject = "1학기 지필평가";
                        eventData.context = "다음 주 부터 3일 간 1학기 지필평가가 시작됩니다.";
                        eventData.choiceLength = 4;
                        eventData.choiceText1 = "좋은 성적을 요구한다..";
                        eventData.choiceSeq1 = 20302;
                        eventData.choiceDelayedDay1 = 0;
                        eventData.choiceText2 = "부담갖지 말라고 한다.";
                        eventData.choiceSeq2 = 20303;
                        eventData.choiceDelayedDay2 = 0;
                        eventData.choiceText3 = "신경쓰지 않는다.";
                        eventData.choiceSeq3 = 20304;
                        eventData.choiceDelayedDay3 = 0;
                        //상하중으로 지능 능력치에 따라 시험 등수 계산
                        if (SonManager.SonData.Intelligence > 150)
                        {
                            // 상
                            SaveGame.Save<int>("TestRank", Random.Range(1, 10));
                        }
                        else if (SonManager.SonData.Intelligence < 50)
                        {
                            // 하
                            SaveGame.Save<int>("TestRank", Random.Range(15, 30));
                        }
                        else
                        {
                            // 중
                            SaveGame.Save<int>("TestRank", Random.Range(10, 20));
                        }



                        break;
                    case 20302:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "1학기 지필평가";
                        eventData.context = "아들에게 좋은 성적을 받았으면 좋겠다고 말했습니다.";
                        eventData.increaseAttributes.Add("스트레스", 200);
                        eventData.resultSeq = 20305;
                        eventData.resultDelayedDay = 7;
                        // 시험 등수 계산                        
                        int testRank1 = SaveGame.Load<int>("TestRank") - Random.Range(0, 10);
                        if (testRank1 < 1) testRank1 = 1;
                        SaveGame.Save<int>("TestRank", testRank1);
                        break;
                    case 20303:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "1학기 지필평가";
                        eventData.context = "아들에게 부담갖지 말라고 말했습니다.";
                        eventData.resultSeq = 20305;
                        eventData.resultDelayedDay = 7;
                        // 시험 등수 계산
                        int testRank2 = SaveGame.Load<int>("TestRank") - Random.Range(0, 5);
                        if (testRank2 < 1) testRank2 = 1;
                        SaveGame.Save<int>("TestRank", testRank2);
                        //데이터 값 초기화                       
                        //SaveGame.Delete("TestRank");

                        break;
                    case 20304:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "1학기 지필평가";
                        eventData.context = "아들이 하고 싶은대로 내버려두었습니다.";
                        eventData.resultSeq = 20305;
                        eventData.resultDelayedDay = 7;
                        eventData.increaseAttributes.Add("스트레스", -50);
                        //성적 등수 계산
                        int testRank3 = SaveGame.Load<int>("TestRank") - Random.Range(-5, 5);
                        if (testRank3 < 1) testRank3 = 1;
                        SaveGame.Save<int>("TestRank", testRank3);
                        break;
                    case 20305:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "1학기 지필평가";
                        eventData.context = "1학기 지필평가가 시작되었습니다.";
                        eventData.resultSeq = 20401;
                        eventData.resultDelayedDay = 7;
                        break;
                    case 20401:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "지필평가 결과";
                        eventData.context = "시험 결과가 나왔습니다. 아들은 반에서 " + SaveGame.Load<int>("TestRank") + "등을 했습니다.";
                        if (SaveGame.Load<int>("TestRank") <= 5)
                        {//상
                            eventData.increaseAttributes.Add("지식평가", 2);
                        }
                        else if (SaveGame.Load<int>("TestRank") >= 20)
                        {//하
                            eventData.increaseAttributes.Add("지식평가", -1);
                            if (SonManager.SonData.Education <= 10) SonManager.SonData.Education = 10;
                        }
                        else
                        {//중
                            eventData.increaseAttributes.Add("지식평가", 1);
                        }
                        break;
                    //날짜고정이벤트 아님
                    case 20501:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "방학";
                        eventData.context = "오늘부터 여름방학입니다!";
                        eventData.increaseAttributes.Add("스트레스", -100);
                        //학교가기 스케쥴 해제(오전/오후)
                        ScheduleManager.Instance.RemoveSchedule(Day.Morning);
                        ScheduleManager.Instance.RemoveSchedule(Day.Evening);
                        break;
                    case 20502:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "개학";
                        eventData.context = "여름방학이 끝났습니다. 이제는 다시 학교로 돌아갈 시간입니다.";
                        eventData.increaseAttributes.Add("스트레스", 100);
                        //학교가기 스케쥴 장착(오전/오후)                       
                        ScheduleManager.Instance.SetSchedule(Day.Morning, ScheduleName.학교가기, true);
                        ScheduleManager.Instance.SetSchedule(Day.Evening, ScheduleName.학교가기, true);

                        break;
                    case 20601:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Default;
                        eventData.subject = "2학기 지필평가";
                        eventData.context = "다음 주 부터 3일 간 2학기 지필평가가 시작됩니다.";
                        eventData.choiceLength = 4;
                        eventData.choiceText1 = "좋은 성적을 요구한다..";
                        eventData.choiceSeq1 = 20602;
                        eventData.choiceDelayedDay1 = 0;
                        eventData.choiceText2 = "부담갖지 말라고 한다.";
                        eventData.choiceSeq2 = 20603;
                        eventData.choiceDelayedDay2 = 0;
                        eventData.choiceText3 = "신경쓰지 않는다.";
                        eventData.choiceSeq3 = 20604;
                        eventData.choiceDelayedDay3 = 0;
                        //상하중으로 지능 능력치에 따라 시험 등수 계산
                        if (SonManager.SonData.Intelligence > 150)
                        {
                            // 상
                            SaveGame.Save<int>("TestRank", Random.Range(1, 10));
                        }
                        else if (SonManager.SonData.Intelligence < 50)
                        {
                            // 하
                            SaveGame.Save<int>("TestRank", Random.Range(15, 30));
                        }
                        else
                        {
                            // 중
                            SaveGame.Save<int>("TestRank", Random.Range(10, 20));
                        }
                        break;
                    case 20602:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "2학기 지필평가";
                        eventData.context = "아들에게 좋은 성적을 받았으면 좋겠다고 말했습니다.";
                        eventData.increaseAttributes.Add("스트레스", 200);
                        eventData.resultSeq = 20605;
                        eventData.resultDelayedDay = 7;
                        // 시험 등수 계산
                        int testRank4 = SaveGame.Load<int>("TestRank") - Random.Range(0, 5);
                        if (testRank4 < 1) testRank4 = 1;
                        SaveGame.Save<int>("TestRank", testRank4);
                        //데이터 값 초기화                       
                        //SaveGame.Delete("TestRank");
                        break;
                    case 20603:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "2학기 지필평가";
                        eventData.context = "아들에게 부담갖지 말라고 말했습니다.";
                        eventData.resultSeq = 20605;
                        eventData.resultDelayedDay = 7;
                        // 시험 등수 계산
                        int testRank5 = SaveGame.Load<int>("TestRank") - Random.Range(0, 5);
                        if (testRank5 < 1) testRank5 = 1;
                        SaveGame.Save<int>("TestRank", testRank5);
                        //데이터 값 초기화                       
                        //SaveGame.Delete("TestRank");
                        break;
                    case 20604:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "2학기 지필평가";
                        eventData.context = "아들이 하고 싶은대로 내버려두었습니다.";
                        eventData.increaseAttributes.Add("스트레스", -50);
                        eventData.resultSeq = 20605;
                        eventData.resultDelayedDay = 7;
                        // 시험 등수 계산
                        int testRank6 = SaveGame.Load<int>("TestRank") - Random.Range(0, 5);
                        if (testRank6 < 1) testRank6 = 1;
                        SaveGame.Save<int>("TestRank", testRank6);
                        //데이터 값 초기화                       
                        //SaveGame.Delete("TestRank");
                        break;
                    case 20605:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "2학기 지필평가";
                        eventData.context = "2학기 지필평가가 시작되었습니다.";
                        eventData.resultSeq = 20701;
                        eventData.resultDelayedDay = 7;
                        break;
                    case 20701:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "지필평가 결과";
                        eventData.context = "시험 결과가 나왔습니다. 아들은 반에서" + SaveGame.Load<int>("TestRank") + "등을 했습니다.";
                        //시험결과
                        if (SaveGame.Load<int>("TestRank") <= 5)
                        {//상
                            eventData.increaseAttributes.Add("지식평가", 2);
                        }
                        else if (SaveGame.Load<int>("TestRank") >= 20)
                        {//하
                            eventData.increaseAttributes.Add("지식평가", -1);
                            if (SonManager.SonData.Education <= 10) SonManager.SonData.Education = 10;
                        }
                        else
                        {//중
                            eventData.increaseAttributes.Add("지식평가", 1);
                        }
                        //10.21                                              
                        break;

                    case 20801:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Default;
                        eventData.subject = "졸업식";
                        eventData.context = "오늘은 아들의 졸업식이 있는 날입니다.";
                        eventData.choiceLength = 2;
                        eventData.choiceText1 = "졸업식에 간다.";
                        eventData.choiceSeq1 = 20802;
                        eventData.choiceDelayedDay1 = 0;
                        eventData.choiceText2 = "졸업식에 가지 않는다.";
                        eventData.choiceSeq2 = 20803;
                        eventData.choiceDelayedDay2 = 0;
                        break;
                        //학교가기 스케쥴 해제(오전/오후)     
                        ScheduleManager.Instance.RemoveSchedule(Day.Morning);
                        ScheduleManager.Instance.RemoveSchedule(Day.Evening);
                    case 20802:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "졸업식";
                        eventData.context = "아들의 졸업식에 참석했습니다.";
                        eventData.increaseAttributes.Add("친밀도", 100);
                        break;
                    case 20803:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "졸업식";
                        eventData.context = "아들의 졸업식에 참석하지 않았습니다.";
                        eventData.increaseAttributes.Add("친밀도", -100);
                        break;
                    case 30101:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Default;
                        eventData.subject = "입학식";
                        eventData.context = "오늘은 아들의 고등학교 입학식이 있는 날입니다.";
                        eventData.choiceLength = 4;
                        eventData.choiceText1 = "입학식에 따라간다.";
                        eventData.choiceSeq1 = 30102;
                        eventData.choiceDelayedDay1 = 0;
                        eventData.choiceText2 = "선물을 준다.";
                        eventData.choiceSeq2 = 30103;
                        eventData.choiceDelayedDay2 = 0;
                        eventData.choiceText3 = "입학을 축하한다고 말한다.";
                        eventData.choiceSeq3 = 30104;
                        eventData.choiceDelayedDay3 = 0;
                        eventData.choiceText4 = "아무것도 하지 않는다.";
                        eventData.choiceSeq4 = 30105;
                        eventData.choiceDelayedDay4 = 0;
                        //학교가기 할당 (오전/오후/저녁)
                        ScheduleManager.Instance.SetSchedule(Day.Morning, ScheduleName.학교가기, true);
                        ScheduleManager.Instance.SetSchedule(Day.Evening, ScheduleName.학교가기, true);
                        ScheduleManager.Instance.SetSchedule(Day.Night, ScheduleName.학교가기, true);
                        break;
                    //3.1
                    case 30102:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "입학식";
                        eventData.context = "아들의 입학식에 따라갔습니다.";
                        SonManager.SonData.CurrentJob = Job.고등학생;
                        eventData.increaseAttributes.Add("친밀도", -50);
                        break;
                    case 30103:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "입학식";
                        eventData.context = "아들에게 선물을 주었습니다.";
                        SonManager.SonData.CurrentJob = Job.고등학생;
                        eventData.increaseAttributes.Add("친밀도", 50);
                        break;
                    case 20104:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "입학식";
                        eventData.context = "아들의 입학을 축하해 주었습니다.";
                        SonManager.SonData.CurrentJob = Job.고등학생;
                        break;
                    case 20105:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "입학식";
                        eventData.context = "아무것도 하지 않았습니다.";
                        SonManager.SonData.CurrentJob = Job.고등학생;
                        break;
                    default:
                        _shouldFindEvent = true;
                        break;
                }
                break;
            case Job.고등학생:
                switch (eventSeq)
                {
                    case 30201:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Default;
                        eventData.subject = "적성검사";
                        eventData.context = "자신을 이해하는데 도움이 되는 각종 검사들은 진로 의사결정에 유용한 정보를 제공합니다.";
                        eventData.choiceLength = 4;
                        eventData.choiceText1 = "IQ 테스트를 본다.";
                        eventData.choiceSeq1 = 30202;
                        eventData.choiceDelayedDay1 = 0;
                        eventData.choiceText2 = "운동 능력 평가를 한다.";
                        eventData.choiceSeq2 = 30203;
                        eventData.choiceDelayedDay2 = 0;
                        eventData.choiceText3 = "그림 심리 검사를 한다.";
                        eventData.choiceSeq3 = 30204;
                        eventData.choiceDelayedDay3 = 0;
                        eventData.choiceText4 = "종이 접기 검사를 한다.";
                        eventData.choiceSeq4 = 30205;
                        eventData.choiceDelayedDay4 = 0;
                        break;
                    case 30202:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Default;
                        eventData.subject = "적성검사";
                        eventData.context = "지난 주에 본 IQ테스트 결과가 나왔습니다.";
                        eventData.choiceLength = 4;
                        eventData.choiceText1 = SonManager.SonData.CurrentDream.ToString() + "에 소질이 있는 것 같다.";
                        eventData.choiceSeq1 = 30206;
                        eventData.choiceDelayedDay1 = 0;
                        eventData.choiceText2 = SonManager.SonData.CurrentDream.ToString() + "에 소질이 있는 것 같다.";
                        eventData.choiceSeq2 = 30206;
                        eventData.choiceDelayedDay2 = 0;
                        eventData.choiceText3 = SonManager.SonData.CurrentDream.ToString() + "에 소질이 있는 것 같다.";
                        eventData.choiceSeq3 = 30206;
                        eventData.choiceDelayedDay3 = 0;
                        eventData.choiceText4 = "테스트 결과가 마음에 들지 않는다.";
                        eventData.choiceSeq4 = 30207;
                        eventData.choiceDelayedDay4 = 0;
                        break;
                    case 30203:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Default;
                        eventData.subject = "적성검사";
                        eventData.context = "지난 주에 본 운동 능력 평가 결과가 나왔습니다.";
                        eventData.choiceLength = 4;
                        eventData.choiceText1 = SonManager.SonData.CurrentDream.ToString() + "에 소질이 있는 것 같다.";
                        eventData.choiceSeq1 = 30206;
                        eventData.choiceDelayedDay1 = 0;
                        eventData.choiceText2 = SonManager.SonData.CurrentDream.ToString() + "에 소질이 있는 것 같다.";
                        eventData.choiceSeq2 = 30206;
                        eventData.choiceDelayedDay2 = 0;
                        eventData.choiceText3 = SonManager.SonData.CurrentDream.ToString() + "에 소질이 있는 것 같다.";
                        eventData.choiceSeq3 = 30206;
                        eventData.choiceDelayedDay3 = 0;
                        eventData.choiceText4 = "테스트 결과가 마음에 들지 않는다.";
                        eventData.choiceSeq4 = 30207;
                        eventData.choiceDelayedDay4 = 0;
                        break;
                    case 30204:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Default;
                        eventData.subject = "적성검사";
                        eventData.context = "지난 주에 본 그림 심리 검사 결과가 나왔습니다.";
                        eventData.choiceLength = 4;
                        eventData.choiceText1 = SonManager.SonData.CurrentDream.ToString() + "에 소질이 있는 것 같다.";
                        eventData.choiceSeq1 = 30206;
                        eventData.choiceDelayedDay1 = 0;
                        eventData.choiceText2 = SonManager.SonData.CurrentDream.ToString() + "에 소질이 있는 것 같다.";
                        eventData.choiceSeq2 = 30206;
                        eventData.choiceDelayedDay2 = 0;
                        eventData.choiceText3 = SonManager.SonData.CurrentDream.ToString() + "에 소질이 있는 것 같다.";
                        eventData.choiceSeq3 = 30206;
                        eventData.choiceDelayedDay3 = 0;
                        eventData.choiceText4 = "테스트 결과가 마음에 들지 않는다.";
                        eventData.choiceSeq4 = 30207;
                        eventData.choiceDelayedDay4 = 0;
                        break;
                    case 30205:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Default;
                        eventData.subject = "적성검사";
                        eventData.context = "지난 주에 본 종이 접기 검사 결과가 나왔습니다.";
                        eventData.choiceLength = 4;
                        eventData.choiceText1 = SonManager.SonData.CurrentDream.ToString() + "에 소질이 있는 것 같다.";
                        eventData.choiceSeq1 = 30206;
                        eventData.choiceDelayedDay1 = 0;
                        eventData.choiceText2 = SonManager.SonData.CurrentDream.ToString() + "에 소질이 있는 것 같다.";
                        eventData.choiceSeq2 = 30206;
                        eventData.choiceDelayedDay2 = 0;
                        eventData.choiceText3 = SonManager.SonData.CurrentDream.ToString() + "에 소질이 있는 것 같다.";
                        eventData.choiceSeq3 = 30206;
                        eventData.choiceDelayedDay3 = 0;
                        eventData.choiceText4 = "테스트 결과가 마음에 들지 않는다.";
                        eventData.choiceSeq4 = 30207;
                        eventData.choiceDelayedDay4 = 0;
                        break;
                    case 30206:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "적성검사";
                        eventData.context = "아들의 꿈이" + "선생님" + "(으)로 바뀌었습니다.";
                        SonManager.SonData.CurrentDream = Dream.선생님;
                        //SonManager.SonData.InterestSchedules.Add(ScheduleName.도서관, 1);
                        //SonManager.SonData.InterestSchedules.Add(ScheduleName.독서하기, 1);                        
                        //현재의 꿈을 변경
                        break;
                    case 30207:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "적성검사";
                        eventData.context = "아들은 지금의 꿈을 계속 키워나가기로 했습니다.";
                        break;
                    case 30301:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Default;
                        eventData.subject = "1학기 시험";
                        eventData.context = "다음 주 부터 7일 간 1학기 시험이 시작됩니다.";
                        eventData.choiceLength = 4;
                        eventData.choiceText1 = "좋은 성적을 요구한다..";
                        eventData.choiceSeq1 = 30302;
                        eventData.choiceDelayedDay1 = 0;
                        eventData.choiceText2 = "부담갖지 말라고 한다.";
                        eventData.choiceSeq2 = 30303;
                        eventData.choiceDelayedDay2 = 0;
                        eventData.choiceText3 = "신경쓰지 않는다.";
                        eventData.choiceSeq3 = 30304;
                        eventData.choiceDelayedDay3 = 0;
                        //상하중으로 지능 능력치에 따라 시험 등수 계산
                        if (SonManager.SonData.Intelligence > 150)
                        {
                            // 상
                            SaveGame.Save<int>("TestRank", Random.Range(1, 10));
                        }
                        else if (SonManager.SonData.Intelligence < 50)
                        {
                            // 하
                            SaveGame.Save<int>("TestRank", Random.Range(15, 30));
                        }
                        else
                        {
                            // 중
                            SaveGame.Save<int>("TestRank", Random.Range(10, 20));
                        }
                        break;
                    case 30302:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "1학기 시험";
                        eventData.context = "아들에게 좋은 성적을 받았으면 좋겠다고 말했습니다.";
                        eventData.increaseAttributes.Add("스트레스", 200);
                        eventData.resultSeq = 30305;
                        eventData.resultDelayedDay = 7;
                        // 시험 등수 계산
                        int testRank1 = SaveGame.Load<int>("TestRank") - Random.Range(0, 5);
                        if (testRank1 < 1) testRank1 = 1;
                        SaveGame.Save<int>("TestRank", testRank1);
                        //데이터 값 초기화                       
                        //SaveGame.Delete("TestRank");
                        break;
                    case 30303:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "1학기 시험";
                        eventData.context = "아들에게 부담갖지 말라고 말했습니다.";
                        eventData.resultSeq = 30305;
                        eventData.resultDelayedDay = 7;
                        // 시험 등수 계산
                        int testRank2 = SaveGame.Load<int>("TestRank") - Random.Range(0, 5);
                        if (testRank2 < 1) testRank2 = 1;
                        SaveGame.Save<int>("TestRank", testRank2);
                        //데이터 값 초기화                       
                        //SaveGame.Delete("TestRank");
                        break;
                    case 30304:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "1학기 시험";
                        eventData.context = "아들이 하고 싶은대로 내버려두었습니다.";
                        eventData.resultSeq = 30305;
                        eventData.resultDelayedDay = 7;
                        eventData.increaseAttributes.Add("스트레스", -50);
                        // 시험 등수 계산
                        int testRank3 = SaveGame.Load<int>("TestRank") - Random.Range(0, 5);
                        if (testRank3 < 1) testRank3 = 1;
                        SaveGame.Save<int>("TestRank", testRank3);
                        //데이터 값 초기화                       
                        //SaveGame.Delete("TestRank");
                        break;
                    case 30305:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "1학기 시험";
                        eventData.context = "1학기 시험이 시작되었습니다.";
                        eventData.resultSeq = 30401;
                        eventData.resultDelayedDay = 7;
                        break;
                    case 30401:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "시험 결과";
                        eventData.context = "시험 결과가 나왔습니다. 아들은 반에서" + SaveGame.Load<int>("TestRank") + "등을 했습니다.";
                        //시험결과
                        if (SaveGame.Load<int>("TestRank") <= 5)
                        {//상
                            eventData.increaseAttributes.Add("지식평가", 2);
                        }
                        else if (SaveGame.Load<int>("TestRank") >= 20)
                        {//하
                            eventData.increaseAttributes.Add("지식평가", -1);
                            if (SonManager.SonData.Education <= 10) SonManager.SonData.Education = 10;
                        }
                        else
                        {//중
                            eventData.increaseAttributes.Add("지식평가", 1);
                        }
                        break;
                    case 30501:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "방학";
                        eventData.context = "오늘부터 여름방학입니다!";
                        eventData.increaseAttributes.Add("스트레스", -100);
                        ScheduleManager.Instance.RemoveSchedule(Day.Morning);
                        ScheduleManager.Instance.RemoveSchedule(Day.Evening);
                        ScheduleManager.Instance.RemoveSchedule(Day.Night);
                        //학교가기 스케쥴 해제(오전/오후/밤)

                        break;
                    case 30502:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "개학";
                        eventData.context = "여름방학이 끝났습니다. 이제는 다시 학교로 돌아갈 시간입니다.";
                        eventData.increaseAttributes.Add("스트레스", 100);
                        //학교가기 스케쥴 장착(오전/오후/밤)
                        ScheduleManager.Instance.SetSchedule(Day.Morning, ScheduleName.학교가기, true);
                        ScheduleManager.Instance.SetSchedule(Day.Evening, ScheduleName.학교가기, true);
                        ScheduleManager.Instance.SetSchedule(Day.Night, ScheduleName.학교가기, true);

                        break;
                    case 30601:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Default;
                        eventData.subject = "2학기 시험";
                        eventData.context = "다음 주 부터 7일 간 2학기 시험이 시작됩니다.";
                        eventData.choiceLength = 4;
                        eventData.choiceText1 = "좋은 성적을 요구한다..";
                        eventData.choiceSeq1 = 30602;
                        eventData.choiceDelayedDay1 = 0;
                        eventData.choiceText2 = "부담갖지 말라고 한다.";
                        eventData.choiceSeq2 = 30603;
                        eventData.choiceDelayedDay2 = 0;
                        eventData.choiceText3 = "신경쓰지 않는다.";
                        eventData.choiceSeq3 = 30604;
                        eventData.choiceDelayedDay3 = 0;

                        //상하중으로 지능 능력치에 따라 시험 등수 계산
                        if (SonManager.SonData.Intelligence > 150)
                        {
                            // 상
                            SaveGame.Save<int>("TestRank", Random.Range(1, 10));
                        }
                        else if (SonManager.SonData.Intelligence < 50)
                        {
                            // 하
                            SaveGame.Save<int>("TestRank", Random.Range(15, 30));
                        }
                        else
                        {
                            // 중
                            SaveGame.Save<int>("TestRank", Random.Range(10, 20));
                        }
                        break;
                    case 30602:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "2학기 시험";
                        eventData.context = "아들에게 좋은 성적을 받았으면 좋겠다고 말했습니다.";
                        eventData.increaseAttributes.Add("스트레스", 200);
                        eventData.resultSeq = 30605;
                        eventData.resultDelayedDay = 7;
                        // 시험 등수 계산
                        int testRank4 = SaveGame.Load<int>("TestRank") - Random.Range(0, 5);
                        if (testRank4 < 1) testRank4 = 1;
                        SaveGame.Save<int>("TestRank", testRank4);
                        //데이터 값 초기화                       
                        //SaveGame.Delete("TestRank");
                        break;
                    case 30603:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "2학기 시험";
                        eventData.context = "아들에게 부담갖지 말라고 말했습니다.";
                        eventData.resultSeq = 30605;
                        eventData.resultDelayedDay = 7;
                        // 시험 등수 계산
                        int testRank5 = SaveGame.Load<int>("TestRank") - Random.Range(0, 5);
                        if (testRank5 < 1) testRank5 = 1;
                        SaveGame.Save<int>("TestRank", testRank5);
                        //데이터 값 초기화                       
                        //SaveGame.Delete("TestRank");

                        break;
                    case 30604:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "2학기 시험";
                        eventData.context = "아들이 하고 싶은대로 내버려두었습니다.";
                        eventData.increaseAttributes.Add("스트레스", -50);
                        eventData.resultSeq = 30605;
                        eventData.resultDelayedDay = 7;
                        // 시험 등수 계산
                        int testRank6 = SaveGame.Load<int>("TestRank") - Random.Range(0, 5);
                        if (testRank6 < 1) testRank6 = 1;
                        SaveGame.Save<int>("TestRank", testRank6);
                        //데이터 값 초기화                       
                        //SaveGame.Delete("TestRank");
                        break;
                    case 30605:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "2학기 시험";
                        eventData.context = "2학기 시험이 시작되었습니다.";
                        eventData.resultSeq = 30701;
                        eventData.resultDelayedDay = 7;
                        break;
                    case 30701:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "시험 결과";
                        eventData.context = "시험 결과가 나왔습니다. 아들은 반에서" + SaveGame.Load<int>("TestRank") + "등을 했습니다.";
                        //시험결과
                        if (SaveGame.Load<int>("TestRank") <= 5)
                        {//상
                            eventData.increaseAttributes.Add("지식평가", 2);
                        }
                        else if (SaveGame.Load<int>("TestRank") >= 20)
                        {//하
                            eventData.increaseAttributes.Add("지식평가", -1);
                            if (SonManager.SonData.Education <= 10) SonManager.SonData.Education = 10;
                        }
                        else
                        {//중
                            eventData.increaseAttributes.Add("지식평가", 1);
                        }
                        break;
                    case 30801:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Default;
                        eventData.subject = "졸업식";
                        eventData.context = "오늘은 아들의 졸업식이 있는 날입니다.";
                        eventData.choiceLength = 2;
                        eventData.choiceText1 = "졸업식에 간다.";
                        eventData.choiceSeq1 = 30802;
                        eventData.choiceDelayedDay1 = 0;
                        eventData.choiceText2 = "졸업식에 가지 않는다.";
                        eventData.choiceSeq2 = 30803;
                        eventData.choiceDelayedDay2 = 0;
                        ScheduleManager.Instance.RemoveSchedule(Day.Morning);
                        ScheduleManager.Instance.RemoveSchedule(Day.Evening);
                        ScheduleManager.Instance.RemoveSchedule(Day.Night);
                        break;
                    //1.4
                    case 30802:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "졸업식";
                        eventData.context = "아들의 졸업식에 참석했습니다.";
                        eventData.increaseAttributes.Add("친밀도", 100);

                        break;
                    case 30803:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "졸업식";
                        eventData.context = "아들의 졸업식에 참석하지 않았습니다.";
                        eventData.increaseAttributes.Add("친밀도", -100);

                        break;
                    case 40101:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Default;
                        eventData.subject = "입학식";
                        eventData.context = "오늘은 아들의 대학교 입학식이 있는 날입니다.";
                        eventData.choiceLength = 4;
                        eventData.choiceText1 = "입학식에 따라간다.";
                        eventData.choiceSeq1 = 40102;
                        eventData.choiceDelayedDay1 = 0;
                        eventData.choiceText2 = "선물을 준다.";
                        eventData.choiceSeq2 = 40103;
                        eventData.choiceDelayedDay2 = 0;
                        eventData.choiceText3 = "입학을 축하한다고 말한다.";
                        eventData.choiceSeq3 = 40104;
                        eventData.choiceDelayedDay3 = 0;
                        eventData.choiceText4 = "아무것도 하지 않는다.";
                        eventData.choiceSeq4 = 40105;
                        eventData.choiceDelayedDay4 = 0;
                        ScheduleManager.Instance.SetSchedule(Day.Morning, ScheduleName.학교가기, true);
                        break;
                    case 40102:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "입학식";
                        eventData.context = "아들의 입학식에 따라갔습니다.";
                        SonManager.SonData.CurrentJob = Job.대학생;
                        eventData.increaseAttributes.Add("친밀도", -250);
                        break;
                    case 40103:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "입학식";
                        eventData.context = "아들에게 선물을 주었습니다.";
                        SonManager.SonData.CurrentJob = Job.대학생;
                        eventData.increaseAttributes.Add("친밀도", 50);
                        break;
                    case 40104:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "입학식";
                        eventData.context = "아들의 입학을 축하해 주었습니다.";
                        SonManager.SonData.CurrentJob = Job.대학생;
                        break;
                    case 40105:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "입학식";
                        eventData.context = "아무것도 하지 않았습니다.";
                        SonManager.SonData.CurrentJob = Job.대학생;
                        break;
                    default:
                        _shouldFindEvent = true;
                        break;
                }
                break;
            case Job.대학생:
                switch (eventSeq)
                {
                    case 40201:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Default;
                        eventData.subject = "1학기 시험";
                        eventData.context = "다음 주 부터 7일 간 1학기 시험이 시작됩니다.";
                        eventData.choiceLength = 4;
                        eventData.choiceText1 = "좋은 성적을 요구한다..";
                        eventData.choiceSeq1 = 40202;
                        eventData.choiceDelayedDay1 = 0;
                        eventData.choiceText2 = "부담갖지 말라고 한다.";
                        eventData.choiceSeq2 = 40203;
                        eventData.choiceDelayedDay2 = 0;
                        eventData.choiceText3 = "신경쓰지 않는다.";
                        eventData.choiceSeq3 = 40204;
                        eventData.choiceDelayedDay3 = 0;
                        //상하중으로 지능 능력치에 따라 시험 등수 계산
                        if (SonManager.SonData.Intelligence > 150)
                        {
                            // 상
                            SaveGame.Save<int>("TestRank", Random.Range(1, 10));
                        }
                        else if (SonManager.SonData.Intelligence < 50)
                        {
                            // 하
                            SaveGame.Save<int>("TestRank", Random.Range(15, 30));
                        }
                        else
                        {
                            // 중
                            SaveGame.Save<int>("TestRank", Random.Range(10, 20));
                        }
                        break;
                    //6.7
                    case 40202:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "1학기 시험";
                        eventData.context = "아들에게 좋은 성적을 받았으면 좋겠다고 말했습니다.";
                        eventData.increaseAttributes.Add("스트레스", 200);
                        eventData.resultSeq = 40205;
                        eventData.resultDelayedDay = 7;
                        // 시험 등수 계산
                        int testRank1 = SaveGame.Load<int>("TestRank") - Random.Range(0, 5);
                        if (testRank1 < 1) testRank1 = 1;
                        SaveGame.Save<int>("TestRank", testRank1);
                        //데이터 값 초기화                       
                        //SaveGame.Delete("TestRank");
                        break;
                    case 40203:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "1학기 시험";
                        eventData.context = "아들에게 부담갖지 말라고 말했습니다.";
                        eventData.resultSeq = 40205;
                        eventData.resultDelayedDay = 7;
                        // 시험 등수 계산
                        int testRank2 = SaveGame.Load<int>("TestRank") - Random.Range(0, 5);
                        if (testRank2 < 1) testRank2 = 1;
                        SaveGame.Save<int>("TestRank", testRank2);
                        //데이터 값 초기화                       
                        //SaveGame.Delete("TestRank");
                        break;
                    case 40204:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "1학기 시험";
                        eventData.context = "아들이 하고 싶은대로 내버려두었습니다.";
                        eventData.resultSeq = 40205;
                        eventData.resultDelayedDay = 7;
                        eventData.increaseAttributes.Add("스트레스", -50);
                        // 시험 등수 계산
                        int testRank3 = SaveGame.Load<int>("TestRank") - Random.Range(0, 5);
                        if (testRank3 < 1) testRank3 = 1;
                        SaveGame.Save<int>("TestRank", testRank3);
                        //데이터 값 초기화                       
                        //SaveGame.Delete("TestRank");                       
                        break;
                    case 40205:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "1학기 시험";
                        eventData.context = "1학기 시험이 시작되었습니다.";
                        eventData.resultSeq = 40301;
                        eventData.resultDelayedDay = 7;
                        break;
                    case 40301:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "시험 결과";
                        eventData.context = "시험 결과가 나왔습니다. 아들은 과에서 " + SaveGame.Load<int>("TestRank") + "등을 했습니다.";
                        //시험결과
                        if (SaveGame.Load<int>("TestRank") <= 5)
                        {//상
                            eventData.increaseAttributes.Add("지식평가", 2);
                        }
                        else if (SaveGame.Load<int>("TestRank") >= 20)
                        {//하
                            eventData.increaseAttributes.Add("지식평가", -1);
                            if (SonManager.SonData.Education <= 10) SonManager.SonData.Education = 10;
                        }
                        else
                        {//중
                            eventData.increaseAttributes.Add("지식평가", 1);
                        }
                        break;
                    case 40401:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "종강";
                        eventData.context = "오늘부로 종강입니다.40401";
                        eventData.increaseAttributes.Add("스트레스", -100);
                        //학교가기 스케쥴 해제(오전)                        
                        ScheduleManager.Instance.RemoveSchedule(Day.Morning);
                        break;
                    case 40402:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "개강";
                        eventData.context = "내일부터 개강입니다.";
                        eventData.increaseAttributes.Add("스트레스", 100);
                        //학교가기 스케쥴 장착(오전)
                        ScheduleManager.Instance.SetSchedule(Day.Morning, ScheduleName.학교가기, true);
                        break;
                    case 40501:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Default;
                        eventData.subject = "2학기 시험";
                        eventData.context = "다음 주 부터 7일 간 2학기 시험이 시작됩니다.";
                        eventData.choiceLength = 4;
                        eventData.choiceText1 = "좋은 성적을 요구한다..";
                        eventData.choiceSeq1 = 40502;
                        eventData.choiceDelayedDay1 = 0;
                        eventData.choiceText2 = "부담갖지 말라고 한다.";
                        eventData.choiceSeq2 = 40503;
                        eventData.choiceDelayedDay2 = 0;
                        eventData.choiceText3 = "신경쓰지 않는다.";
                        eventData.choiceSeq3 = 40504;
                        eventData.choiceDelayedDay3 = 0;
                        //상하중으로 지능 능력치에 따라 시험 등수 계산
                        if (SonManager.SonData.Intelligence > 150)
                        {
                            // 상
                            SaveGame.Save<int>("TestRank", Random.Range(1, 10));
                        }
                        else if (SonManager.SonData.Intelligence < 50)
                        {
                            // 하
                            SaveGame.Save<int>("TestRank", Random.Range(15, 30));
                        }
                        else
                        {
                            // 중
                            SaveGame.Save<int>("TestRank", Random.Range(10, 20));
                        }
                        //11.29
                        break;
                    case 40502:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "2학기 시험";
                        eventData.context = "아들에게 좋은 성적을 받았으면 좋겠다고 말했습니다.";
                        eventData.increaseAttributes.Add("스트레스", 200);
                        eventData.resultSeq = 40505;
                        eventData.resultDelayedDay = 7;
                        // 시험 등수 계산
                        int testRank4 = SaveGame.Load<int>("TestRank") - Random.Range(0, 5);
                        if (testRank4 < 1) testRank4 = 1;
                        SaveGame.Save<int>("TestRank", testRank4);
                        //데이터 값 초기화                       
                        //SaveGame.Delete("TestRank");                       
                        break;
                    case 40503:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "2학기 시험";
                        eventData.context = "아들에게 부담갖지 말라고 말했습니다.";
                        eventData.resultSeq = 40505;
                        eventData.resultDelayedDay = 7;
                        // 시험 등수 계산
                        int testRank5 = SaveGame.Load<int>("TestRank") - Random.Range(0, 5);
                        if (testRank5 < 1) testRank5 = 1;
                        SaveGame.Save<int>("TestRank", testRank5);
                        //데이터 값 초기화                       
                        //SaveGame.Delete("TestRank");
                        break;
                    case 40504:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "2학기 시험";
                        eventData.context = "아들이 하고 싶은대로 내버려두었습니다.";
                        eventData.resultSeq = 40505;
                        eventData.resultDelayedDay = 7;
                        eventData.increaseAttributes.Add("스트레스", -50);
                        // 시험 등수 계산
                        int testRank6 = SaveGame.Load<int>("TestRank") - Random.Range(0, 5);
                        if (testRank6 < 1) testRank6 = 1;
                        SaveGame.Save<int>("TestRank", testRank6);
                        //데이터 값 초기화                       
                        //SaveGame.Delete("TestRank");                       
                        break;
                    case 40505:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "2학기 시험";
                        eventData.context = "2학기 시험이 시작되었습니다.";
                        eventData.resultSeq = 40601;
                        eventData.resultDelayedDay = 7;
                        break;
                    case 40601:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "시험 결과";
                        eventData.context = "시험 결과가 나왔습니다. 아들은 과에서" + SaveGame.Load<int>("TestRank") + "등을 했습니다.";
                        //시험결과
                        if (SaveGame.Load<int>("TestRank") <= 5)
                        {//상
                            eventData.increaseAttributes.Add("지식평가", 2);
                        }
                        else if (SaveGame.Load<int>("TestRank") >= 20)
                        {//하
                            eventData.increaseAttributes.Add("지식평가", -1);
                            if (SonManager.SonData.Education <= 10) SonManager.SonData.Education = 10;
                        }
                        else
                        {//중
                            eventData.increaseAttributes.Add("지식평가", 1);
                        }
                        break;
                    case 40701:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "종강";
                        eventData.context = "오늘부로 종강입니다.";
                        eventData.increaseAttributes.Add("스트레스", -100);
                        //학교가기 스케쥴 해제(오전)
                        ScheduleManager.Instance.RemoveSchedule(Day.Morning);
                        //12.13
                        break;
                    case 40801:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "복학";
                        eventData.context = "아들이 복학하여 다시 학교로 돌아왔습니다";
                        //학교가기 스케쥴 장착(오전)
                        ScheduleManager.Instance.SetSchedule(Day.Morning, ScheduleName.학교가기, true);
                        break;
                    case 40901:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Default;
                        eventData.subject = "1학기 시험";
                        eventData.context = "다음 주 부터 7일 간 학기 시험이 시작됩니다.";
                        eventData.choiceLength = 4;
                        eventData.choiceText1 = "좋은 성적을 요구한다..";
                        eventData.choiceSeq1 = 40902;
                        eventData.choiceDelayedDay1 = 0;
                        eventData.choiceText2 = "부담갖지 말라고 한다.";
                        eventData.choiceSeq2 = 40903;
                        eventData.choiceDelayedDay2 = 0;
                        eventData.choiceText3 = "신경쓰지 않는다.";
                        eventData.choiceSeq3 = 40904;
                        eventData.choiceDelayedDay3 = 0;
                        //상하중으로 지능 능력치에 따라 시험 등수 계산
                        if (SonManager.SonData.Intelligence > 150)
                        {
                            // 상
                            SaveGame.Save<int>("TestRank", Random.Range(1, 10));
                        }
                        else if (SonManager.SonData.Intelligence < 50)
                        {
                            // 하
                            SaveGame.Save<int>("TestRank", Random.Range(15, 30));
                        }
                        else
                        {
                            // 중
                            SaveGame.Save<int>("TestRank", Random.Range(10, 20));
                        }
                        break;
                    case 40902:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "1학기 시험";
                        eventData.context = "아들에게 좋은 성적을 받았으면 좋겠다고 말했습니다.";
                        eventData.increaseAttributes.Add("스트레스", 200);
                        eventData.increaseAttributes.Add("친밀도", -100);
                        eventData.resultSeq = 40905;
                        eventData.resultDelayedDay = 7;
                        // 시험 등수 계산
                        int testRank7 = SaveGame.Load<int>("TestRank") - Random.Range(0, 5);
                        if (testRank7 < 1) testRank7 = 1;
                        SaveGame.Save<int>("TestRank", testRank7);
                        //데이터 값 초기화                       
                        //SaveGame.Delete("TestRank");       
                        break;
                    case 40903:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "1학기 시험";
                        eventData.context = "아들에게 부담갖지 말라고 말했습니다.";
                        eventData.resultSeq = 40905;
                        eventData.resultDelayedDay = 7;
                        // 시험 등수 계산
                        int testRank8 = SaveGame.Load<int>("TestRank") - Random.Range(0, 5);
                        if (testRank8 < 1) testRank8 = 1;
                        SaveGame.Save<int>("TestRank", testRank8);
                        //데이터 값 초기화                       
                        //SaveGame.Delete("TestRank");       
                        break;
                    case 40904:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "1학기 시험";
                        eventData.context = "아들이 하고 싶은대로 내버려두었습니다.";
                        eventData.resultSeq = 40905;
                        eventData.resultDelayedDay = 7;
                        eventData.increaseAttributes.Add("스트레스", -50);
                        // 시험 등수 계산
                        int testRank9 = SaveGame.Load<int>("TestRank") - Random.Range(0, 5);
                        if (testRank9 < 1) testRank9 = 1;
                        SaveGame.Save<int>("TestRank", testRank9);
                        //데이터 값 초기화                       
                        //SaveGame.Delete("TestRank");       
                        //성적이 나빠질 수 있는 가능성 입력
                        break;
                    case 40905:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "1학기 시험";
                        eventData.context = "1학기 시험이 시작되었습니다.";
                        eventData.resultSeq = 41001;
                        eventData.resultDelayedDay = 7;
                        break;
                    case 41001:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "시험 결과";
                        eventData.context = "시험 결과가 나왔습니다. 아들은 과에서" + SaveGame.Load<int>("TestRank") + "등을 했습니다.";
                        //시험 점수 입력해야함
                        //테스트 결과 상.중.하에 따라 스트레스 변동 if{}                                                
                        break;
                    case 41101:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "종강";
                        eventData.context = "오늘부로 종강입니다.41101";
                        eventData.increaseAttributes.Add("스트레스", -100);
                        //학교가기 스케쥴 해제(오전)
                        break;
                    case 41102:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "개강";
                        eventData.context = "내일부터 개강입니다.";
                        eventData.increaseAttributes.Add("스트레스", 100);
                        //학교가기 스케쥴 장착(오전)
                        break;
                    case 41201:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Default;
                        eventData.subject = "2학기 시험";
                        eventData.context = "다음 주 부터 7일 간 2학기 시험이 시작됩니다.";
                        eventData.choiceLength = 4;
                        eventData.choiceText1 = "좋은 성적을 요구한다..";
                        eventData.choiceSeq1 = 41202;
                        eventData.choiceDelayedDay1 = 0;
                        eventData.choiceText2 = "부담갖지 말라고 한다.";
                        eventData.choiceSeq2 = 41203;
                        eventData.choiceDelayedDay2 = 0;
                        eventData.choiceText3 = "신경쓰지 않는다.";
                        eventData.choiceSeq3 = 41204;
                        eventData.choiceDelayedDay3 = 0;
                        //상하중으로 지능 능력치에 따라 시험 등수 계산
                        if (SonManager.SonData.Intelligence > 150)
                        {
                            // 상
                            SaveGame.Save<int>("TestRank", Random.Range(1, 10));
                        }
                        else if (SonManager.SonData.Intelligence < 50)
                        {
                            // 하
                            SaveGame.Save<int>("TestRank", Random.Range(15, 30));
                        }
                        else
                        {
                            // 중
                            SaveGame.Save<int>("TestRank", Random.Range(10, 20));
                        }
                        break;
                    case 41202:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "2학기 시험";
                        eventData.context = "아들에게 좋은 성적을 받았으면 좋겠다고 말했습니다.";
                        eventData.increaseAttributes.Add("스트레스", 200);
                        eventData.increaseAttributes.Add("친밀도", -100);
                        eventData.resultSeq = 41205;
                        // 시험 등수 계산
                        int testRank10 = SaveGame.Load<int>("TestRank") - Random.Range(0, 5);
                        if (testRank10 < 1) testRank10 = 1;
                        SaveGame.Save<int>("TestRank", testRank10);
                        //데이터 값 초기화                       
                        //SaveGame.Delete("TestRank");       
                        break;
                    case 41203:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "2학기 시험";
                        eventData.context = "아들에게 부담갖지 말라고 말했습니다.";
                        eventData.resultSeq = 41205;
                        eventData.resultDelayedDay = 7;
                        // 시험 등수 계산
                        int testRank11 = SaveGame.Load<int>("TestRank") - Random.Range(0, 5);
                        if (testRank11 < 1) testRank11 = 1;
                        SaveGame.Save<int>("TestRank", testRank11);
                        //데이터 값 초기화                       
                        //SaveGame.Delete("TestRank");       
                        break;
                    case 41204:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "2학기 시험";
                        eventData.context = "아들이 하고 싶은대로 내버려두었습니다.";
                        eventData.resultSeq = 41205;
                        eventData.resultDelayedDay = 7;
                        eventData.increaseAttributes.Add("스트레스", -50);
                        // 시험 등수 계산
                        int testRank12 = SaveGame.Load<int>("TestRank") - Random.Range(0, 5);
                        if (testRank12 < 1) testRank12 = 1;
                        SaveGame.Save<int>("TestRank", testRank12);
                        //데이터 값 초기화                       
                        //SaveGame.Delete("TestRank");       
                        break;
                    case 41205:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "2학기 시험";
                        eventData.context = "2학기 시험이 시작되었습니다.";
                        eventData.resultSeq = 41301;
                        eventData.resultDelayedDay = 7;
                        //시작과 함께 시험 결과 정보를 가지고 있어야 함
                        break;
                    case 41301:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "시험 결과";
                        eventData.context = "시험 결과가 나왔습니다. 아들은 과에서" + SaveGame.Load<int>("TestRank") + "등을 했습니다.";
                        //시험결과
                        if (SaveGame.Load<int>("TestRank") <= 5)
                        {//상
                            eventData.increaseAttributes.Add("지식평가", 2);
                        }
                        else if (SaveGame.Load<int>("TestRank") >= 20)
                        {//하
                            eventData.increaseAttributes.Add("지식평가", -1);
                            if (SonManager.SonData.Education <= 10) SonManager.SonData.Education = 10;
                        }
                        else
                        {//중
                            eventData.increaseAttributes.Add("지식평가", 1);
                        }
                        break;
                    case 41401:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "종강";
                        eventData.context = "오늘부로 종강입니다.";
                        eventData.increaseAttributes.Add("스트레스", -10);
                        //학교가기 스케쥴 해제(오전)
                        ScheduleManager.Instance.RemoveSchedule(Day.Morning);
                        break;
                    case 41501:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Default;
                        eventData.subject = "졸업식";
                        eventData.context = "오늘은 아들의 졸업식이 있는 날입니다.";
                        eventData.choiceLength = 2;
                        eventData.choiceText1 = "졸업식에 간다.";
                        eventData.choiceSeq1 = 41502;
                        eventData.choiceDelayedDay1 = 0;
                        eventData.choiceText2 = "졸업식에 가지 않는다.";
                        eventData.choiceSeq2 = 41503;
                        eventData.choiceDelayedDay2 = 0;
                        break;
                    case 41502:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "졸업식";
                        eventData.context = "아들의 졸업식에 참석했습니다.";
                        eventData.increaseAttributes.Add("친밀도", 50);
                        SonManager.SonData.CurrentJob = Job.백수;
                        break;
                    case 41503:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "졸업식";
                        eventData.context = "아들의 졸업식에 참석하지 않았습니다.";
                        eventData.increaseAttributes.Add("친밀도", -50);
                        SonManager.SonData.CurrentJob = Job.백수;
                        break;



                    case 50101:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "입대";
                        eventData.context = "영장이 나왔습니다. 아들은" + "n" + "급으로 현역 입대하게 되었습니다. 학교에는 휴학계를 냈습니다.";
                        eventData.increaseAttributes.Add("스트레스", 1000);
                        SonManager.SonData.CurrentJob = Job.군인;
                        //주유소가 아무 이벤트도 없는 스케쥴이기 때문에 군인 대신 더미로 입력
                        ScheduleManager.Instance.SetSchedule(Day.Morning, ScheduleName.주유소, true);
                        ScheduleManager.Instance.SetSchedule(Day.Evening, ScheduleName.주유소, true);
                        ScheduleManager.Instance.SetSchedule(Day.Night, ScheduleName.주유소, true);
                        break;
                    default:
                        _shouldFindEvent = true;
                        break;
                }
                break;
            case Job.군인:
                switch (eventSeq)
                {
                    case 50201:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "전역";
                        eventData.context = "아들은 몸 건강히 복무를 마치고 현역만기 제대로 집에 돌아왔습니다.";
                        eventData.increaseAttributes.Add("스트레스", -1000);
                        SonManager.SonData.CurrentJob = Job.대학생;
                        SonManager.SonData.MilitaryServiceStatus = true;
                        SaveGame.Save<bool>("MilitaryServiceStatus", true);
                        //bool 변수명 = SaveGame.Load<bool>("MilitaryServiceStatus");
                        //군대 값이 true로 바뀌면서 복학 이후 이벤트 가능                        
                        break;
                    default:
                        _shouldFindEvent = true;
                        break;
                }
                break;
            case Job.백수:
                switch (eventSeq)
                {
                    case 70101:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "이력서";
                        eventData.context = "아들이 취업을 위해 이력서를 작성하고 있습니다.";
                        eventData.resultSeq = 70102;
                        eventData.resultDelayedDay = 7;
                        eventData.increaseAttributes.Add("스트레스", 50);
                        SaveGame.Save<bool>("HasResume", true);
                        //확률이벤트로 작성할 것
                        break;
                    case 70102:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Default;
                        eventData.subject = "면접";
                        eventData.context = "아들은 몇 몇 군데에서 면접을 보러 오라는 연락을 받았습니다. 아들은 어디로 가야할 지 고민중입니다. ";
                        eventData.choiceLength = 4;
                        eventData.choiceText1 = "등대처럼 밝은 대기업" + " 면접을 추천한다.";
                        eventData.choiceSeq1 = 70103;
                        eventData.choiceDelayedDay1 = 0;
                        eventData.choiceText2 = "다정다감 가족같은 회사" + " 면접을 추천한다.";
                        eventData.choiceSeq2 = 70103;
                        eventData.choiceDelayedDay2 = 0;
                        eventData.choiceText3 = "열정넘치는 스타트업" + " 면접을 추천한다.";
                        eventData.choiceSeq3 = 70103;
                        eventData.choiceDelayedDay3 = 0;
                        eventData.choiceText4 = "아무데도 추천하지 않는다.";
                        eventData.choiceSeq4 = 70106;
                        eventData.choiceDelayedDay4 = 0;
                        //회사명 입력
                        //회사명에 따른 필요 능력 조건 리스트업
                        int num1 = Random.Range(0, 10);
                        int num2 = num1;
                        while (num1 == num2)
                        {
                            num2 = Random.Range(0, 10);
                        }
                        int num3 = num2;
                        while (num3 == num1 || num3 == num2)
                        {
                            num3 = Random.Range(0, 10);
                        }

                        FindCompanyEventData(num1);
                        eventData.choiceText1 = choiceText;
                        eventData.choiceSeq1 = choiceSeq;
                        eventData.choiceDelayedDay1 = choiceDelayedDay;

                        FindCompanyEventData(num2);
                        eventData.choiceText2 = choiceText;
                        eventData.choiceSeq2 = choiceSeq;
                        eventData.choiceDelayedDay2 = choiceDelayedDay;

                        FindCompanyEventData(num3);
                        eventData.choiceText3 = choiceText;
                        eventData.choiceSeq3 = choiceSeq;
                        eventData.choiceDelayedDay3 = choiceDelayedDay;
                        break;
                    case 70103:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "면접";
                        eventData.context = "아들은 추천에 따라" + SaveGame.Load<string>("CompanyName") + "에 면접을 보러 갔습니다.";
                        //회사명 입력
                        if (SaveGame.Load<bool>("Pass"))
                        {
                            eventData.resultSeq = 70104;
                        }
                        else
                        {
                            eventData.resultSeq = 70105;
                        }
                        eventData.resultDelayedDay = 7;
                        //더미로 만들어서 제작                                
                        break;
                    case 70104:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "면접";
                        eventData.context = "아들은 자신감 있게 면접을 마쳤습니다.";
                        eventData.resultSeq = 70201;
                        eventData.resultDelayedDay = 7;

                        break;
                    case 70105:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "면접";
                        eventData.context = "아들은 면접 내내 실수를 연발했습니다.";
                        eventData.resultSeq = 70101;
                        eventData.resultDelayedDay = 7;

                        break;
                    case 70106:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "면접";
                        eventData.context = "아들은 아무 곳에도 가지 않기로 했습니다.";
                        eventData.resultSeq = 70101;
                        eventData.resultDelayedDay = 7;

                        break;
                    case 70201:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = "취직";
                        eventData.context = "축하합니다. 아들이 취업에 성공했습니다.";
                        eventData.resultSeq = 70202;
                        eventData.resultDelayedDay = 3;
                        break;
                    case 70202:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = SaveGame.Load<string>("CompanyName"); //회사이름
                        eventData.context = "아들의 첫 출근 날입니다.";
                        eventData.resultSeq = 70203;
                        eventData.resultDelayedDay = 30;
                        SonManager.SonData.CurrentJob = Job.직장인;
                        break;
                    default:
                        _shouldFindEvent = true;
                        break;
                }
                break;
            case Job.직장인:
                switch (eventSeq)
                {
                    case 70203:
                        eventData.imagePath = "Entrance ceremony2";
                        eventData.eventType = EventType.Result;
                        eventData.subject = SaveGame.Load<string>("CompanyName");
                        eventData.context = "첫 월급을 받았습니다.";
                        eventData.resultSeq = 80101;
                        eventData.resultDelayedDay = 0;
                        break;
                    case 80101:
                        string companyName = SaveGame.Load<string>("CompanyName");
                        string endingText = null;
                        switch (companyName)
                        {
                            case "연 매출 1위의 대기업":
                            case "보수적인 대기업":
                            case "100년 전통의 대기업":
                                endingText = "뛰어난 능력으로 회사원이 된 아들은 사내에서 높은 평가를 받으며 승승장구 했습니다. 몇년 간의 연애를 끝으로 결혼에 골인 한 아들은 얼마지나지 않아 아내가 임신했다는 소식을 듣게 되었습니다. 그리고 이듬해, 아들은 노력끝에 마침내 아이와 함께 지낼 자기 소유의 집도 한 채 마련할 수 있게 되었습니다.";
                                break;
                            case "가족같은 분위기의 중소기업":
                            case "튼실한 중견기업":
                            case "복지 좋은 중소기업":
                                endingText = "취업에 성공한 아들은 이후 성실함을 바탕으로 오랫동안 회사에 이바지했습니다. 일과가 끝난 후에 마시는 맥주 한 잔에 즐거워하고, 주말에는 가족들과 나들이를 가며 화목한 시간을 보내기도 했습니다. 가끔씩 안 좋은 일도 있었지만, 크게 걱정할 만한 거리는 아니었습니다. 그야말로, 평범한 인생이었습니다.";
                                break;
                            case "이제 막 개업한 법인 사무실":
                            case "작년에 주식상장 된 벤처기업":
                            case "열정넘치는 스타트업":
                                endingText = "어렵사리 회사원이 된 아들은 자신의 열정을 바탕으로 회사에서 열심히 활약했습니다. 열정만 쓰다가 바닥나 버릴거라는 주변의 우려에도 아랑곳않고 열심히 달린 아들은, 회사의 중역까지 올라 회사를 어엿한 기업으로 성장시켰습니다. 힘든 일도 많이 있었지만, 아들의 마음은 보람으로 가득찼습니다.";
                                break;
                        }
                        SaveGame.Save<string>("EndingTitle", companyName);
                        SaveGame.Save<string>("EndingContext", endingText);
                        SceneManager.LoadScene("Ending");
                        // eventData.imagePath = "Entrance ceremony2";
                        // eventData.eventType = EventType.Result;
                        // eventData.subject = "엔딩";
                        // eventData.context = "아들은" + SaveGame.Load<string>("CompanyName") + "에 취직하여 튀지도 않고 모나지도 않은 평범한 회사원이 되었습니다. 이렇다할 자격증은 없었고 회사에서 가끔은 실수를 하는 일도 있었지만 커다란 문제아니었습니다. 아들은 당신이 원하는 모습이 되었나요? 아니면... 다른 모습인가요?";
                        //엔딩스크립트
                        break;
                    default:
                        _shouldFindEvent = true;
                        break;
                }
                break;
        }
        if (_shouldFindEvent)
        {
            switch (eventSeq)
            {
                //교육
                //100=1
                case 90101:
                    eventData.imagePath = "Entrance ceremony2";
                    eventData.eventType = EventType.Result;
                    eventData.subject = "학교";
                    eventData.context = "머리에 쏙쏙 들어옵니다! 평소보다 집중이 잘 된 것 같습니다.";
                    eventData.increaseAttributes.Add("지능", 50);
                    break;
                case 90102:
                    eventData.imagePath = "Entrance ceremony2";
                    eventData.eventType = EventType.Result;
                    eventData.subject = "학교";
                    eventData.context = "공부하는 내내 딴 생각에 잠겨 집중할 수 없었습니다.";
                    eventData.increaseAttributes.Add("스트레스", 100);
                    break;
                case 90201:
                    eventData.imagePath = "Entrance ceremony2";
                    eventData.eventType = EventType.Result;
                    eventData.subject = "학원";
                    eventData.context = "오늘은 평소보다 공부가 잘 됐습니다.";
                    eventData.increaseAttributes.Add("지능", 50);
                    break;
                case 90202:
                    eventData.imagePath = "Entrance ceremony2";
                    eventData.eventType = EventType.Result;
                    eventData.subject = "학원";
                    eventData.context = "다른 친구들과 잡담을 하다 학원 선생님에게 혼이 났습니다.";
                    eventData.increaseAttributes.Add("스트레스", 100);
                    break;
                case 90301:
                    eventData.imagePath = "Entrance ceremony2";
                    eventData.eventType = EventType.Result;
                    eventData.subject = "과외";
                    eventData.context = "과외를 받은 보람이 있습니다. 오늘은 가르쳐주는 대로 이해가 잘 됐습니다.";
                    eventData.increaseAttributes.Add("지능", 50);
                    break;
                case 90302:
                    eventData.imagePath = "Entrance ceremony2";
                    eventData.eventType = EventType.Result;
                    eventData.subject = "과외";
                    eventData.context = "공부하는 내내 다른 생각이 들어 집중할 수 없었습니다.";
                    eventData.increaseAttributes.Add("지능", 50);
                    break;
                case 90401:
                    eventData.imagePath = "Entrance ceremony2";
                    eventData.eventType = EventType.Result;
                    eventData.subject = "스터디";
                    eventData.context = "서로 의기투합하여 공부에 매진했습니다.";
                    eventData.increaseAttributes.Add("지능", 50);
                    break;
                case 90402:
                    eventData.imagePath = "Entrance ceremony2";
                    eventData.eventType = EventType.Result;
                    eventData.subject = "스터디";
                    eventData.context = "서로 의기투합하여 수다떨기에 매진했습니다.";
                    eventData.increaseAttributes.Add("스트레스", -100);
                    break;
                case 90501:
                    eventData.imagePath = "Entrance ceremony2";
                    eventData.eventType = EventType.Result;
                    eventData.subject = "독서실";
                    eventData.context = "집중해서 공부했습니다. 오늘 하루도 보람차네요!";
                    eventData.increaseAttributes.Add("지능", 50);
                    break;
                case 90502:
                    eventData.imagePath = "Entrance ceremony2";
                    eventData.eventType = EventType.Result;
                    eventData.subject = "독서실";
                    eventData.context = "시끄럽게 떠드는 사람이 있어 방해를 받았습니다.";
                    eventData.increaseAttributes.Add("스트레스", 200);
                    break;
                case 90601:
                    eventData.imagePath = "Entrance ceremony2";
                    eventData.eventType = EventType.Result;
                    eventData.subject = "도서관";
                    eventData.context = "집중해서 공부했습니다. 오늘 하루도 보람차네요!";
                    eventData.increaseAttributes.Add("지능", 50);
                    break;
                case 90602:
                    eventData.imagePath = "Entrance ceremony2";
                    eventData.eventType = EventType.Result;
                    eventData.subject = "도서관";
                    eventData.context = "시끄럽게 떠드는 사람이 있어 방해를 받았습니다.";
                    eventData.increaseAttributes.Add("스트레스", 200);
                    break;

                //알바
                case 100101:
                    eventData.imagePath = "Entrance ceremony2";
                    eventData.eventType = EventType.Result;
                    eventData.subject = "봉사활동";
                    eventData.context = "수레를 끌며 언덕을 오르며 땀을 뻘 뻘 흘리고 있는 할아버지 한 분을 보게 되었습니다. 아들은 말없이 다가가 할아버지의 수레를 밀어드렸습니다.";
                    eventData.increaseAttributes.Add("Notion", 100);
                    //notion 능력치 검색 안됨
                    break;
                case 100201:
                    eventData.imagePath = "Entrance ceremony2";
                    eventData.eventType = EventType.Result;
                    eventData.subject = "레스토랑";
                    eventData.context = "서빙을 하던 중 실수를 해서 접시를 깨 뜨리고 말았습니다.";
                    eventData.increaseAttributes.Add("스트레스", 200);
                    break;
                case 100301:
                    eventData.imagePath = "Entrance ceremony2";
                    eventData.eventType = EventType.Result;
                    eventData.subject = "카페";
                    eventData.context = "손님 중 한 명이 전화번호를 물어봤습니다. 아들이 마음에 드는 모양입니다.";
                    eventData.increaseAttributes.Add("Charm", 50);
                    break;
                case 100401:
                    eventData.imagePath = "Entrance ceremony2";
                    eventData.eventType = EventType.Result;
                    eventData.subject = "편의점";
                    eventData.context = "술에 취해 들어와 고성방가를 하는 손님에게 시달려 진땀을 뺐습니다.";
                    eventData.increaseAttributes.Add("스트레스", 200);
                    break;
                case 100501:
                    eventData.imagePath = "Entrance ceremony2";
                    eventData.eventType = EventType.Result;
                    eventData.subject = "피시방";
                    eventData.context = "청소를 하던 중, 손님 한 명이 계산을 하지 않고 도망갔습니다.";
                    eventData.increaseAttributes.Add("스트레스", 200);
                    break;
                case 100601:
                    eventData.imagePath = "Entrance ceremony2";
                    eventData.eventType = EventType.Result;
                    eventData.subject = "패스트푸드";
                    eventData.context = "다른 직원들 보다 일을 잘한다며 매니저에게 칭찬을 받았습니다.";
                    eventData.increaseAttributes.Add("스트레스", -100);
                    eventData.increaseAttributes.Add("손재주", -50);
                    break;
                case 100701:
                    eventData.imagePath = "Entrance ceremony2";
                    eventData.eventType = EventType.Result;
                    eventData.subject = "전단지";
                    eventData.context = "오늘은 전단지를 받아주는 사람이 별로 없었습니다.";
                    eventData.increaseAttributes.Add("스트레스", 100);
                    break;
                case 100801:
                    eventData.imagePath = "Entrance ceremony2";
                    eventData.eventType = EventType.Result;
                    eventData.subject = "인형탈";
                    eventData.context = "쓰고 있는 인형탈이 너무 더웠나 봅니다. 현기증을 느끼다 쓰러졌습니다.";
                    eventData.increaseAttributes.Add("스트레스", 200);
                    eventData.increaseAttributes.Add("체력", -50);
                    break;
                case 100901:
                    eventData.imagePath = "Entrance ceremony2";
                    eventData.eventType = EventType.Result;
                    eventData.subject = "택배상하차";
                    eventData.context = "하루 종일 택배 상자를 옮기다가 그만 몸살이 났습니다.";
                    eventData.increaseAttributes.Add("스트레스", +200);
                    eventData.increaseAttributes.Add("체력", -100);
                    break;
                case 100902:
                    eventData.imagePath = "Entrance ceremony2";
                    eventData.eventType = EventType.Result;
                    eventData.subject = "택배상하차";
                    eventData.context = "일이 너무 힘든 나머지 아들은 참지 못하고 택배상하차장에서 도망쳤습니다.";
                    eventData.increaseAttributes.Add("스트레스", +200);
                    //eventData.increaseAttributes.Add("Notion", -20);
                    //notion 능력치 검색 안됨
                    break;

                //휴식
                case 110101:
                    eventData.imagePath = "Entrance ceremony2";
                    eventData.eventType = EventType.Result;
                    eventData.subject = "뒹굴거리기";
                    eventData.context = "집에서 푹 쉬었습니다.";
                    eventData.increaseAttributes.Add("스트레스", -100);
                    break;
                case 110102:
                    eventData.imagePath = "Entrance ceremony2";
                    eventData.eventType = EventType.Result;
                    eventData.subject = "뒹굴거리기";
                    eventData.context = "쉰 것 같지도 않은 날이었습니다. 피곤하기만 합니다.";
                    eventData.increaseAttributes.Add("스트레스", 100);
                    break;
                case 110201:
                    eventData.imagePath = "Entrance ceremony2";
                    eventData.eventType = EventType.Result;
                    eventData.subject = "친구 만나기";
                    eventData.context = "친구와 만나 즐거운 시간을 보냈습니다.";
                    eventData.increaseAttributes.Add("스트레스", 100);
                    break;
                case 110202:
                    eventData.imagePath = "Entrance ceremony2";
                    eventData.eventType = EventType.Result;
                    eventData.subject = "친구 만나기";
                    eventData.context = "친구와 만나 이야기를 하던 중 사소한 말 다툼이 있었습니다.";
                    eventData.increaseAttributes.Add("스트레스", 100);
                    break;
                case 110301:
                    eventData.imagePath = "Entrance ceremony2";
                    eventData.eventType = EventType.Result;
                    eventData.subject = "게임하기";
                    eventData.context = "게임에서 제멋대로인 트롤러를 만나 기분이 몹시 안 좋아 졌습니다.";
                    eventData.increaseAttributes.Add("스트레스", 100);
                    break;
                case 110302:
                    eventData.imagePath = "Entrance ceremony2";
                    eventData.eventType = EventType.Result;
                    eventData.subject = "게임하기";
                    eventData.context = "오늘은 게임이 잘 풀렸습니다. 펜타 킬! 전원 처치!";
                    eventData.increaseAttributes.Add("스트레스", -200);
                    eventData.increaseAttributes.Add("손재주", 50);
                    break;
                case 110401:
                    eventData.imagePath = "Entrance ceremony2";
                    eventData.eventType = EventType.Result;
                    eventData.subject = "운동하기";
                    eventData.context = "평소보다 컨디션이 좋아서인지 운동이 잘 되는 느낌입니다.";
                    eventData.increaseAttributes.Add("체력", 50);
                    break;
                case 110402:
                    eventData.imagePath = "Entrance ceremony2";
                    eventData.eventType = EventType.Result;
                    eventData.subject = "운동하기";
                    eventData.context = "평소보다 몸이 무겁습니다. 오늘 운동은 여기까지로 해야겠어요.";
                    eventData.increaseAttributes.Add("스트레스", 100);
                    break;
                case 110501:
                    eventData.imagePath = "Entrance ceremony2";
                    eventData.eventType = EventType.Result;
                    eventData.subject = "영화보기";
                    eventData.context = "오늘 본 영화는 너무 감동적이었습니다.";
                    eventData.increaseAttributes.Add("스트레스", -100);
                    eventData.increaseAttributes.Add("감수성", 50);
                    break;
                case 110502:
                    eventData.imagePath = "Entrance ceremony2";
                    eventData.eventType = EventType.Result;
                    eventData.subject = "영화보기";
                    eventData.context = "오늘 본 영화는 너무 지루했습니다.";
                    eventData.increaseAttributes.Add("스트레스", 100);
                    eventData.increaseAttributes.Add("감수성", -50);
                    break;
                case 110601:
                    eventData.imagePath = "Entrance ceremony2";
                    eventData.eventType = EventType.Result;
                    eventData.subject = "그림그리기";
                    eventData.context = "오늘은 그림이 제법 잘 그려진 것 같습니다.";
                    eventData.increaseAttributes.Add("감수성", 50);
                    break;
                case 110602:
                    eventData.imagePath = "Entrance ceremony2";
                    eventData.eventType = EventType.Result;
                    eventData.subject = "그림그리기";
                    eventData.context = "오늘 그림은 망작이네요, 망작. 유치원 생이 그려도 이것 보단 잘 그릴 것 같습니다.";
                    eventData.increaseAttributes.Add("감수성", -50);
                    break;
                case 110701:
                    eventData.imagePath = "Entrance ceremony2";
                    eventData.eventType = EventType.Result;
                    eventData.subject = "음악감상";
                    eventData.context = "음악을 듣던 중 특별한 영감이 떠올랐습니다. 멜로디가 귓가에서 속삭입니다.";
                    eventData.increaseAttributes.Add("감수성", 50);
                    break;
                case 110801:
                    eventData.imagePath = "Entrance ceremony2";
                    eventData.eventType = EventType.Result;
                    eventData.subject = "독서";
                    eventData.context = "문장들이 춤을 춥니다. 어쩜 이리 아름다운 글이 있을 수 있을까요.";
                    eventData.increaseAttributes.Add("감수성", 50);
                    break;
                case 110802:
                    eventData.imagePath = "Entrance ceremony2";
                    eventData.eventType = EventType.Result;
                    eventData.subject = "독서";
                    eventData.context = "이런 것도 문학이라고...? 끔찍한 작품이었습니다.";
                    eventData.increaseAttributes.Add("지능", 50);
                    eventData.increaseAttributes.Add("감수성", -50);
                    break;
                case 110901:
                    eventData.imagePath = "Entrance ceremony2";
                    eventData.eventType = EventType.Result;
                    eventData.subject = "맛집탐방";
                    eventData.context = "맛집 탐방을 하던 중 생각했던 것 보다 훨씬 맛있는 맛집을 발견했습니다. 기분이 좋아졌습니다.";
                    eventData.increaseAttributes.Add("스트레스", -100);
                    break;
                case 110902:
                    eventData.imagePath = "Entrance ceremony2";
                    eventData.eventType = EventType.Result;
                    eventData.subject = "맛집탐방";
                    eventData.context = "이번에 찾아간 맛집은 실망스러웠습니다. 그저 광고로 유명해진 곳이었군요. 역시 돈이면 다 되나 봅니다.";
                    eventData.increaseAttributes.Add("스트레스", 100);
                    break;
                case 111001:
                    eventData.imagePath = "Entrance ceremony2";
                    eventData.eventType = EventType.Result;
                    eventData.subject = "종교활동";
                    eventData.context = "종교활동을 하던 중 무언가 계시를 받은 것 같은 기분이 들었습니다. 가슴이 벅차 오릅니다.";
                    eventData.increaseAttributes.Add("스트레스", -100);
                    break;
                case 111002:
                    eventData.imagePath = "Entrance ceremony2";
                    eventData.eventType = EventType.Result;
                    eventData.subject = "종교활동";
                    eventData.context = "신은 정말 존재하는 것일까요...? 아들의 마음속에 한 가지 의문이 생겼습니다. 악마의 소행인지도 모릅니다!";
                    eventData.increaseAttributes.Add("스트레스", 200);
                    break;
            }
        }
        return eventData;
    }

    public void FindCompanyEventData(int number)
    {
        bool isPass = false;
        int passFactor = 0;
        int randomNumber1000 = 0;
        switch (number)
        {
            case 0:
                //기준 능력치 일때 80% 이상으로 결정
                //상, 18 18 일 때 72%
                //0.1~25=상 / 0.25~35=중 / 나머지 = 하
                choiceText = "연 매출 1위의 대기업" + " 면접을 추천한다.";
                choiceSeq = 70103;
                choiceDelayedDay = 0;
                SaveGame.Save<string>("CompanyName", "연 매출 1위의 대기업");
                isPass = false;

                passFactor = 0;
                passFactor += (SonManager.SonData.Education * 20);
                passFactor += (int)(SonManager.SonData.Intelligence * 0.2);
                randomNumber1000 = Random.Range(0, 1000);
                if (passFactor >= randomNumber1000)
                {
                    isPass = true;
                }
                SaveGame.Save<bool>("Pass", isPass);
                //bool ttest = SaveGame.Load<bool>("Pass");
                //if(SaveGame.Load<bool>("Pass") == true)
                break;
            case 1:
                //기준 능력치 일때 80% 이상으로 결정
                //중, 13 13일때 78%           
                choiceText = "가족같은 분위기의 중소기업" + " 면접을 추천한다.";
                choiceSeq = 70103;
                choiceDelayedDay = 0;
                SaveGame.Save<string>("CompanyName", "가족같은 분위기의 중소기업");
                isPass = false;

                passFactor = 0;
                passFactor += (SonManager.SonData.Education * 30);
                passFactor += (int)(SonManager.SonData.Intelligence * 0.3);
                randomNumber1000 = Random.Range(0, 1000);
                if (passFactor >= randomNumber1000)
                {
                    isPass = true;
                }
                SaveGame.Save<bool>("Pass", isPass);
                break;
            case 2:
                //기준 능력치 일때 80% 이상으로 결정
                //중, 10 10일때 70%  
                choiceText = "열정넘치는 스타트업" + " 면접을 추천한다.";
                choiceSeq = 70103;
                choiceDelayedDay = 0;
                SaveGame.Save<string>("CompanyName", "열정넘치는 스타트업");
                isPass = false;

                passFactor = 0;
                passFactor += (SonManager.SonData.Education * 35);
                passFactor += (int)(SonManager.SonData.Intelligence * 0.35);
                randomNumber1000 = Random.Range(0, 1000);
                if (passFactor >= randomNumber1000)
                {
                    isPass = true;
                }
                SaveGame.Save<bool>("Pass", isPass);
                break;
            case 4:
                choiceText = "튼실한 중견기업" + " 면접을 추천한다.";
                choiceSeq = 70103;
                choiceDelayedDay = 0;
                SaveGame.Save<string>("CompanyName", "튼실한 중견기업");
                isPass = false;

                passFactor = 0;
                passFactor += (SonManager.SonData.Education * 25);
                passFactor += (int)(SonManager.SonData.Intelligence * 0.25);
                randomNumber1000 = Random.Range(0, 1000);
                if (passFactor >= randomNumber1000)
                {
                    isPass = true;
                }
                SaveGame.Save<bool>("Pass", isPass);
                break;
            case 5:
                choiceText = "100년 전통의 대기업" + " 면접을 추천한다.";
                choiceSeq = 70103;
                choiceDelayedDay = 0;
                SaveGame.Save<string>("CompanyName", "100년 전통의 대기업");
                isPass = false;

                passFactor = 0;
                passFactor += (SonManager.SonData.Education * 20);
                passFactor += (int)(SonManager.SonData.Intelligence * 0.2);
                randomNumber1000 = Random.Range(0, 1000);
                if (passFactor >= randomNumber1000)
                {
                    isPass = true;
                }
                SaveGame.Save<bool>("Pass", isPass);
                break;
            case 6:
                choiceText = "작년에 주식상장 된 벤처기업" + " 면접을 추천한다.";
                choiceSeq = 70103;
                choiceDelayedDay = 0;
                SaveGame.Save<string>("CompanyName", "작년에 주식상장 된 벤처기업");
                isPass = false;

                passFactor = 0;
                passFactor += (SonManager.SonData.Education * 30);
                passFactor += (int)(SonManager.SonData.Intelligence * 0.3);
                randomNumber1000 = Random.Range(0, 1000);
                if (passFactor >= randomNumber1000)
                {
                    isPass = true;
                }
                SaveGame.Save<bool>("Pass", isPass);
                break;
            case 7:
                choiceText = "보수적인 중견기업" + " 면접을 추천한다.";
                choiceSeq = 70103;
                choiceDelayedDay = 0;
                SaveGame.Save<string>("CompanyName", "보수적인 중견기업");
                isPass = false;

                passFactor = 0;
                passFactor += (SonManager.SonData.Education * 25);
                passFactor += (int)(SonManager.SonData.Intelligence * 0.25);
                randomNumber1000 = Random.Range(0, 1000);
                if (passFactor >= randomNumber1000)
                {
                    isPass = true;
                }
                SaveGame.Save<bool>("Pass", isPass);
                break;
            case 8:
                choiceText = "이제 막 개업한 법인 사무실" + " 면접을 추천한다.";
                choiceSeq = 70103;
                choiceDelayedDay = 0;
                SaveGame.Save<string>("CompanyName", "이제 막 개업한 법인 사무실");
                isPass = false;

                passFactor = 0;
                passFactor += (SonManager.SonData.Education * 42);
                passFactor += (int)(SonManager.SonData.Intelligence * 0.42);
                randomNumber1000 = Random.Range(0, 1000);
                if (passFactor >= randomNumber1000)
                {
                    isPass = true;
                }
                SaveGame.Save<bool>("Pass", isPass);
                break;
            case 9:
                choiceText = "복지 좋은 중소기업" + " 면접을 추천한다.";
                choiceSeq = 70103;
                choiceDelayedDay = 0;
                SaveGame.Save<string>("CompanyName", "복지 좋은 중소기업");
                isPass = false;

                passFactor = 0;
                passFactor += (SonManager.SonData.Education * 30);
                passFactor += (int)(SonManager.SonData.Intelligence * 0.3);
                randomNumber1000 = Random.Range(0, 1000);
                if (passFactor >= randomNumber1000)
                {
                    isPass = true;
                }
                SaveGame.Save<bool>("Pass", isPass);
                break;
        }
    }
}
