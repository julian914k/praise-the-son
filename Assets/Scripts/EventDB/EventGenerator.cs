﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BayatGames.SaveGameFree;

public class EventGenerator : MonoBehaviour
{
    public static EventGenerator Instance = null;
    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }

    // 일반 확률 이벤트 추가
    public void AddGeneralProbabilityEvent(List<ProbabilityEventData> eventProbabilityDatas)
    {
        //여기서 아들의 능력치 등을 참조해, 조건에 맞지 않는 확률 이벤트는 ADD 하지 않도록 한다.
        // 아들은 SonData로 참조 확률은 총합 1000
        //eventProbabilityDatas.Add(new ProbabilityEventData(101011, 300));
        //eventProbabilityDatas.Add(new ProbabilityEventData(111, 50));
        //eventProbabilityDatas.Add(new ProbabilityEventData(121, 50));
    }

    // 스케쥴 확률 이벤트 추가
    public void AddScheduleProbabilityEvent(List<ProbabilityEventData> eventProbabilityDatas)
    {
        // 스케쥴에 따른 이벤트 발생
        foreach (KeyValuePair<Day, ScheduleName> schedule in ScheduleManager.Instance.Schedules)
        {
            switch (schedule.Value)
            {
                case ScheduleName.학교가기:
                    //교육
                    //100=10%
                    eventProbabilityDatas.Add(new ProbabilityEventData(90101, 5));
                    eventProbabilityDatas.Add(new ProbabilityEventData(90102, 5));
                    break;
                case ScheduleName.학원가기:
                    eventProbabilityDatas.Add(new ProbabilityEventData(90201, 5));
                    eventProbabilityDatas.Add(new ProbabilityEventData(90202, 5));
                    break;
                case ScheduleName.과외:
                    eventProbabilityDatas.Add(new ProbabilityEventData(90301, 5));
                    eventProbabilityDatas.Add(new ProbabilityEventData(90302, 5));
                    break;
                case ScheduleName.스터디그룹:
                    eventProbabilityDatas.Add(new ProbabilityEventData(90401, 5));
                    eventProbabilityDatas.Add(new ProbabilityEventData(90402, 5));
                    break;
                case ScheduleName.도서관:
                    eventProbabilityDatas.Add(new ProbabilityEventData(90601, 5));
                    eventProbabilityDatas.Add(new ProbabilityEventData(90602, 5));
                    break;
                //알바
                case ScheduleName.봉사활동:
                    eventProbabilityDatas.Add(new ProbabilityEventData(100101, 100));
                    break;
                case ScheduleName.레스토랑:
                    eventProbabilityDatas.Add(new ProbabilityEventData(100201, 5));
                    break;
                case ScheduleName.카페:
                    eventProbabilityDatas.Add(new ProbabilityEventData(100310, 5));
                    break;
                case ScheduleName.편의점:
                    eventProbabilityDatas.Add(new ProbabilityEventData(100401, 5));
                    break;
                case ScheduleName.피시방:
                    eventProbabilityDatas.Add(new ProbabilityEventData(100501, 5));
                    break;
                case ScheduleName.패스트푸드:
                    eventProbabilityDatas.Add(new ProbabilityEventData(100601, 5));
                    break;
                case ScheduleName.전단지:
                    eventProbabilityDatas.Add(new ProbabilityEventData(100701, 5));
                    break;
                case ScheduleName.인형탈:
                    eventProbabilityDatas.Add(new ProbabilityEventData(100801, 5));
                    break;
                case ScheduleName.택배상하차:
                    eventProbabilityDatas.Add(new ProbabilityEventData(100901, 100));
                    eventProbabilityDatas.Add(new ProbabilityEventData(100902, 100));
                    break;
                //휴식
                case ScheduleName.집에서휴식:
                    eventProbabilityDatas.Add(new ProbabilityEventData(110101, 5));
                    eventProbabilityDatas.Add(new ProbabilityEventData(110102, 5));
                    break;
                case ScheduleName.친구만나기:
                    eventProbabilityDatas.Add(new ProbabilityEventData(110201, 5));
                    eventProbabilityDatas.Add(new ProbabilityEventData(110202, 5));
                    break;
                case ScheduleName.게임하기:
                    eventProbabilityDatas.Add(new ProbabilityEventData(110301, 5));
                    eventProbabilityDatas.Add(new ProbabilityEventData(110302, 5));
                    break;
                case ScheduleName.운동하기:
                    eventProbabilityDatas.Add(new ProbabilityEventData(110401, 5));
                    eventProbabilityDatas.Add(new ProbabilityEventData(110402, 5));
                    break;
                case ScheduleName.영화보기:
                    eventProbabilityDatas.Add(new ProbabilityEventData(110501, 5));
                    eventProbabilityDatas.Add(new ProbabilityEventData(110502, 5));
                    break;
                case ScheduleName.그림그리기:
                    eventProbabilityDatas.Add(new ProbabilityEventData(110601, 5));
                    eventProbabilityDatas.Add(new ProbabilityEventData(110602, 5));
                    break;
                case ScheduleName.음악듣기:
                    eventProbabilityDatas.Add(new ProbabilityEventData(110701, 5));
                    break;
                case ScheduleName.독서하기:
                    eventProbabilityDatas.Add(new ProbabilityEventData(110801, 5));
                    eventProbabilityDatas.Add(new ProbabilityEventData(110802, 5));
                    break;
                case ScheduleName.맛집탐방:
                    eventProbabilityDatas.Add(new ProbabilityEventData(110901, 5));
                    eventProbabilityDatas.Add(new ProbabilityEventData(110902, 5));
                    break;
                case ScheduleName.종교활동:
                    eventProbabilityDatas.Add(new ProbabilityEventData(111001, 5));
                    eventProbabilityDatas.Add(new ProbabilityEventData(111002, 5));
                    break;
            }



            // switch (schedule.Value)
            // {
            //  case ScheduleName.봉사활동:
            // if (SonManager.SonData.Physical <= 50) { }

            //  break;
            // if (SonManager.SonData.CurrentPersonalityTrait1 == PersonalityTrait.내성적
            // || SonManager.SonData.CurrentPersonalityTrait2 == PersonalityTrait.내성적
            // || SonManager.SonData.CurrentPersonalityTrait3 == PersonalityTrait.내성적
            // || SonManager.SonData.CurrentPersonalityTrait4 == PersonalityTrait.내성적)
            // eventProbabilityDatas.Add(new ProbabilityEventData(101, 50));
            // case ScheduleName.학교가기:
            //     break;
            //  }
        }
    }

    // 직업 확률 이벤트 추가
    public void AddJobProbabilityEvent(List<ProbabilityEventData> eventProbabilityDatas)
    {
        if (SonManager.SonData.CurrentJob == Job.백수)
        {
            if (!SaveGame.Load<bool>("HasResume"))
            {
                eventProbabilityDatas.Add(new ProbabilityEventData(70101, 50));
            }
        }

        // 현재 오전 스케쥴을 조건으로 지정하고 싶을때
        //if (ScheduleManager.Instance.MorningScheduleText.text == ScheduleName.봉사활동.ToString())
        //{
        // if(TimeManager.Instance.MonthText.text>="3" && TimeManager.Instance.DayText.text>=5  )
        //}
    }

    // 꿈 확률 이벤트 추가
    public void AddDreamProbabilityEvent(List<ProbabilityEventData> eventProbabilityDatas)
    {

    }

    // 날짜 이벤트 추가
    public bool AddDayEvent(List<int> eventQueue)
    {
        bool hasEvent = false;
        switch (TimeManager.Instance.GetMonth())
        {
            case 1:
                switch (TimeManager.Instance.GetDay())
                {
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                    case 4:
                        if (SonManager.SonData.CurrentJob == Job.고등학생)
                        {
                            eventQueue.Add(30801);
                        }
                        break;
                    case 5:
                        break;
                    case 6:
                        if (SonManager.SonData.CurrentJob == Job.중학생)
                        {
                            eventQueue.Add(20801);
                        }
                        break;
                    case 7:
                        if (SonManager.SonData.CurrentJob == Job.초등학생)
                        {
                            eventQueue.Add(10401);
                        }
                        break;
                    case 8:
                        break;
                    case 9:
                        break;
                    case 10:
                        break;
                    case 11:
                        break;
                    case 12:
                        break;
                    case 13:
                        break;
                    case 14:
                        break;
                    case 15:
                        break;
                    case 16:
                        break;
                    case 17:
                        break;
                    case 18:
                        break;
                    case 19:
                        break;
                    case 20:
                        break;
                    case 21:
                        break;
                    case 22:
                        break;
                    case 23:
                        break;
                    case 24:
                        break;
                    case 25:
                        break;
                    case 26:
                        break;
                    case 27:
                        break;
                    case 28:
                        break;
                    case 29:
                        break;
                    case 30:
                        break;
                    case 31:
                        if (SonManager.SonData.CurrentJob == Job.대학생 && SonManager.SonData.MilitaryServiceStatus == true)
                        {
                            eventQueue.Add(41501);
                        }
                        break;
                }
                break;
            case 2:
                switch (TimeManager.Instance.GetDay())
                {
                    case 1:
                        if (SonManager.SonData.CurrentJob == Job.군인)
                        {
                            eventQueue.Add(50201);
                        }
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                    case 4:
                        break;
                    case 5:
                        break;
                    case 6:
                        if (SonManager.SonData.CurrentJob == Job.대학생 && SonManager.SonData.MilitaryServiceStatus == false)
                        {
                            eventQueue.Add(50101);
                        }
                        break;
                    case 7:
                        break;
                    case 8:
                        break;
                    case 9:
                        break;
                    case 10:
                        break;
                    case 11:
                        break;
                    case 12:
                        break;
                    case 13:
                        break;
                    case 14:
                        break;
                    case 15:
                        break;
                    case 16:
                        break;
                    case 17:
                        break;
                    case 18:
                        break;
                    case 19:
                        break;
                    case 20:
                        break;
                    case 21:
                        break;
                    case 22:
                        break;
                    case 23:
                        break;
                    case 24:
                        break;
                    case 25:
                        break;
                    case 26:
                        break;
                    case 27:
                        break;
                    case 28:
                        if (SonManager.SonData.CurrentJob == Job.대학생 && SonManager.SonData.MilitaryServiceStatus == true)
                        {
                            eventQueue.Add(40801);
                        }
                        break;
                }
                break;
            case 3:
                switch (TimeManager.Instance.GetDay())
                {
                    case 1:
                        switch (SonManager.SonData.CurrentJob)
                        {
                            case Job.유치원생:
                                eventQueue.Add(10101);
                                break;
                            case Job.초등학생:
                                eventQueue.Add(20101);
                                break;
                            case Job.중학생:
                                eventQueue.Add(30101);
                                break;
                            case Job.고등학생:
                                eventQueue.Add(40101);
                                break;
                            case Job.대학생:
                                break;
                            case Job.군인:
                                break;
                        }
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                    case 4:
                        break;
                    case 5:
                        break;
                    case 6:
                        break;
                    case 7:
                        break;
                    case 8:
                        break;
                    case 9:
                        break;
                    case 10:
                        break;
                    case 11:
                        break;
                    case 12:
                        break;
                    case 13:
                        break;
                    case 14:
                        break;
                    case 15:
                        break;
                    case 16:
                        break;
                    case 17:
                        break;
                    case 18:
                        break;
                    case 19:
                        break;
                    case 20:
                        break;
                    case 21:
                        break;
                    case 22:
                        break;
                    case 23:
                        break;
                    case 24:
                        break;
                    case 25:
                        break;
                    case 26:
                        break;
                    case 27:
                        break;
                    case 28:
                        break;
                    case 29:
                        break;
                    case 30:
                        break;
                    case 31:
                        break;
                }
                break;
            case 4:
                switch (TimeManager.Instance.GetDay())
                {
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                    case 4:
                        break;
                    case 5:
                        break;
                    case 6:
                        if (SonManager.SonData.CurrentJob == Job.고등학생)
                        {
                            eventQueue.Add(30201);
                        }
                        break;
                    case 7:
                        break;
                    case 8:
                        if (SonManager.SonData.CurrentJob == Job.중학생)
                        {
                            eventQueue.Add(20201);
                        }
                        break;
                    case 9:
                        break;
                    case 10:
                        break;
                    case 11:
                        break;
                    case 12:
                        break;
                    case 13:
                        break;
                    case 14:
                        break;
                    case 15:
                        break;
                    case 16:
                        break;
                    case 17:
                        break;
                    case 18:
                        break;
                    case 19:
                        break;
                    case 20:
                        break;
                    case 21:
                        break;
                    case 22:
                        break;
                    case 23:
                        break;
                    case 24:
                        break;
                    case 25:
                        break;
                    case 26:
                        break;
                    case 27:
                        if (SonManager.SonData.CurrentJob == Job.고등학생)
                        {
                            eventQueue.Add(30301);
                        }
                        break;
                    case 28:
                        break;
                    case 29:
                        if (SonManager.SonData.CurrentJob == Job.중학생)
                        {
                            eventQueue.Add(20301);
                        }
                        break;
                    case 30:
                        break;
                }
                break;
            case 5:
                switch (TimeManager.Instance.GetDay())
                {
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                    case 4:
                        break;
                    case 5:
                        break;
                    case 6:
                        break;
                    case 7:
                        break;
                    case 8:
                        break;
                    case 9:
                        break;
                    case 10:
                        break;
                    case 11:
                        break;
                    case 12:
                        break;
                    case 13:
                        break;
                    case 14:
                        break;
                    case 15:
                        break;
                    case 16:
                        break;
                    case 17:
                        break;
                    case 18:
                        break;
                    case 19:
                        break;
                    case 20:
                        break;
                    case 21:
                        break;
                    case 22:
                        break;
                    case 23:
                        break;
                    case 24:
                        break;
                    case 25:
                        break;
                    case 26:
                        break;
                    case 27:
                        break;
                    case 28:
                        break;
                    case 29:
                        break;
                    case 30:
                        break;
                    case 31:
                        break;
                }
                break;
            case 6:
                switch (TimeManager.Instance.GetDay())
                {
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                    case 4:
                        break;
                    case 5:
                        if (SonManager.SonData.CurrentJob == Job.대학생 && SonManager.SonData.MilitaryServiceStatus == true)
                        {
                            eventQueue.Add(40901);
                        }
                        break;
                    case 6:
                        break;
                    case 7:
                        if (SonManager.SonData.CurrentJob == Job.대학생 && SonManager.SonData.MilitaryServiceStatus == false)
                        {
                            eventQueue.Add(40201);
                        }
                        break;
                    case 8:
                        break;
                    case 9:
                        break;
                    case 10:
                        break;
                    case 11:
                        break;
                    case 12:
                        break;
                    case 13:
                        break;
                    case 14:
                        break;
                    case 15:
                        break;
                    case 16:
                        break;
                    case 17:
                        break;
                    case 18:
                        break;
                    case 19:
                        if (SonManager.SonData.CurrentJob == Job.대학생 && SonManager.SonData.MilitaryServiceStatus == true)
                        {
                            eventQueue.Add(41101);
                        }
                        break;
                    case 20:
                        break;
                    case 21:
                        break;
                    case 22:
                        if (SonManager.SonData.CurrentJob == Job.대학생 && SonManager.SonData.MilitaryServiceStatus == false)
                        {
                            eventQueue.Add(40401);
                        }
                        break;
                    case 23:
                        break;
                    case 24:
                        break;
                    case 25:
                        break;
                    case 26:
                        break;
                    case 27:
                        break;
                    case 28:
                        break;
                    case 29:
                        break;
                    case 30:
                        break;
                }
                break;
            case 7:
                switch (TimeManager.Instance.GetDay())
                {
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                    case 4:
                        break;
                    case 5:
                        break;
                    case 6:
                        break;
                    case 7:
                        break;
                    case 8:
                        break;
                    case 9:
                        break;
                    case 10:
                        break;
                    case 11:
                        break;
                    case 12:
                        break;
                    case 13:
                        if (SonManager.SonData.CurrentJob == Job.고등학생)
                        {
                            eventQueue.Add(30501);
                        }
                        break;
                    case 14:
                        break;
                    case 15:
                        if (SonManager.SonData.CurrentJob == Job.중학생)
                        {
                            eventQueue.Add(20501);
                        }
                        break;
                    case 16:
                        if (SonManager.SonData.CurrentJob == Job.초등학생)
                        {
                            eventQueue.Add(10201);
                        }
                        break;
                    case 17:
                        break;
                    case 18:
                        break;
                    case 19:
                        break;
                    case 20:
                        break;
                    case 21:
                        break;
                    case 22:
                        break;
                    case 23:
                        break;
                    case 24:
                        break;
                    case 25:
                        break;
                    case 26:
                        break;
                    case 27:
                        break;
                    case 28:
                        break;
                    case 29:
                        break;
                    case 30:
                        break;
                    case 31:
                        break;
                }
                break;
            case 8:
                switch (TimeManager.Instance.GetDay())
                {
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                    case 4:
                        break;
                    case 5:
                        break;
                    case 6:
                        break;
                    case 7:
                        break;
                    case 8:
                        break;
                    case 9:
                        break;
                    case 10:
                        break;
                    case 11:
                        break;
                    case 12:
                        break;
                    case 13:
                        break;
                    case 14:
                        break;
                    case 15:
                        break;
                    case 16:
                        break;
                    case 17:
                        if (SonManager.SonData.CurrentJob == Job.고등학생)
                        {
                            eventQueue.Add(30502);
                        }
                        break;
                    case 18:
                        break;
                    case 19:
                        if (SonManager.SonData.CurrentJob == Job.중학생)
                        {
                            eventQueue.Add(20502);
                        }
                        break;
                    case 20:
                        if (SonManager.SonData.CurrentJob == Job.초등학생)
                        {
                            eventQueue.Add(10301);
                        }
                        break;
                    case 21:
                        break;
                    case 22:
                        break;
                    case 23:
                        break;
                    case 24:
                        break;
                    case 25:
                        break;
                    case 26:
                        break;
                    case 27:
                        break;
                    case 28:
                        break;
                    case 29:
                        break;
                    case 30:
                        break;
                    case 31:
                        break;
                }
                break;
            case 9:
                switch (TimeManager.Instance.GetDay())
                {
                    case 1:
                        if (SonManager.SonData.CurrentJob == Job.대학생 && SonManager.SonData.MilitaryServiceStatus == false)
                        {
                            eventQueue.Add(40402);
                        }
                        if (SonManager.SonData.CurrentJob == Job.대학생 && SonManager.SonData.MilitaryServiceStatus == true)
                        {
                            eventQueue.Add(41102);
                        }
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                    case 4:
                        break;
                    case 5:
                        break;
                    case 6:
                        break;
                    case 7:
                        break;
                    case 8:
                        break;
                    case 9:
                        break;
                    case 10:
                        break;
                    case 11:
                        break;
                    case 12:
                        break;
                    case 13:
                        break;
                    case 14:
                        break;
                    case 15:
                        break;
                    case 16:
                        break;
                    case 17:
                        break;
                    case 18:
                        break;
                    case 19:
                        break;
                    case 20:
                        break;
                    case 21:
                        break;
                    case 22:
                        break;
                    case 23:
                        break;
                    case 24:
                        break;
                    case 25:
                        break;
                    case 26:
                        break;
                    case 27:
                        break;
                    case 28:
                        break;
                    case 29:
                        break;
                    case 30:
                        break;
                }
                break;
            case 10:
                switch (TimeManager.Instance.GetDay())
                {
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                    case 4:
                        break;
                    case 5:
                        if (SonManager.SonData.CurrentJob == Job.고등학생)
                        {
                            eventQueue.Add(30601);
                        }
                        break;
                    case 6:
                        break;
                    case 7:
                        if (SonManager.SonData.CurrentJob == Job.중학생)
                        {
                            eventQueue.Add(20601);
                        }
                        break;
                    case 8:
                        break;
                    case 9:
                        break;
                    case 10:
                        break;
                    case 11:
                        break;
                    case 12:
                        break;
                    case 13:
                        break;
                    case 14:
                        break;
                    case 15:
                        break;
                    case 16:
                        break;
                    case 17:
                        break;
                    case 18:
                        break;
                    case 19:
                        break;
                    case 20:
                        break;
                    case 21:
                        break;
                    case 22:
                        break;
                    case 23:
                        break;
                    case 24:
                        break;
                    case 25:
                        break;
                    case 26:
                        break;
                    case 27:
                        break;
                    case 28:
                        break;
                    case 29:
                        break;
                    case 30:
                        break;
                    case 31:
                        break;
                }
                break;
            case 11:
                switch (TimeManager.Instance.GetDay())
                {
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                    case 4:
                        break;
                    case 5:
                        break;
                    case 6:
                        break;
                    case 7:
                        break;
                    case 8:
                        break;
                    case 9:
                        break;
                    case 10:
                        break;
                    case 11:
                        break;
                    case 12:
                        break;
                    case 13:
                        break;
                    case 14:
                        break;
                    case 15:
                        break;
                    case 16:
                        break;
                    case 17:
                        break;
                    case 18:
                        break;
                    case 19:
                        break;
                    case 20:
                        break;
                    case 21:
                        break;
                    case 22:
                        break;
                    case 23:
                        break;
                    case 24:
                        break;
                    case 25:
                        break;
                    case 26:
                        break;
                    case 27:
                        break;
                    case 28:
                        break;
                    case 29:
                        if (SonManager.SonData.CurrentJob == Job.대학생 && SonManager.SonData.MilitaryServiceStatus == false)
                        {
                            eventQueue.Add(40501);
                        }
                        break;
                    case 30:
                        break;
                }
                break;
            case 12:
                switch (TimeManager.Instance.GetDay())
                {
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                    case 4:
                        if (SonManager.SonData.CurrentJob == Job.대학생 && SonManager.SonData.MilitaryServiceStatus == true)
                        {
                            eventQueue.Add(41201);
                        }
                        break;
                    case 5:
                        break;
                    case 6:
                        break;
                    case 7:
                        break;
                    case 8:
                        break;
                    case 9:
                        break;
                    case 10:
                        break;
                    case 11:
                        break;
                    case 12:
                        break;
                    case 13:
                        if (SonManager.SonData.CurrentJob == Job.대학생 && SonManager.SonData.MilitaryServiceStatus == false)
                        {
                            eventQueue.Add(40701);
                        }
                        break;
                    case 14:
                        break;
                    case 15:
                        break;
                    case 16:
                        break;
                    case 17:
                        break;
                    case 18:
                        if (SonManager.SonData.CurrentJob == Job.대학생 && SonManager.SonData.MilitaryServiceStatus == true)
                        {
                            eventQueue.Add(41401);
                        }
                        break;
                    case 19:
                        break;
                    case 20:
                        break;
                    case 21:
                        break;
                    case 22:
                        break;
                    case 23:
                        break;
                    case 24:
                        break;
                    case 25:
                        break;
                    case 26:
                        break;
                    case 27:
                        break;
                    case 28:
                        break;
                    case 29:
                        break;
                    case 30:
                        break;
                    case 31:
                        break;
                }
                break;
        }
        return hasEvent;
    }
}
