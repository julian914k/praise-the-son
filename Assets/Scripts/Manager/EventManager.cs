﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using BayatGames.SaveGameFree;

public class EventManager : MonoBehaviour
{
    // 이벤트 매니저가 Event 팝업창 관련 UI 컴포넌트를 레퍼런스로 가지고 있고, Event 스크립트 자체는 MonoBehaviour를 상속 받지 않는 데이터 상태로 둬야함.
    // 이벤트 팝업과 이벤트 결과 팝업 창은 Event 스크립트를 참조하지 않고, Event 스크립트는 오직 New 명령어를 통해서만 생성되야함.

    // 요약: 실제 데이터는 Event 스크립트가 가지고 있고, 그 데이터를 참조해 EventManager에 연결되어 있는 유니티 이벤트 팝업 컴포넌트에 데이터를 연결해줌.
    // 이유: Event와 EventData가 같은 스크립트인데, MonoBehaviour 상속 여부에 따라 2개로 나뉘어져 중복 작업이 발생했음.
    // 작업: Event 스크립트를 삭제하고, EventData 스크립트로 통합해야함. MonoBehaviour를 상속 받지 않는 스크립트는 뒤에 Data를 붙여서 통일성을 줌.    

    public static EventManager Instance = null;
    public GameObject DarkEventBackgroundPanel;

    [Header("Event Panel")]
    public GameObject EventPanelUI;
    public Image EventImage;
    public Text EventSubject;
    public Text EventContext;
    public Text EventChoiceText1;
    public Text EventChoiceText2;
    public Text EventChoiceText3;
    public Text EventChoiceText4;

    [Header("Event Result Panel")]
    public GameObject EventResultPanelUI;
    public Image EventResultImage;
    public Text EventResultSubject;
    public Text EventResultContext;
    public Text EventResultConfirmText;
    public Text EventResultAttributeUpText;
    public Text EventResultAttributeDownText;


    [Header("Event Manager")]
    public List<TriggerEventData> triggerEventsQueue;
    public List<int> eventQueue;
    public EventType eventType = EventType.Default;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }
    void Start()
    {
        triggerEventsQueue = new List<TriggerEventData>();
        eventQueue = new List<int>();

        LoadEventQueue();
        LoadTriggerEventQueue();
    }
    public void ActiveEvent(EventData eventData)
    {
        eventType = eventData.eventType;

        IncreaseAttributes(eventData.increaseAttributes);
        DecreaseAttributes(eventData.decreaseAttributes);
        AddTrait(eventData.addTrait);
        RemoveTrait(eventData.removeTrait);

        if (eventType == EventType.Default)
        {
            EventImage.sprite = Resources.Load<Sprite>("EventSprites/" + eventData.imagePath);
            EventSubject.text = eventData.subject;
            EventContext.text = eventData.context;
            EventChoiceText1.text = eventData.choiceText1;
            EventChoiceText2.text = eventData.choiceText2;
            EventChoiceText3.text = eventData.choiceText3;
            EventChoiceText4.text = eventData.choiceText4;

            SetActiveEvent(true, eventData.choiceLength);
        }
        else if (eventType == EventType.Result)
        {
            EventResultImage.sprite = Resources.Load<Sprite>("EventSprites/" + eventData.imagePath);
            EventResultSubject.text = eventData.subject;
            EventResultContext.text = eventData.context;
            EventResultConfirmText.text = eventData.confirmText;

            foreach (KeyValuePair<string, int> attribute in eventData.increaseAttributes)
            {
                if (attribute.Key == "스트레스")
                {
                    EventResultAttributeDownText.text += attribute.Key;
                    EventResultAttributeDownText.text += "+";
                    EventResultAttributeDownText.text += "  ";
                    continue;
                }
                EventResultAttributeUpText.text += attribute.Key;
                EventResultAttributeUpText.text += "+";
                EventResultAttributeUpText.text += "  ";
                //EventResultAttributeUpText.text += attribute.Value;
            }
            foreach (KeyValuePair<string, int> attribute in eventData.decreaseAttributes)
            {
                if (attribute.Key == "스트레스")
                {
                    EventResultAttributeUpText.text += attribute.Key;
                    EventResultAttributeUpText.text += "-";
                    EventResultAttributeUpText.text += "  ";
                    continue;
                }
                EventResultAttributeDownText.text += attribute.Key;
                EventResultAttributeDownText.text += "-";
                EventResultAttributeDownText.text += "  ";
                //EventResultAttributeDownText.text += attribute.Value;
            }

            if (EventResultAttributeUpText.text == "")
            {
                EventResultAttributeUpText.text = "X";
            }
            if (EventResultAttributeDownText.text == "")
            {
                EventResultAttributeDownText.text = "X";
            }

            EventResultPanelUI.SetActive(true);
        }
    }
    public void IncreaseAttributes(Dictionary<string, int> increaseAttributes)
    {
        foreach (KeyValuePair<string, int> attribute in increaseAttributes)
        {
            switch (attribute.Key)
            {
                case "체력":
                    SonManager.SonData.Physical += attribute.Value;
                    break;
                case "지능":
                    SonManager.SonData.Intelligence += attribute.Value;
                    break;
                case "감수성":
                    SonManager.SonData.Sensibility += attribute.Value;
                    break;
                case "손재주":
                    SonManager.SonData.Craft += attribute.Value;
                    break;
                case "사교성":
                    SonManager.SonData.Sociability += attribute.Value;
                    break;
                case "매력":
                    SonManager.SonData.Charm += attribute.Value;
                    break;
                case "개념":
                    SonManager.SonData.Notion += attribute.Value;
                    break;
                case "친밀도":
                    SonManager.SonData.Intimacy += attribute.Value;
                    break;
                case "스트레스":
                    SonManager.SonData.Stress += attribute.Value;
                    break;
                case "운동평가":
                    SonManager.SonData.Athletic += attribute.Value;
                    break;
                case "지식평가":
                    SonManager.SonData.Education += attribute.Value;
                    break;
                case "예술평가":
                    SonManager.SonData.Art += attribute.Value;
                    break;
                case "기술평가":
                    SonManager.SonData.Technic += attribute.Value;
                    break;
            }
        }
    }
    public void DecreaseAttributes(Dictionary<string, int> decreaseAttributes)
    {
        foreach (KeyValuePair<string, int> attribute in decreaseAttributes)
        {
            switch (attribute.Key)
            {
                case "체력":
                    SonManager.SonData.Physical -= attribute.Value;
                    break;
                case "지능":
                    SonManager.SonData.Intelligence -= attribute.Value;
                    break;
                case "감수성":
                    SonManager.SonData.Sensibility -= attribute.Value;
                    break;
                case "손재주":
                    SonManager.SonData.Craft -= attribute.Value;
                    break;
                case "사교성":
                    SonManager.SonData.Sociability -= attribute.Value;
                    break;
                case "매력":
                    SonManager.SonData.Charm -= attribute.Value;
                    break;
                case "개념":
                    SonManager.SonData.Notion -= attribute.Value;
                    break;
                case "친밀도":
                    SonManager.SonData.Intimacy -= attribute.Value;
                    break;
                case "스트레스":
                    SonManager.SonData.Stress -= attribute.Value;
                    break;
                case "운동평가":
                    SonManager.SonData.Athletic -= attribute.Value;
                    break;
                case "지식평가":
                    SonManager.SonData.Education -= attribute.Value;
                    break;
                case "예술평가":
                    SonManager.SonData.Art -= attribute.Value;
                    break;
                case "기술평가":
                    SonManager.SonData.Technic += attribute.Value;
                    break;
            }
        }

    }
    public void AddTrait(PersonalityTrait addTrait)
    {
        if (addTrait != PersonalityTrait.없음)
        {
            if (SonManager.SonData.CurrentPersonalityTrait1 == PersonalityTrait.없음)
            {
                SonManager.SonData.CurrentPersonalityTrait1 = addTrait;
            }
            else if (SonManager.SonData.CurrentPersonalityTrait2 == PersonalityTrait.없음)
            {
                SonManager.SonData.CurrentPersonalityTrait2 = addTrait;
            }
            else if (SonManager.SonData.CurrentPersonalityTrait3 == PersonalityTrait.없음)
            {
                SonManager.SonData.CurrentPersonalityTrait3 = addTrait;
            }
            else if (SonManager.SonData.CurrentPersonalityTrait4 == PersonalityTrait.없음)
            {
                SonManager.SonData.CurrentPersonalityTrait4 = addTrait;
            }
            else
            {
                int randomInt = Random.Range(1, 4);
                switch (randomInt)
                {
                    case 1:
                        SonManager.SonData.CurrentPersonalityTrait1 = addTrait;
                        break;
                    case 2:
                        SonManager.SonData.CurrentPersonalityTrait2 = addTrait;
                        break;
                    case 3:
                        SonManager.SonData.CurrentPersonalityTrait3 = addTrait;
                        break;
                    case 4:
                        SonManager.SonData.CurrentPersonalityTrait4 = addTrait;
                        break;
                }
            }
            Debug.Log("트레잇1: " + SonManager.SonData.CurrentPersonalityTrait1);
            Debug.Log("트레잇2: " + SonManager.SonData.CurrentPersonalityTrait2);
            Debug.Log("트레잇3: " + SonManager.SonData.CurrentPersonalityTrait3);
            Debug.Log("트레잇4: " + SonManager.SonData.CurrentPersonalityTrait4);
            SonManager.SonData.SaveSonPersonalityTrait();
            SonManager.SonData.LoadSonPersonalityTrait();
        }
    }
    public void RemoveTrait(PersonalityTrait removeTrait)
    {
        if (removeTrait != PersonalityTrait.없음)
        {
            if (SonManager.SonData.CurrentPersonalityTrait1 == removeTrait)
            {
                SonManager.SonData.CurrentPersonalityTrait1 = PersonalityTrait.없음;
                SonManager.SonData.LoadSonPersonalityTrait();
            }
            else if (SonManager.SonData.CurrentPersonalityTrait2 == removeTrait)
            {
                SonManager.SonData.CurrentPersonalityTrait2 = PersonalityTrait.없음;
                SonManager.SonData.LoadSonPersonalityTrait();
            }
            else if (SonManager.SonData.CurrentPersonalityTrait3 == removeTrait)
            {
                SonManager.SonData.CurrentPersonalityTrait3 = PersonalityTrait.없음;
                SonManager.SonData.LoadSonPersonalityTrait();
            }
            else if (SonManager.SonData.CurrentPersonalityTrait4 == removeTrait)
            {
                SonManager.SonData.CurrentPersonalityTrait4 = PersonalityTrait.없음;
                SonManager.SonData.LoadSonPersonalityTrait();
            }
            SonManager.SonData.SaveSonPersonalityTrait();
            SonManager.SonData.LoadSonPersonalityTrait();
        }
    }
    public void SetActiveEvent(bool shouldActive)
    {
        if (eventType == EventType.Default)
        {
            EventPanelUI.SetActive(shouldActive);
        }
        else if (eventType == EventType.Result)
        {
            EventResultPanelUI.SetActive(shouldActive);
        }
    }
    public void SetActiveEvent(bool shouldActive, int choiceLength)
    {
        this.SetActiveEvent(shouldActive);
        // choiceLength에 따른 선택지 UI 활성화/비활성화       
    }
    public void SaveEventQueue()
    {
        SaveGame.Save<List<int>>("EventQueue", eventQueue);
    }
    public void SaveTriggerEventQueue()
    {
        SaveGame.Save<List<TriggerEventData>>("TriggerEventQueue", triggerEventsQueue);
    }
    public void LoadEventQueue()
    {
        if (SaveGame.Exists("EventQueue"))
        {
            eventQueue = SaveGame.Load<List<int>>("EventQueue");
        }
    }
    public void LoadTriggerEventQueue()
    {
        if (SaveGame.Exists("TriggerEventQueue"))
        {
            triggerEventsQueue = SaveGame.Load<List<TriggerEventData>>("TriggerEventQueue");
        }
    }
    public void GenerateEvent()
    {
        if (eventQueue.Count > 0)
        {
            if (eventQueue[0] != 0)
            {
                TimeManager.Instance.StopTime();
                DarkEventBackgroundPanel.SetActive(true);
                ActiveEvent(EventDB.Instance.FindEvent(eventQueue[0]));
            }
        }
        else
        {
            TimeManager.Instance.SetPhase(TimeManager.Instance.GetPhase() + 1);
        }
    }
    public void AddEventQueue()
    {
        EventGenerator.Instance.AddDayEvent(eventQueue);
        DecreaseTriggerEventDelayedDay();
        AddTriggerEvent();
        AddProbabilityEvent();

        // 이벤트 큐 저장
        SaveTriggerEventQueue();
        SaveEventQueue();
    }
    public void CloseEvent()
    {
        eventQueue.RemoveAt(0);
        DarkEventBackgroundPanel.SetActive(false);
        SetActiveEvent(false);
        TimeManager.Instance.SetPhase(TimeManager.Instance.GetPhase() + 1);
        TimeManager.Instance.SaveDate();
        SonManager.SonData.SaveAllSonDatas();
        SonManager.Instance.UpdateAllSonInfo();
        ScheduleManager.Instance.LoadTalentAndInterestSchedule();
        TimeManager.Instance.StartTime();
    }
    public void DecreaseTriggerEventDelayedDay()
    {
        for (int i = 0; i < triggerEventsQueue.Count; i++)
        {
            if (triggerEventsQueue[i].DelayedDay > 0)
            {
                // 이벤트 지연 날짜 하루씩 감소
                triggerEventsQueue[i].DelayedDay -= 1;
            }
        }
    }
    public void AddTriggerEvent()
    {
        List<int> addedTriggerEvents = new List<int>();

        for (int i = 0; i < triggerEventsQueue.Count; i++)
        {
            if (triggerEventsQueue[i].DelayedDay <= 0)
            {
                // 이벤트 큐에 담아준다.
                eventQueue.Add(triggerEventsQueue[i].EventSeq);
                addedTriggerEvents.Add(i);
            }
        }
        foreach (int triggerEventIndex in addedTriggerEvents)
        {
            triggerEventsQueue.RemoveAt(triggerEventIndex);
        }
    }

    public void ChoiceEvent(int choiceNumber)
    {
        EventData _eventData = EventDB.Instance.eventData;
        switch (choiceNumber)
        {
            case 1:
                if (_eventData.choiceSeq1 != 0)
                    EventManager.Instance.triggerEventsQueue.Add(new TriggerEventData(_eventData.choiceSeq1, _eventData.choiceDelayedDay1));
                break;
            case 2:
                if (_eventData.choiceSeq2 != 0)
                    EventManager.Instance.triggerEventsQueue.Add(new TriggerEventData(_eventData.choiceSeq2, _eventData.choiceDelayedDay2));
                break;
            case 3:
                if (_eventData.choiceSeq3 != 0)
                    EventManager.Instance.triggerEventsQueue.Add(new TriggerEventData(_eventData.choiceSeq3, _eventData.choiceDelayedDay3));
                break;
            case 4:
                if (_eventData.choiceSeq4 != 0)
                    EventManager.Instance.triggerEventsQueue.Add(new TriggerEventData(_eventData.choiceSeq4, _eventData.choiceDelayedDay4));
                break;
        }

        CloseEvent();
    }

    public void ResultEvent()
    {
        EventData _eventResultData = EventDB.Instance.eventData;
        if (_eventResultData.resultSeq != 0)
        {
            EventManager.Instance.triggerEventsQueue.Add(new TriggerEventData(_eventResultData.resultSeq, _eventResultData.resultDelayedDay));
        }
        CloseEvent();
    }

    public void FindAllProbabilityEvents(List<ProbabilityEventData> eventProbabilityDatas)
    {
        EventGenerator.Instance.AddGeneralProbabilityEvent(eventProbabilityDatas);
        EventGenerator.Instance.AddScheduleProbabilityEvent(eventProbabilityDatas);
        EventGenerator.Instance.AddJobProbabilityEvent(eventProbabilityDatas);
        EventGenerator.Instance.AddDreamProbabilityEvent(eventProbabilityDatas);
    }

    public void AddProbabilityEvent()
    {
        List<ProbabilityEventData> eventProbabilityDatas = new List<ProbabilityEventData>();
        FindAllProbabilityEvents(eventProbabilityDatas);

        int sumFactor = 0;

        foreach (ProbabilityEventData eventProbabilityData in eventProbabilityDatas)
        {
            // 여기서 확률 이벤트 확률팩터 총합을 구한다.
            sumFactor += eventProbabilityData.Probability;
        }
        // 확률팩터 총합을 참고해서 확률을 계산해서, 선택된 확률 팩터 값을 찾는다.
        int randomInt = Random.Range(1, 1000);

        //Debug.Log("확률 계산 randomInt + SumFactor : " + randomInt + " + " + sumFactor);

        if (randomInt > sumFactor)
        {
            // 선택된 확률 이벤트 없음
            return;
        }
        else
        {
            int _sumFactor = 0;
            foreach (ProbabilityEventData eventProbabilityData in eventProbabilityDatas)
            {
                // 선택된 확률 값 randomInt와 매칭되는 이벤트를 찾는다.
                _sumFactor += eventProbabilityData.Probability;
                //Debug.Log("선택된 이벤트 찾기 randomInt + SumFactor : " + randomInt + " + " + _sumFactor);
                if (randomInt < _sumFactor)
                {
                    eventQueue.Add(eventProbabilityData.Seq);
                    return;
                }
            }
        }
    }
}
