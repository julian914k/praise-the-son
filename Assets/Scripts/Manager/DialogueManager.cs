﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour
{
	public GameObject DialogueActiveButton;
    public GameObject DialoguePanel;
    public GameObject DialogueResultPanel;	
    public Text DialogueResultText;

    public void ActiveDialoguePanel()
    {
		InactivePanel(DialogueActiveButton);
        ActivePanel(DialoguePanel);		
    }
    public void ActivePanel(GameObject panel)
    {
        panel.SetActive(true);
    }
    public void InactiveDialogueResultPanel()
    {
        InactivePanel(DialogueResultPanel);
		ActivePanel(DialogueActiveButton);
    }
    public void InactivePanel(GameObject panel)
    {
        panel.SetActive(false);
    }
    public void ChoiceDialogue(int choiceNumber)
    {
        InactivePanel(DialoguePanel);
        switch (choiceNumber)
        {
            case 1:
                DialogueResultText.text = "우왓! 고맙습니다!";
                break;
            case 2:
                DialogueResultText.text = "오늘은 기분이 좋아요.";
                break;
            case 3:
                DialogueResultText.text = "저한테 왜 그러세요!";
                break;
            default:
                DialogueResultText.text = "...";
                break;
        }
        ActivePanel(DialogueResultPanel);
    }
}
