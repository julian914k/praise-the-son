﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using BayatGames.SaveGameFree;

public class ScheduleManager : MonoBehaviour
{
    public static ScheduleManager Instance = null;
    public Dictionary<Day, ScheduleName> Schedules;
    public Image MorningScheduleImage;
    public Image EveningScheduleImage;
    public Image NightScheduleImage;
    public Image HolidayScheduleImage;
    public Text MorningScheduleText;
    public Text EveningScheduleText;
    public Text NightScheduleText;
    public Text HolidayScheduleText;
    public Image MorningIcon;
    public Image EveningIcon;
    public Image NightIcon;
    public Image HolidayIcon;
    public GameObject MorningLockPanel;
    public GameObject EveningLockPanel;
    public GameObject NightLockPanel;
    public GameObject HolidayLockPanel;
    public GameObject SchedulePopupPanel;
    public GameObject ScheduleMaskPanel;
    public GameObject EducationSchedulePanel;
    public GameObject ArbeitSchedulePanel;
    public GameObject VacationSchedulePanel;
    public Image DayIcon;
    public Day SchedulePopupDayType;
    public Sprite Transparent;
    public Sprite GoodTalentIcon;
    public Sprite BadTalentIcon;
    public Sprite GoodInterestIcon;
    public Sprite BadInterestIcon;
    [Header("교육")]
    public Sprite 학교가기;
    public Image 학원가기;
    public Image 과외;
    public Image 스터디그룹;
    public Image 도서관;
    [Header("알바")]
    public Image 봉사활동;
    public Image 레스토랑;
    public Image 카페;
    public Image 편의점;
    public Image 피시방;
    public Image 패스트푸드;
    public Image 전단지;
    public Image 인형탈;
    public Image 주유소;
    public Image 택배상하차;
    [Header("휴식")]
    public Image 집에서휴식;
    public Image 친구만나기;
    public Image 게임하기;
    public Image 운동하기;
    public Image 영화보기;
    public Image 그림그리기;
    public Image 음악듣기;
    public Image 독서하기;
    public Image 맛집탐방;
    public Image 종교활동;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }
    void Start()
    {
        Schedules = new Dictionary<Day, ScheduleName>();
        LoadSchedule();
        LoadTalentAndInterestSchedule();
        UpdateLockedDownSchedules();
    }
    public void SetScheduleAttributes()
    {
        SetScheduleAttribute(Day.Morning);
        SetScheduleAttribute(Day.Evening);
        SetScheduleAttribute(Day.Night);
        SetScheduleAttribute(Day.Holiday);
        SonManager.SonData.SaveSonAttributes();
        SonManager.Instance.UpdateSonAttributes();
    }
    public void SetScheduleAttribute(Day dayType)
    {
        string scheduleText = null;
        switch (dayType)
        {
            case Day.Morning:
                scheduleText = MorningScheduleText.text;
                break;
            case Day.Evening:
                scheduleText = EveningScheduleText.text;
                break;
            case Day.Night:
                scheduleText = NightScheduleText.text;
                break;
            case Day.Holiday:
                scheduleText = HolidayScheduleText.text;
                break;
        }
        switch (scheduleText)
        {
            case "학교가기":
                SonManager.SonData.IncreaseSonAttribute("Intelligence", 2);
                SonManager.SonData.IncreaseSonAttribute("Sociability", 1);
                SonManager.SonData.IncreaseSonAttribute("Stress", 1);
                break;
            case "학원가기":
                SonManager.SonData.IncreaseSonAttribute("Intelligence", 3);
                SonManager.SonData.IncreaseSonAttribute("Sociability", 1);
                SonManager.SonData.IncreaseSonAttribute("Physical", -1);
                SonManager.SonData.IncreaseSonAttribute("Stress", 1);
                break;
            case "과외":
                SonManager.SonData.IncreaseSonAttribute("Intelligence", 4);
                SonManager.SonData.IncreaseSonAttribute("Physical", -1);
                SonManager.SonData.IncreaseSonAttribute("Stress", 1);
                break;
            case "스터디그룹":
                SonManager.SonData.IncreaseSonAttribute("Intelligence", 1);
                SonManager.SonData.IncreaseSonAttribute("Physical", -1);
                SonManager.SonData.IncreaseSonAttribute("Stress", -1);
                break;
            case "도서관":
                SonManager.SonData.IncreaseSonAttribute("Intelligence", 1);
                SonManager.SonData.IncreaseSonAttribute("Physical", -1);
                SonManager.SonData.IncreaseSonAttribute("Stress", 1);
                break;
            case "봉사활동":
                SonManager.SonData.IncreaseSonAttribute("Sociability", 1);
                SonManager.SonData.IncreaseSonAttribute("Notion", 1);
                SonManager.SonData.IncreaseSonAttribute("Stress", 1);
                break;
            case "레스토랑":
                SonManager.SonData.IncreaseSonAttribute("Charm", 1);
                SonManager.SonData.IncreaseSonAttribute("Stress", 1);
                break;
            case "카페":
                SonManager.SonData.IncreaseSonAttribute("Sensibility", 1);
                SonManager.SonData.IncreaseSonAttribute("Stress", 1);
                break;
            case "편의점":
                SonManager.SonData.IncreaseSonAttribute("Physical", -1);
                SonManager.SonData.IncreaseSonAttribute("Stress", 1);
                break;
            case "피시방":
                SonManager.SonData.IncreaseSonAttribute("Physical", -1);
                SonManager.SonData.IncreaseSonAttribute("Stress", 1);
                break;
            case "패스트푸드":
                SonManager.SonData.IncreaseSonAttribute("Stress", 1);
                break;
            case "전단지":
                SonManager.SonData.IncreaseSonAttribute("Stress", 1);
                break;
            case "인형탈":
                SonManager.SonData.IncreaseSonAttribute("Stress", 1);
                SonManager.SonData.IncreaseSonAttribute("Physical", 1);
                break;
            case "주유소":
                break;
            case "택배상하차":
                SonManager.SonData.IncreaseSonAttribute("Stress", 2);
                SonManager.SonData.IncreaseSonAttribute("Physical", 2);
                break;

            //휴식
            case "집에서휴식":
                SonManager.SonData.IncreaseSonAttribute("Stress", -10);
                break;
            case "친구만나기":
                SonManager.SonData.IncreaseSonAttribute("Stress", -10);
                break;
            case "게임하기":
                SonManager.SonData.IncreaseSonAttribute("Stress", -10);
                SonManager.SonData.IncreaseSonAttribute("Physical", -1);
                SonManager.SonData.IncreaseSonAttribute("Craft", 1);
                break;
            case "운동하기":
                SonManager.SonData.IncreaseSonAttribute("Physical", 2);
                SonManager.SonData.IncreaseSonAttribute("Stress", -5);
                break;
            case "영화보기":
                SonManager.SonData.IncreaseSonAttribute("Sensibility", 1);
                SonManager.SonData.IncreaseSonAttribute("Stress", -5);
                break;
            case "그림그리기":
                SonManager.SonData.IncreaseSonAttribute("Sensibility", 1);
                SonManager.SonData.IncreaseSonAttribute("Stress", -5);
                break;
            case "음악듣기":
                SonManager.SonData.IncreaseSonAttribute("Sensibility", 1);
                SonManager.SonData.IncreaseSonAttribute("Stress", -5);
                break;
            case "독서하기":
                SonManager.SonData.IncreaseSonAttribute("Intelligence", 1);
                SonManager.SonData.IncreaseSonAttribute("Stress", -5);
                break;
            case "맛집탐방":
                SonManager.SonData.IncreaseSonAttribute("Stress", -5);
                SonManager.SonData.IncreaseSonAttribute("Stress", -5);
                break;
            case "종교활동":
                SonManager.SonData.IncreaseSonAttribute("Notion", 1);
                SonManager.SonData.IncreaseSonAttribute("Stress", -5);
                break;
        }
    }
    public void SwitchCategoryPanel(string category)
    {
        switch (category)
        {
            case "education":
                EducationSchedulePanel.SetActive(true);
                ArbeitSchedulePanel.SetActive(false);
                VacationSchedulePanel.SetActive(false);
                ScheduleMaskPanel.GetComponent<ScrollRect>().content = EducationSchedulePanel.GetComponent<RectTransform>();
                break;
            case "arbeit":
                EducationSchedulePanel.SetActive(false);
                ArbeitSchedulePanel.SetActive(true);
                VacationSchedulePanel.SetActive(false);

                ScheduleMaskPanel.GetComponent<ScrollRect>().content = ArbeitSchedulePanel.GetComponent<RectTransform>();
                break;
            case "vacation":
                EducationSchedulePanel.SetActive(false);
                ArbeitSchedulePanel.SetActive(false);
                VacationSchedulePanel.SetActive(true);

                ScheduleMaskPanel.GetComponent<ScrollRect>().content = VacationSchedulePanel.GetComponent<RectTransform>();
                break;
        }
    }
    public void OpenSchedulePopupPanel(string dayType)
    {
        if (SchedulePopupDayType.ToString() == dayType)
        {
            SchedulePopupPanel.SetActive(false);
            SchedulePopupDayType = Day.None;
        }
        else
        {
            switch (dayType)
            {
                case "Morning":
                    if (SaveGame.Load<bool>("IsMorningScheduleLockedDown")) break;
                    DayIcon.sprite = MorningIcon.sprite;
                    SchedulePopupDayType = Day.Morning;
                    SchedulePopupPanel.SetActive(true);
                    break;
                case "Evening":
                    if (SaveGame.Load<bool>("IsEveningScheduleLockedDown")) break;
                    DayIcon.sprite = EveningIcon.sprite;
                    SchedulePopupDayType = Day.Evening;
                    SchedulePopupPanel.SetActive(true);
                    break;
                case "Night":
                    if (SaveGame.Load<bool>("IsNightScheduleLockedDown")) break;
                    DayIcon.sprite = NightIcon.sprite;
                    SchedulePopupDayType = Day.Night;
                    SchedulePopupPanel.SetActive(true);
                    break;
                case "Holiday":
                    if (SaveGame.Load<bool>("IsHolidayScheduleLockedDown")) break;
                    DayIcon.sprite = HolidayIcon.sprite;
                    SchedulePopupDayType = Day.Holiday;
                    SchedulePopupPanel.SetActive(true);
                    break;
            }
        }
    }
    public void UpdateLockedDownSchedules()
    {
        if (SaveGame.Load<bool>("IsMorningScheduleLockedDown"))
        {
            MorningLockPanel.SetActive(true);
        }
        else
        {
            MorningLockPanel.SetActive(false);
        }

        if (SaveGame.Load<bool>("IsEveningScheduleLockedDown"))
        {
            EveningLockPanel.SetActive(true);
        }
        else
        {
            EveningLockPanel.SetActive(false);
        }

        if (SaveGame.Load<bool>("IsNightScheduleLockedDown"))
        {
            NightLockPanel.SetActive(true);
        }
        else
        {
            NightLockPanel.SetActive(false);
        }

        if (SaveGame.Load<bool>("IsHolidayScheduleLockedDown"))
        {
            HolidayLockPanel.SetActive(true);
        }
        else
        {
            HolidayLockPanel.SetActive(false);
        }
    }
    public void DecreaseLockedDownDay()
    {
        if (SaveGame.Exists("MorningScheduleLockedDownDay"))
        {
            int lockedDownDay = SaveGame.Load<int>("MorningScheduleLockedDownDay");
            lockedDownDay--;
            if (lockedDownDay <= 0)
            {
                SaveGame.Delete("MorningScheduleLockedDownDay");
                SaveGame.Save<bool>("IsMorningScheduleLockedDown", false);
            }
            else
            {
                SaveGame.Save<int>("MorningScheduleLockedDownDay", lockedDownDay);
            }
        }
        if (SaveGame.Exists("EveningScheduleLockedDownDay"))
        {
            int lockedDownDay = SaveGame.Load<int>("EveningScheduleLockedDownDay");
            lockedDownDay--;
            if (lockedDownDay <= 0)
            {
                SaveGame.Delete("EveningScheduleLockedDownDay");
                SaveGame.Save<bool>("IsEveningScheduleLockedDown", false);
            }
            else
            {
                SaveGame.Save<int>("EveningScheduleLockedDownDay", lockedDownDay);
            }
        }
        if (SaveGame.Exists("NightScheduleLockedDownDay"))
        {
            int lockedDownDay = SaveGame.Load<int>("NightScheduleLockedDownDay");
            lockedDownDay--;
            if (lockedDownDay <= 0)
            {
                SaveGame.Delete("NightScheduleLockedDownDay");
                SaveGame.Save<bool>("IsNightScheduleLockedDown", false);
            }
            else
            {
                SaveGame.Save<int>("NightScheduleLockedDownDay", lockedDownDay);
            }
        }
        if (SaveGame.Exists("HolidayScheduleLockedDownDay"))
        {
            int lockedDownDay = SaveGame.Load<int>("HolidayScheduleLockedDownDay");
            lockedDownDay--;
            if (lockedDownDay <= 0)
            {
                SaveGame.Delete("HolidayScheduleLockedDownDay");
                SaveGame.Save<bool>("IsHolidayScheduleLockedDown", false);
            }
            else
            {
                SaveGame.Save<int>("HolidayScheduleLockedDownDay", lockedDownDay);
            }
        }
    }
    public void SetLockedDown(Day dayType)
    {
        switch (dayType)
        {
            case Day.Morning:
                SaveGame.Save<bool>("IsMorningScheduleLockedDown", true);
                break;
            case Day.Evening:
                SaveGame.Save<bool>("IsEveningScheduleLockedDown", true);
                break;
            case Day.Night:
                SaveGame.Save<bool>("IsNightScheduleLockedDown", true);
                break;
            case Day.Holiday:
                SaveGame.Save<bool>("IsHolidayScheduleLockedDown", true);
                break;
        }
    }
    public void SetLockedDownDay(Day dayType, int lockedDownDay)
    {
        switch (dayType)
        {
            case Day.Morning:
                SaveGame.Save<int>("MorningScheduleLockedDownDay", lockedDownDay);
                break;
            case Day.Evening:
                SaveGame.Save<int>("EveningScheduleLockedDownDay", lockedDownDay);
                break;
            case Day.Night:
                SaveGame.Save<int>("NightScheduleLockedDownDay", lockedDownDay);
                break;
            case Day.Holiday:
                SaveGame.Save<int>("HolidayScheduleLockedDownDay", lockedDownDay);
                break;
        }
    }
    public void RemoveSchedule(Day dayType)
    {
        if (Schedules.ContainsKey(dayType))
        {
            Schedules.Remove(dayType);
        }
        switch (dayType)
        {
            case Day.Morning:
                MorningScheduleImage.sprite = Transparent;
                MorningScheduleText.text = "";
                SaveGame.Delete("MorningSchedule");
                SaveGame.Delete("MorningScheduleLockedDownDay");
                SaveGame.Save<bool>("IsMorningScheduleLockedDown", false);
                break;
            case Day.Evening:
                EveningScheduleImage.sprite = Transparent;
                EveningScheduleText.text = "";
                SaveGame.Delete("EveningSchedule");
                SaveGame.Delete("EveningScheduleLockedDownDay");
                SaveGame.Save<bool>("IsEveningScheduleLockedDown", false);
                break;
            case Day.Night:
                NightScheduleImage.sprite = Transparent;
                NightScheduleText.text = "";
                SaveGame.Delete("NightSchedule");
                SaveGame.Delete("NightScheduleLockedDownDay");
                SaveGame.Save<bool>("IsNightScheduleLockedDown", false);
                break;
            case Day.Holiday:
                HolidayScheduleImage.sprite = Transparent;
                HolidayScheduleText.text = "";
                SaveGame.Delete("HolidaySchedule");
                SaveGame.Delete("HolidayScheduleLockedDownDay");
                SaveGame.Save<bool>("IsHolidayScheduleLockedDown", false);
                break;
        }
        UpdateLockedDownSchedules();
    }
    public void SetSchedule(ScheduleSlot scheduleSlot)
    {
        SetSchedule(SchedulePopupDayType, scheduleSlot.ScheduleName);
    }
    public void SetSchedule(Day dayType, ScheduleName scheduleName, bool hasLockedDown)
    {
        SetSchedule(dayType, scheduleName);
        SetLockedDown(dayType);
        UpdateLockedDownSchedules();
    }
    public void SetSchedule(Day dayType, ScheduleName scheduleName, bool hasLockedDown, int lockedDownDay)
    {
        SetSchedule(dayType, scheduleName, true);
        SetLockedDownDay(dayType, lockedDownDay);
    }
    public void SetSchedule(Day dayType, ScheduleName scheduleName)
    {
        if (Schedules.ContainsKey(dayType))
        {
            Schedules.Remove(dayType);
        }
        Schedules.Add(dayType, scheduleName);

        switch (dayType)
        {
            case Day.Morning:
                MorningScheduleImage.sprite = GetScheduleSprite(scheduleName.ToString());
                MorningScheduleText.text = scheduleName.ToString();
                SaveGame.Save<ScheduleName>("MorningSchedule", scheduleName);
                break;
            case Day.Evening:
                EveningScheduleImage.sprite = GetScheduleSprite(scheduleName.ToString());
                EveningScheduleText.text = scheduleName.ToString();
                SaveGame.Save<ScheduleName>("EveningSchedule", scheduleName);
                break;
            case Day.Night:
                NightScheduleImage.sprite = GetScheduleSprite(scheduleName.ToString());
                NightScheduleText.text = scheduleName.ToString();
                SaveGame.Save<ScheduleName>("NightSchedule", scheduleName);
                break;
            case Day.Holiday:
                HolidayScheduleImage.sprite = GetScheduleSprite(scheduleName.ToString());
                HolidayScheduleText.text = scheduleName.ToString();
                SaveGame.Save<ScheduleName>("HolidaySchedule", scheduleName);
                break;
        }
    }

    public void LoadSchedule()
    {
        if (SaveGame.Exists("MorningSchedule"))
        {
            ScheduleName scheduleName = SaveGame.Load<ScheduleName>("MorningSchedule");
            MorningScheduleText.text = scheduleName.ToString();
            MorningScheduleImage.sprite = GetScheduleSprite(MorningScheduleText.text);
            Schedules.Add(Day.Morning, scheduleName);

        }
        if (SaveGame.Exists("EveningSchedule"))
        {
            ScheduleName scheduleName = SaveGame.Load<ScheduleName>("EveningSchedule");
            EveningScheduleText.text = scheduleName.ToString();
            EveningScheduleImage.sprite = GetScheduleSprite(EveningScheduleText.text);
            Schedules.Add(Day.Evening, scheduleName);
        }
        if (SaveGame.Exists("NightSchedule"))
        {
            ScheduleName scheduleName = SaveGame.Load<ScheduleName>("NightSchedule");
            NightScheduleText.text = scheduleName.ToString();
            NightScheduleImage.sprite = GetScheduleSprite(NightScheduleText.text);
            Schedules.Add(Day.Night, scheduleName);
        }
        if (SaveGame.Exists("HolidaySchedule"))
        {
            ScheduleName scheduleName = SaveGame.Load<ScheduleName>("HolidaySchedule");
            HolidayScheduleText.text = scheduleName.ToString();
            HolidayScheduleImage.sprite = GetScheduleSprite(HolidayScheduleText.text);
            Schedules.Add(Day.Holiday, scheduleName);
        }
    }

    public Sprite GetScheduleSprite(string scheduleText)
    {
        switch (scheduleText)
        {
            case "학교가기":
                return 학교가기;
            case "학원가기":
                return 학원가기.sprite;
            case "과외":
                return 과외.sprite;
            case "스터디그룹":
                return 스터디그룹.sprite;
            case "도서관":
                return 도서관.sprite;
            case "봉사활동":
                return 봉사활동.sprite;
            case "레스토랑":
                return 레스토랑.sprite;
            case "카페":
                return 카페.sprite;
            case "편의점":
                return 편의점.sprite;
            case "피시방":
                return 피시방.sprite;
            case "패스트푸드":
                return 패스트푸드.sprite;
            case "전단지":
                return 전단지.sprite;
            case "인형탈":
                return 인형탈.sprite;
            case "주유소":
                return 주유소.sprite;
            case "택배상하차":
                return 택배상하차.sprite;
            case "집에서휴식":
                return 집에서휴식.sprite;
            case "친구만나기":
                return 친구만나기.sprite;
            case "게임하기":
                return 게임하기.sprite;
            case "운동하기":
                return 운동하기.sprite;
            case "영화보기":
                return 영화보기.sprite;
            case "그림그리기":
                return 그림그리기.sprite;
            case "음악듣기":
                return 음악듣기.sprite;
            case "독서하기":
                return 독서하기.sprite;
            case "맛집탐방":
                return 맛집탐방.sprite;
            case "종교활동":
                return 종교활동.sprite;
        }
        return Transparent;
    }
    public void LoadTalentAndInterestSchedule()
    {
        LoadTalentSchedule();
        LoadInterestSchedule();
    }
    public void LoadTalentSchedule()
    {
        foreach (KeyValuePair<ScheduleName, int> talentSchedule in SonManager.SonData.TalentSchedules)
        {
            switch (talentSchedule.Key.ToString())
            {
                case "학원가기":
                    if (talentSchedule.Value == 1)
                    {
                        Image talentSlotImage = 학원가기.GetComponent<ScheduleSlot>().TalentSlot1;
                        if (talentSlotImage.sprite != Transparent && talentSlotImage.sprite != GoodTalentIcon)
                        {
                            talentSlotImage = 학원가기.GetComponent<ScheduleSlot>().TalentSlot2;
                        }

                        talentSlotImage.sprite = GoodTalentIcon;
                    }
                    else if (talentSchedule.Value == -1)
                    {
                        Image talentSlotImage = 학원가기.GetComponent<ScheduleSlot>().TalentSlot1;
                        if (talentSlotImage.sprite != Transparent && talentSlotImage.sprite != BadTalentIcon)
                        {
                            talentSlotImage = 학원가기.GetComponent<ScheduleSlot>().TalentSlot2;
                        }

                        talentSlotImage.sprite = BadTalentIcon;
                    }
                    break;
                case "과외":
                    if (talentSchedule.Value == 1)
                    {
                        Image talentSlotImage = 과외.GetComponent<ScheduleSlot>().TalentSlot1;
                        if (talentSlotImage.sprite != Transparent && talentSlotImage.sprite != GoodTalentIcon)
                        {
                            talentSlotImage = 과외.GetComponent<ScheduleSlot>().TalentSlot2;
                        }

                        talentSlotImage.sprite = GoodTalentIcon;
                    }
                    else if (talentSchedule.Value == -1)
                    {
                        Image talentSlotImage = 과외.GetComponent<ScheduleSlot>().TalentSlot1;
                        if (talentSlotImage.sprite != Transparent && talentSlotImage.sprite != BadTalentIcon)
                        {
                            talentSlotImage = 과외.GetComponent<ScheduleSlot>().TalentSlot2;
                        }

                        talentSlotImage.sprite = BadTalentIcon;
                    }
                    break;
                case "스터디그룹":
                    if (talentSchedule.Value == 1)
                    {
                        Image talentSlotImage = 스터디그룹.GetComponent<ScheduleSlot>().TalentSlot1;
                        if (talentSlotImage.sprite != Transparent && talentSlotImage.sprite != GoodTalentIcon)
                        {
                            talentSlotImage = 스터디그룹.GetComponent<ScheduleSlot>().TalentSlot2;
                        }

                        talentSlotImage.sprite = GoodTalentIcon;
                    }
                    else if (talentSchedule.Value == -1)
                    {
                        Image talentSlotImage = 스터디그룹.GetComponent<ScheduleSlot>().TalentSlot1;
                        if (talentSlotImage.sprite != Transparent && talentSlotImage.sprite != BadTalentIcon)
                        {
                            talentSlotImage = 스터디그룹.GetComponent<ScheduleSlot>().TalentSlot2;
                        }

                        talentSlotImage.sprite = BadTalentIcon;
                    }
                    break;
                case "도서관":
                    if (talentSchedule.Value == 1)
                    {
                        Image talentSlotImage = 도서관.GetComponent<ScheduleSlot>().TalentSlot1;
                        if (talentSlotImage.sprite != Transparent && talentSlotImage.sprite != GoodTalentIcon)
                        {
                            talentSlotImage = 도서관.GetComponent<ScheduleSlot>().TalentSlot2;
                        }

                        talentSlotImage.sprite = GoodTalentIcon;
                    }
                    else if (talentSchedule.Value == -1)
                    {
                        Image talentSlotImage = 도서관.GetComponent<ScheduleSlot>().TalentSlot1;
                        if (talentSlotImage.sprite != Transparent && talentSlotImage.sprite != BadTalentIcon)
                        {
                            talentSlotImage = 도서관.GetComponent<ScheduleSlot>().TalentSlot2;
                        }

                        talentSlotImage.sprite = BadTalentIcon;
                    }
                    break;

                case "봉사활동":
                    if (talentSchedule.Value == 1)
                    {
                        Image talentSlotImage = 봉사활동.GetComponent<ScheduleSlot>().TalentSlot1;
                        if (talentSlotImage.sprite != Transparent && talentSlotImage.sprite != GoodTalentIcon)
                        {
                            talentSlotImage = 봉사활동.GetComponent<ScheduleSlot>().TalentSlot2;
                        }

                        talentSlotImage.sprite = GoodTalentIcon;
                    }
                    else if (talentSchedule.Value == -1)
                    {
                        Image talentSlotImage = 봉사활동.GetComponent<ScheduleSlot>().TalentSlot1;
                        if (talentSlotImage.sprite != Transparent && talentSlotImage.sprite != BadTalentIcon)
                        {
                            talentSlotImage = 봉사활동.GetComponent<ScheduleSlot>().TalentSlot2;
                        }

                        talentSlotImage.sprite = BadTalentIcon;
                    }
                    break;
                case "레스토랑":
                    if (talentSchedule.Value == 1)
                    {
                        Image talentSlotImage = 레스토랑.GetComponent<ScheduleSlot>().TalentSlot1;
                        if (talentSlotImage.sprite != Transparent && talentSlotImage.sprite != GoodTalentIcon)
                        {
                            talentSlotImage = 레스토랑.GetComponent<ScheduleSlot>().TalentSlot2;
                        }

                        talentSlotImage.sprite = GoodTalentIcon;
                    }
                    else if (talentSchedule.Value == -1)
                    {
                        Image talentSlotImage = 레스토랑.GetComponent<ScheduleSlot>().TalentSlot1;
                        if (talentSlotImage.sprite != Transparent && talentSlotImage.sprite != BadTalentIcon)
                        {
                            talentSlotImage = 레스토랑.GetComponent<ScheduleSlot>().TalentSlot2;
                        }

                        talentSlotImage.sprite = BadTalentIcon;
                    }
                    break;
                case "카페":
                    if (talentSchedule.Value == 1)
                    {
                        Image talentSlotImage = 카페.GetComponent<ScheduleSlot>().TalentSlot1;
                        if (talentSlotImage.sprite != Transparent && talentSlotImage.sprite != GoodTalentIcon)
                        {
                            talentSlotImage = 카페.GetComponent<ScheduleSlot>().TalentSlot2;
                        }

                        talentSlotImage.sprite = GoodTalentIcon;
                    }
                    else if (talentSchedule.Value == -1)
                    {
                        Image talentSlotImage = 카페.GetComponent<ScheduleSlot>().TalentSlot1;
                        if (talentSlotImage.sprite != Transparent && talentSlotImage.sprite != BadTalentIcon)
                        {
                            talentSlotImage = 카페.GetComponent<ScheduleSlot>().TalentSlot2;
                        }

                        talentSlotImage.sprite = BadTalentIcon;
                    }
                    break;
                case "편의점":
                    if (talentSchedule.Value == 1)
                    {
                        Image talentSlotImage = 편의점.GetComponent<ScheduleSlot>().TalentSlot1;
                        if (talentSlotImage.sprite != Transparent && talentSlotImage.sprite != GoodTalentIcon)
                        {
                            talentSlotImage = 편의점.GetComponent<ScheduleSlot>().TalentSlot2;
                        }

                        talentSlotImage.sprite = GoodTalentIcon;
                    }
                    else if (talentSchedule.Value == -1)
                    {
                        Image talentSlotImage = 편의점.GetComponent<ScheduleSlot>().TalentSlot1;
                        if (talentSlotImage.sprite != Transparent && talentSlotImage.sprite != BadTalentIcon)
                        {
                            talentSlotImage = 편의점.GetComponent<ScheduleSlot>().TalentSlot2;
                        }

                        talentSlotImage.sprite = BadTalentIcon;
                    }
                    break;
                case "피시방":
                    if (talentSchedule.Value == 1)
                    {
                        Image talentSlotImage = 피시방.GetComponent<ScheduleSlot>().TalentSlot1;
                        if (talentSlotImage.sprite != Transparent && talentSlotImage.sprite != GoodTalentIcon)
                        {
                            talentSlotImage = 피시방.GetComponent<ScheduleSlot>().TalentSlot2;
                        }

                        talentSlotImage.sprite = GoodTalentIcon;
                    }
                    else if (talentSchedule.Value == -1)
                    {
                        Image talentSlotImage = 피시방.GetComponent<ScheduleSlot>().TalentSlot1;
                        if (talentSlotImage.sprite != Transparent && talentSlotImage.sprite != BadTalentIcon)
                        {
                            talentSlotImage = 피시방.GetComponent<ScheduleSlot>().TalentSlot2;
                        }

                        talentSlotImage.sprite = BadTalentIcon;
                    }
                    break;
                case "패스트푸드":
                    if (talentSchedule.Value == 1)
                    {
                        Image talentSlotImage = 패스트푸드.GetComponent<ScheduleSlot>().TalentSlot1;
                        if (talentSlotImage.sprite != Transparent && talentSlotImage.sprite != GoodTalentIcon)
                        {
                            talentSlotImage = 패스트푸드.GetComponent<ScheduleSlot>().TalentSlot2;
                        }

                        talentSlotImage.sprite = GoodTalentIcon;
                    }
                    else if (talentSchedule.Value == -1)
                    {
                        Image talentSlotImage = 패스트푸드.GetComponent<ScheduleSlot>().TalentSlot1;
                        if (talentSlotImage.sprite != Transparent && talentSlotImage.sprite != BadTalentIcon)
                        {
                            talentSlotImage = 패스트푸드.GetComponent<ScheduleSlot>().TalentSlot2;
                        }

                        talentSlotImage.sprite = BadTalentIcon;
                    }
                    break;
                case "전단지":
                    if (talentSchedule.Value == 1)
                    {
                        Image talentSlotImage = 전단지.GetComponent<ScheduleSlot>().TalentSlot1;
                        if (talentSlotImage.sprite != Transparent && talentSlotImage.sprite != GoodTalentIcon)
                        {
                            talentSlotImage = 전단지.GetComponent<ScheduleSlot>().TalentSlot2;
                        }

                        talentSlotImage.sprite = GoodTalentIcon;
                    }
                    else if (talentSchedule.Value == -1)
                    {
                        Image talentSlotImage = 전단지.GetComponent<ScheduleSlot>().TalentSlot1;
                        if (talentSlotImage.sprite != Transparent && talentSlotImage.sprite != BadTalentIcon)
                        {
                            talentSlotImage = 전단지.GetComponent<ScheduleSlot>().TalentSlot2;
                        }

                        talentSlotImage.sprite = BadTalentIcon;
                    }
                    break;
                case "인형탈":
                    if (talentSchedule.Value == 1)
                    {
                        Image talentSlotImage = 인형탈.GetComponent<ScheduleSlot>().TalentSlot1;
                        if (talentSlotImage.sprite != Transparent && talentSlotImage.sprite != GoodTalentIcon)
                        {
                            talentSlotImage = 인형탈.GetComponent<ScheduleSlot>().TalentSlot2;
                        }

                        talentSlotImage.sprite = GoodTalentIcon;
                    }
                    else if (talentSchedule.Value == -1)
                    {
                        Image talentSlotImage = 인형탈.GetComponent<ScheduleSlot>().TalentSlot1;
                        if (talentSlotImage.sprite != Transparent && talentSlotImage.sprite != BadTalentIcon)
                        {
                            talentSlotImage = 인형탈.GetComponent<ScheduleSlot>().TalentSlot2;
                        }

                        talentSlotImage.sprite = BadTalentIcon;
                    }
                    break;
                case "주유소":
                    if (talentSchedule.Value == 1)
                    {
                        Image talentSlotImage = 주유소.GetComponent<ScheduleSlot>().TalentSlot1;
                        if (talentSlotImage.sprite != Transparent && talentSlotImage.sprite != GoodTalentIcon)
                        {
                            talentSlotImage = 주유소.GetComponent<ScheduleSlot>().TalentSlot2;
                        }

                        talentSlotImage.sprite = GoodTalentIcon;
                    }
                    else if (talentSchedule.Value == -1)
                    {
                        Image talentSlotImage = 주유소.GetComponent<ScheduleSlot>().TalentSlot1;
                        if (talentSlotImage.sprite != Transparent && talentSlotImage.sprite != BadTalentIcon)
                        {
                            talentSlotImage = 주유소.GetComponent<ScheduleSlot>().TalentSlot2;
                        }

                        talentSlotImage.sprite = BadTalentIcon;
                    }
                    break;
                case "택배상하차":
                    if (talentSchedule.Value == 1)
                    {
                        Image talentSlotImage = 택배상하차.GetComponent<ScheduleSlot>().TalentSlot1;
                        if (talentSlotImage.sprite != Transparent && talentSlotImage.sprite != GoodTalentIcon)
                        {
                            talentSlotImage = 택배상하차.GetComponent<ScheduleSlot>().TalentSlot2;
                        }

                        talentSlotImage.sprite = GoodTalentIcon;
                    }
                    else if (talentSchedule.Value == -1)
                    {
                        Image talentSlotImage = 택배상하차.GetComponent<ScheduleSlot>().TalentSlot1;
                        if (talentSlotImage.sprite != Transparent && talentSlotImage.sprite != BadTalentIcon)
                        {
                            talentSlotImage = 택배상하차.GetComponent<ScheduleSlot>().TalentSlot2;
                        }

                        talentSlotImage.sprite = BadTalentIcon;
                    }
                    break;

                case "집에서휴식":
                    if (talentSchedule.Value == 1)
                    {
                        Image talentSlotImage = 집에서휴식.GetComponent<ScheduleSlot>().TalentSlot1;
                        if (talentSlotImage.sprite != Transparent && talentSlotImage.sprite != GoodTalentIcon)
                        {
                            talentSlotImage = 집에서휴식.GetComponent<ScheduleSlot>().TalentSlot2;
                        }

                        talentSlotImage.sprite = GoodTalentIcon;
                    }
                    else if (talentSchedule.Value == -1)
                    {
                        Image talentSlotImage = 집에서휴식.GetComponent<ScheduleSlot>().TalentSlot1;
                        if (talentSlotImage.sprite != Transparent && talentSlotImage.sprite != BadTalentIcon)
                        {
                            talentSlotImage = 집에서휴식.GetComponent<ScheduleSlot>().TalentSlot2;
                        }

                        talentSlotImage.sprite = BadTalentIcon;
                    }
                    break;
                case "친구만나기":
                    if (talentSchedule.Value == 1)
                    {
                        Image talentSlotImage = 친구만나기.GetComponent<ScheduleSlot>().TalentSlot1;
                        if (talentSlotImage.sprite != Transparent && talentSlotImage.sprite != GoodTalentIcon)
                        {
                            talentSlotImage = 친구만나기.GetComponent<ScheduleSlot>().TalentSlot2;
                        }

                        talentSlotImage.sprite = GoodTalentIcon;
                    }
                    else if (talentSchedule.Value == -1)
                    {
                        Image talentSlotImage = 친구만나기.GetComponent<ScheduleSlot>().TalentSlot1;
                        if (talentSlotImage.sprite != Transparent && talentSlotImage.sprite != BadTalentIcon)
                        {
                            talentSlotImage = 친구만나기.GetComponent<ScheduleSlot>().TalentSlot2;
                        }

                        talentSlotImage.sprite = BadTalentIcon;
                    }
                    break;
                case "게임하기":
                    if (talentSchedule.Value == 1)
                    {
                        Image talentSlotImage = 게임하기.GetComponent<ScheduleSlot>().TalentSlot1;
                        if (talentSlotImage.sprite != Transparent && talentSlotImage.sprite != GoodTalentIcon)
                        {
                            talentSlotImage = 게임하기.GetComponent<ScheduleSlot>().TalentSlot2;
                        }

                        talentSlotImage.sprite = GoodTalentIcon;
                    }
                    else if (talentSchedule.Value == -1)
                    {
                        Image talentSlotImage = 게임하기.GetComponent<ScheduleSlot>().TalentSlot1;
                        if (talentSlotImage.sprite != Transparent && talentSlotImage.sprite != BadTalentIcon)
                        {
                            talentSlotImage = 게임하기.GetComponent<ScheduleSlot>().TalentSlot2;
                        }

                        talentSlotImage.sprite = BadTalentIcon;
                    }
                    break;
                case "운동하기":
                    if (talentSchedule.Value == 1)
                    {
                        Image talentSlotImage = 운동하기.GetComponent<ScheduleSlot>().TalentSlot1;
                        if (talentSlotImage.sprite != Transparent && talentSlotImage.sprite != GoodTalentIcon)
                        {
                            talentSlotImage = 운동하기.GetComponent<ScheduleSlot>().TalentSlot2;
                        }

                        talentSlotImage.sprite = GoodTalentIcon;
                    }
                    else if (talentSchedule.Value == -1)
                    {
                        Image talentSlotImage = 운동하기.GetComponent<ScheduleSlot>().TalentSlot1;
                        if (talentSlotImage.sprite != Transparent && talentSlotImage.sprite != BadTalentIcon)
                        {
                            talentSlotImage = 운동하기.GetComponent<ScheduleSlot>().TalentSlot2;
                        }

                        talentSlotImage.sprite = BadTalentIcon;
                    }
                    break;
                case "영화보기":
                    if (talentSchedule.Value == 1)
                    {
                        Image talentSlotImage = 영화보기.GetComponent<ScheduleSlot>().TalentSlot1;
                        if (talentSlotImage.sprite != Transparent && talentSlotImage.sprite != GoodTalentIcon)
                        {
                            talentSlotImage = 영화보기.GetComponent<ScheduleSlot>().TalentSlot2;
                        }
                        talentSlotImage.sprite = GoodTalentIcon;
                    }
                    else if (talentSchedule.Value == -1)
                    {
                        Image talentSlotImage = 영화보기.GetComponent<ScheduleSlot>().TalentSlot1;
                        if (talentSlotImage.sprite != Transparent && talentSlotImage.sprite != BadTalentIcon)
                        {
                            talentSlotImage = 영화보기.GetComponent<ScheduleSlot>().TalentSlot2;
                        }

                        talentSlotImage.sprite = BadTalentIcon;
                    }
                    break;
                case "그림그리기":
                    if (talentSchedule.Value == 1)
                    {
                        Image talentSlotImage = 그림그리기.GetComponent<ScheduleSlot>().TalentSlot1;
                        if (talentSlotImage.sprite != Transparent && talentSlotImage.sprite != GoodTalentIcon)
                        {
                            talentSlotImage = 그림그리기.GetComponent<ScheduleSlot>().TalentSlot2;
                        }

                        talentSlotImage.sprite = GoodTalentIcon;
                    }
                    else if (talentSchedule.Value == -1)
                    {
                        Image talentSlotImage = 그림그리기.GetComponent<ScheduleSlot>().TalentSlot1;
                        if (talentSlotImage.sprite != Transparent && talentSlotImage.sprite != BadTalentIcon)
                        {
                            talentSlotImage = 그림그리기.GetComponent<ScheduleSlot>().TalentSlot2;
                        }

                        talentSlotImage.sprite = BadTalentIcon;
                    }
                    break;
                case "음악듣기":
                    if (talentSchedule.Value == 1)
                    {
                        Image talentSlotImage = 음악듣기.GetComponent<ScheduleSlot>().TalentSlot1;
                        if (talentSlotImage.sprite != Transparent && talentSlotImage.sprite != GoodTalentIcon)
                        {
                            talentSlotImage = 음악듣기.GetComponent<ScheduleSlot>().TalentSlot2;
                        }

                        talentSlotImage.sprite = GoodTalentIcon;
                    }
                    else if (talentSchedule.Value == -1)
                    {
                        Image talentSlotImage = 음악듣기.GetComponent<ScheduleSlot>().TalentSlot1;
                        if (talentSlotImage.sprite != Transparent && talentSlotImage.sprite != BadTalentIcon)
                        {
                            talentSlotImage = 음악듣기.GetComponent<ScheduleSlot>().TalentSlot2;
                        }

                        talentSlotImage.sprite = BadTalentIcon;
                    }
                    break;
                case "독서하기":
                    if (talentSchedule.Value == 1)
                    {
                        Image talentSlotImage = 독서하기.GetComponent<ScheduleSlot>().TalentSlot1;
                        if (talentSlotImage.sprite != Transparent && talentSlotImage.sprite != GoodTalentIcon)
                        {
                            talentSlotImage = 독서하기.GetComponent<ScheduleSlot>().TalentSlot2;
                        }

                        talentSlotImage.sprite = GoodTalentIcon;
                    }
                    else if (talentSchedule.Value == -1)
                    {
                        Image talentSlotImage = 독서하기.GetComponent<ScheduleSlot>().TalentSlot1;
                        if (talentSlotImage.sprite != Transparent && talentSlotImage.sprite != BadTalentIcon)
                        {
                            talentSlotImage = 독서하기.GetComponent<ScheduleSlot>().TalentSlot2;
                        }

                        talentSlotImage.sprite = BadTalentIcon;
                    }
                    break;
                case "맛집탐방":
                    if (talentSchedule.Value == 1)
                    {
                        Image talentSlotImage = 맛집탐방.GetComponent<ScheduleSlot>().TalentSlot1;
                        if (talentSlotImage.sprite != Transparent && talentSlotImage.sprite != GoodTalentIcon)
                        {
                            talentSlotImage = 맛집탐방.GetComponent<ScheduleSlot>().TalentSlot2;
                        }

                        talentSlotImage.sprite = GoodTalentIcon;
                    }
                    else if (talentSchedule.Value == -1)
                    {
                        Image talentSlotImage = 맛집탐방.GetComponent<ScheduleSlot>().TalentSlot1;
                        if (talentSlotImage.sprite != Transparent && talentSlotImage.sprite != BadTalentIcon)
                        {
                            talentSlotImage = 맛집탐방.GetComponent<ScheduleSlot>().TalentSlot2;
                        }

                        talentSlotImage.sprite = BadTalentIcon;
                    }
                    break;
                case "종교활동":
                    if (talentSchedule.Value == 1)
                    {
                        Image talentSlotImage = 종교활동.GetComponent<ScheduleSlot>().TalentSlot1;
                        if (talentSlotImage.sprite != Transparent && talentSlotImage.sprite != GoodTalentIcon)
                        {
                            talentSlotImage = 종교활동.GetComponent<ScheduleSlot>().TalentSlot2;
                        }

                        talentSlotImage.sprite = GoodTalentIcon;
                    }
                    else if (talentSchedule.Value == -1)
                    {
                        Image talentSlotImage = 종교활동.GetComponent<ScheduleSlot>().TalentSlot1;
                        if (talentSlotImage.sprite != Transparent && talentSlotImage.sprite != BadTalentIcon)
                        {
                            talentSlotImage = 종교활동.GetComponent<ScheduleSlot>().TalentSlot2;
                        }

                        talentSlotImage.sprite = BadTalentIcon;
                    }
                    break;
            }
        }
    }
    public void LoadInterestSchedule()
    {
        foreach (KeyValuePair<ScheduleName, int> interestSchedule in SonManager.SonData.InterestSchedules)
        {
            switch (interestSchedule.Key.ToString())
            {
                case "학원가기":
                    if (interestSchedule.Value == 1)
                    {
                        학원가기.GetComponent<ScheduleSlot>().TalentSlot1.sprite = GoodInterestIcon;
                    }
                    else if (interestSchedule.Value == -1)
                    {
                        학원가기.GetComponent<ScheduleSlot>().TalentSlot1.sprite = BadInterestIcon;
                    }
                    break;
                case "과외":
                    if (interestSchedule.Value == 1)
                    {
                        과외.GetComponent<ScheduleSlot>().TalentSlot1.sprite = GoodInterestIcon;
                    }
                    else if (interestSchedule.Value == -1)
                    {
                        과외.GetComponent<ScheduleSlot>().TalentSlot1.sprite = BadInterestIcon;
                    }
                    break;
                case "스터디그룹":
                    if (interestSchedule.Value == 1)
                    {
                        스터디그룹.GetComponent<ScheduleSlot>().TalentSlot1.sprite = GoodInterestIcon;
                    }
                    else if (interestSchedule.Value == -1)
                    {
                        스터디그룹.GetComponent<ScheduleSlot>().TalentSlot1.sprite = BadInterestIcon;
                    }
                    break;
                case "도서관":
                    if (interestSchedule.Value == 1)
                    {
                        도서관.GetComponent<ScheduleSlot>().TalentSlot1.sprite = GoodInterestIcon;
                    }
                    else if (interestSchedule.Value == -1)
                    {
                        도서관.GetComponent<ScheduleSlot>().TalentSlot1.sprite = BadInterestIcon;
                    }
                    break;

                case "봉사활동":
                    if (interestSchedule.Value == 1)
                    {
                        봉사활동.GetComponent<ScheduleSlot>().TalentSlot1.sprite = GoodInterestIcon;
                    }
                    else if (interestSchedule.Value == -1)
                    {
                        봉사활동.GetComponent<ScheduleSlot>().TalentSlot1.sprite = BadInterestIcon;
                    }
                    break;
                case "레스토랑":
                    if (interestSchedule.Value == 1)
                    {
                        레스토랑.GetComponent<ScheduleSlot>().TalentSlot1.sprite = GoodInterestIcon;
                    }
                    else if (interestSchedule.Value == -1)
                    {
                        레스토랑.GetComponent<ScheduleSlot>().TalentSlot1.sprite = BadInterestIcon;
                    }
                    break;
                case "카페":
                    if (interestSchedule.Value == 1)
                    {
                        카페.GetComponent<ScheduleSlot>().TalentSlot1.sprite = GoodInterestIcon;
                    }
                    else if (interestSchedule.Value == -1)
                    {
                        카페.GetComponent<ScheduleSlot>().TalentSlot1.sprite = BadInterestIcon;
                    }
                    break;
                case "편의점":
                    if (interestSchedule.Value == 1)
                    {
                        편의점.GetComponent<ScheduleSlot>().TalentSlot1.sprite = GoodInterestIcon;
                    }
                    else if (interestSchedule.Value == -1)
                    {
                        편의점.GetComponent<ScheduleSlot>().TalentSlot1.sprite = BadInterestIcon;
                    }
                    break;
                case "피시방":
                    if (interestSchedule.Value == 1)
                    {
                        피시방.GetComponent<ScheduleSlot>().TalentSlot1.sprite = GoodInterestIcon;
                    }
                    else if (interestSchedule.Value == -1)
                    {
                        피시방.GetComponent<ScheduleSlot>().TalentSlot1.sprite = BadInterestIcon;
                    }
                    break;
                case "패스트푸드":
                    if (interestSchedule.Value == 1)
                    {
                        패스트푸드.GetComponent<ScheduleSlot>().TalentSlot1.sprite = GoodInterestIcon;
                    }
                    else if (interestSchedule.Value == -1)
                    {
                        패스트푸드.GetComponent<ScheduleSlot>().TalentSlot1.sprite = BadInterestIcon;
                    }
                    break;
                case "전단지":
                    if (interestSchedule.Value == 1)
                    {
                        전단지.GetComponent<ScheduleSlot>().TalentSlot1.sprite = GoodInterestIcon;
                    }
                    else if (interestSchedule.Value == -1)
                    {
                        전단지.GetComponent<ScheduleSlot>().TalentSlot1.sprite = BadInterestIcon;
                    }
                    break;
                case "인형탈":
                    if (interestSchedule.Value == 1)
                    {
                        인형탈.GetComponent<ScheduleSlot>().TalentSlot1.sprite = GoodInterestIcon;
                    }
                    else if (interestSchedule.Value == -1)
                    {
                        인형탈.GetComponent<ScheduleSlot>().TalentSlot1.sprite = BadInterestIcon;
                    }
                    break;
                case "주유소":
                    if (interestSchedule.Value == 1)
                    {
                        주유소.GetComponent<ScheduleSlot>().TalentSlot1.sprite = GoodInterestIcon;
                    }
                    else if (interestSchedule.Value == -1)
                    {
                        주유소.GetComponent<ScheduleSlot>().TalentSlot1.sprite = BadInterestIcon;
                    }
                    break;
                case "택배상하차":
                    if (interestSchedule.Value == 1)
                    {
                        택배상하차.GetComponent<ScheduleSlot>().TalentSlot1.sprite = GoodInterestIcon;
                    }
                    else if (interestSchedule.Value == -1)
                    {
                        택배상하차.GetComponent<ScheduleSlot>().TalentSlot1.sprite = BadInterestIcon;
                    }
                    break;

                case "집에서휴식":
                    if (interestSchedule.Value == 1)
                    {
                        집에서휴식.GetComponent<ScheduleSlot>().TalentSlot1.sprite = GoodInterestIcon;
                    }
                    else if (interestSchedule.Value == -1)
                    {
                        집에서휴식.GetComponent<ScheduleSlot>().TalentSlot1.sprite = BadInterestIcon;
                    }
                    break;
                case "친구만나기":
                    if (interestSchedule.Value == 1)
                    {
                        친구만나기.GetComponent<ScheduleSlot>().TalentSlot1.sprite = GoodInterestIcon;
                    }
                    else if (interestSchedule.Value == -1)
                    {
                        친구만나기.GetComponent<ScheduleSlot>().TalentSlot1.sprite = BadInterestIcon;
                    }
                    break;
                case "게임하기":
                    if (interestSchedule.Value == 1)
                    {
                        게임하기.GetComponent<ScheduleSlot>().TalentSlot1.sprite = GoodInterestIcon;
                    }
                    else if (interestSchedule.Value == -1)
                    {
                        게임하기.GetComponent<ScheduleSlot>().TalentSlot1.sprite = BadInterestIcon;
                    }
                    break;
                case "운동하기":
                    if (interestSchedule.Value == 1)
                    {
                        운동하기.GetComponent<ScheduleSlot>().TalentSlot1.sprite = GoodInterestIcon;
                    }
                    else if (interestSchedule.Value == -1)
                    {
                        운동하기.GetComponent<ScheduleSlot>().TalentSlot1.sprite = BadInterestIcon;
                    }
                    break;
                case "영화보기":
                    if (interestSchedule.Value == 1)
                    {
                        영화보기.GetComponent<ScheduleSlot>().TalentSlot1.sprite = GoodInterestIcon;
                    }
                    else if (interestSchedule.Value == -1)
                    {
                        영화보기.GetComponent<ScheduleSlot>().TalentSlot1.sprite = BadInterestIcon;
                    }
                    break;
                case "그림그리기":
                    if (interestSchedule.Value == 1)
                    {
                        그림그리기.GetComponent<ScheduleSlot>().TalentSlot1.sprite = GoodInterestIcon;
                    }
                    else if (interestSchedule.Value == -1)
                    {
                        그림그리기.GetComponent<ScheduleSlot>().TalentSlot1.sprite = BadInterestIcon;
                    }
                    break;
                case "음악듣기":
                    if (interestSchedule.Value == 1)
                    {
                        음악듣기.GetComponent<ScheduleSlot>().TalentSlot1.sprite = GoodInterestIcon;
                    }
                    else if (interestSchedule.Value == -1)
                    {
                        음악듣기.GetComponent<ScheduleSlot>().TalentSlot1.sprite = BadInterestIcon;
                    }
                    break;
                case "독서하기":
                    if (interestSchedule.Value == 1)
                    {
                        독서하기.GetComponent<ScheduleSlot>().TalentSlot1.sprite = GoodInterestIcon;
                    }
                    else if (interestSchedule.Value == -1)
                    {
                        독서하기.GetComponent<ScheduleSlot>().TalentSlot1.sprite = BadInterestIcon;
                    }
                    break;
                case "맛집탐방":
                    if (interestSchedule.Value == 1)
                    {
                        맛집탐방.GetComponent<ScheduleSlot>().TalentSlot1.sprite = GoodInterestIcon;
                    }
                    else if (interestSchedule.Value == -1)
                    {
                        맛집탐방.GetComponent<ScheduleSlot>().TalentSlot1.sprite = BadInterestIcon;
                    }
                    break;
                case "종교활동":
                    if (interestSchedule.Value == 1)
                    {
                        종교활동.GetComponent<ScheduleSlot>().TalentSlot1.sprite = GoodInterestIcon;
                    }
                    else if (interestSchedule.Value == -1)
                    {
                        종교활동.GetComponent<ScheduleSlot>().TalentSlot1.sprite = BadInterestIcon;
                    }
                    break;
            }
        }
    }
}
