﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using BayatGames.SaveGameFree;

public class EndingManager : MonoBehaviour
{
    public Text EndingTitle;
    public Text EndingContext;

    void Start()
    {
        EndingTitle.text = SaveGame.Load<string>("EndingTitle");
        EndingContext.text = SaveGame.Load<string>("EndingContext");
    }

    public void LoadScene()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
