﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomManager : MonoBehaviour
{

    public GameObject QuitPanelUI;
    public GameObject OptionPanelUI;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            ActivePanel("quit");
    }

    public void QuitGame()
    {
        Application.Quit();
    }
    public void ActivePanel(string panelName)
    {
        switch (panelName)
        {
            case "quit":
                ActivePanel(QuitPanelUI);
                break;
            case "option":
                ActivePanel(OptionPanelUI);
                break;
        }
    }

    public void InactivePanel(string panelName)
    {
        switch (panelName)
        {
            case "quit":
                InactivePanel(QuitPanelUI);
                break;
            case "option":
                InactivePanel(OptionPanelUI);
                break;
        }
    }
    public void ActivePanel(GameObject panel)
    {
        panel.SetActive(true);
    }

    public void InactivePanel(GameObject panel)
    {
        panel.SetActive(false);
    }

}
