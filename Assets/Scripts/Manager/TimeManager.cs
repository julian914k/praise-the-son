﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using BayatGames.SaveGameFree;

public class TimeManager : MonoBehaviour
{
    public static TimeManager Instance = null;
    public GameObject ProgressPanel;
    public Animator ProgressAnimator;
    public GameObject StartTimer;
    public GameObject StopTimer;

    public Text MonthText;
    public Text DayText;

    [SerializeField]
    private int year;
    [SerializeField]
    private int month;
    [SerializeField]
    private int day;
    [SerializeField]
    private int phase;

    private bool hasStudying = false;
    private bool hasArbeit = false;
    private bool hasVolunteering = false;
    private bool hasResting = false;
    private bool hasHangout = false;

    private bool allowTimer = false;

    public int GetYear()
    {
        return year;
    }
    public int GetMonth()
    {
        return month;
    }
    public int GetDay()
    {
        return day;
    }
    public int GetPhase()
    {
        return phase;
    }
    public void SetMonth(int month)
    {
        this.month = month;
    }
    public void SetPhase(int phase)
    {
        this.phase = phase;
    }

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }

    // Use this for initialization
    void Start()
    {
        allowTimer = false;
        LoadDate();
        MonthText.text = month.ToString();
        DayText.text = day.ToString();
    }
    private void UpdateTime()
    {
        if (allowTimer)
        {
            if (phase == 0)
            {
                EventManager.Instance.AddEventQueue();
                phase++;
            }
            else
            {
                EventManager.Instance.AddTriggerEvent();
                EventManager.Instance.GenerateEvent();
            }

            if (phase >= 10)
            {
                if (day % 7 == 0)
                {
                    ScheduleManager.Instance.SetScheduleAttributes();
                }
                day++;
                phase = 0;
                ScheduleManager.Instance.DecreaseLockedDownDay();
            }


            UpdateCalendar();
            // 매달 1일로 바뀌는 순간 아들의 능력치 저장
            if (day == 1 && phase == 0)
            {
                SonManager.Instance.CompareLastSonAttributes();
                SonManager.SonData.SaveSonLastAttributes();
            }
            // 현재 날짜/시간/페이즈 저장
            SaveDate();
        }
    }
    public void UpdateCalendar()
    {
        switch (month)
        {
            case 2:
                if (day > 28)
                {
                    month++;
                    day %= 28;
                }
                break;
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                if (day > 31)
                {
                    month++;
                    day %= 31;
                }
                break;
            case 4:
            case 6:
            case 9:
            case 11:
                if (day > 30)
                {
                    month++;
                    day %= 30;
                }
                break;
        }
        if (month > 12)
        {
            year++;
            month %= 12;
        }

        MonthText.text = month.ToString();
        DayText.text = day.ToString();
    }
    public void StartTime()
    {
        if (!allowTimer)
        {
            allowTimer = true;
            SwitchTimerButton();

            //EventManager.Instance.DarkEventBackgroundPanel.SetActive(true);
            ProgressPanel.SetActive(true);
            SetProgressAnimation();
            //"UpdateTime", 플레이 버튼 이후 진행 지연 시간, 진행 속도 0.01=10배, 0.001=100배 
            InvokeRepeating("UpdateTime", 1.0f, 0.05f);
        }
    }
    public void StopTime()
    {
        if (allowTimer)
        {
            allowTimer = false;
            SwitchTimerButton();

            //EventManager.Instance.DarkEventBackgroundPanel.SetActive(false);
            ProgressPanel.SetActive(false);
            InitProgressType();
            CancelInvoke("UpdateTime");
        }
    }
    public void SaveDate()
    {
        SaveGame.Save<int>("Year", year);
        SaveGame.Save<int>("Month", month);
        SaveGame.Save<int>("Day", day);
        SaveGame.Save<int>("Phase", phase);
    }
    public void LoadDate()
    {
        year = SaveGame.Load<int>("Year");
        month = SaveGame.Load<int>("Month");
        day = SaveGame.Load<int>("Day");
        phase = SaveGame.Load<int>("Phase");
    }
    public void SwitchTimerButton()
    {
        StopTimer.SetActive(allowTimer);
        StartTimer.SetActive(!allowTimer);
    }
    private void InitProgressType()
    {
        hasStudying = false;
        hasArbeit = false;
        hasHangout = false;
        hasResting = false;
        hasVolunteering = false;
    }
    private void SetProgressAnimation()
    {
        SetProgressType(ScheduleManager.Instance.MorningScheduleText.text);
        SetProgressType(ScheduleManager.Instance.EveningScheduleText.text);
        SetProgressType(ScheduleManager.Instance.NightScheduleText.text);
        SetProgressType(ScheduleManager.Instance.HolidayScheduleText.text);
        if (!hasStudying && !hasVolunteering && !hasArbeit && !hasResting && !hasHangout) hasHangout = true;

        List<string> randomAnimationClips = new List<string>();
        if (hasStudying) randomAnimationClips.Add("hasStudying");
        if (hasVolunteering) randomAnimationClips.Add("hasVolunteering");
        if (hasArbeit) randomAnimationClips.Add("hasArbeit");
        if (hasResting) randomAnimationClips.Add("hasResting");
        if (hasHangout) randomAnimationClips.Add("hasHangout");

        if (randomAnimationClips.Count == 0)
        {
            ProgressAnimator.SetTrigger("hasHangout");
        }
        else
        {
            ProgressAnimator.SetTrigger(randomAnimationClips[Random.Range(0, randomAnimationClips.Count)]);
        }
    }
    private void SetProgressType(string scheduleText)
    {
        switch (scheduleText)
        {
            case "학교가기":
            case "학원가기":
            case "과외":
            case "스터디그룹":
            case "도서관":
                hasStudying = true;
                break;
            case "봉사활동":
                hasVolunteering = true;
                break;
            case "레스토랑":
            case "카페":
            case "편의점":
            case "피시방":
            case "패스트푸드":
            case "전단지":
            case "인형탈":
            case "주유소":
            case "택배상하차":
                hasArbeit = true;
                break;
            case "집에서휴식":
                hasResting = true;
                break;
            case "친구만나기":
                hasHangout = true;
                break;
            case "게임하기":
            case "운동하기":
            case "영화보기":
            case "그림그리기":
            case "음악듣기":
            case "독서하기":
            case "맛집탐방":
            case "종교활동":
                break;
        }
    }
}
