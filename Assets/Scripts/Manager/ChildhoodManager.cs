﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using BayatGames.SaveGameFree;

public class ChildhoodManager : MonoBehaviour
{
    public static SonData SonData = null;
    public Image BirthdayPartyImage;
    public Image TheaterRoleImage;
    public Image BirthdayPresentImage;
    public Image FamilyPicnicImage;
    [Header("Event Sprite")]
    public Sprite BirthdayPartySprite;
    public Sprite TheaterRoleSprite;
    public Sprite BirthdayPresentSprite;
    public Sprite FamilyPicnicSprite;
    [Header("Choice Sprite")]
    public Sprite 공;
    public Sprite 연필;
    public Sprite 바늘;
    public Sprite 물감;
    public Sprite 왕자;
    public Sprite 공주;
    public Sprite 나무;
    public Sprite 개구리;
    public Sprite 로봇;
    public Sprite 동화책;
    public Sprite 실로폰;
    public Sprite 게임기;
    public Sprite 캠핑장;
    public Sprite 박물관;
    public Sprite 놀이공원;
    public Sprite 동물원;
    [Header("General Object")]
    public GameObject NextButton;
    public GameObject InfoEventPanel;
    public Image InfoEventImage;
    public Text InfoEventTitle;
    public Text InfoEventContext;
    public GameObject ChoiceEventPanel;
    public Text ChoiceEventTitle;
    public Image ChoiceEventImage1;
    public Image ChoiceEventImage2;
    public Image ChoiceEventImage3;
    public Image ChoiceEventImage4;
    public Text ChoiceEventText1;
    public Text ChoiceEventText2;
    public Text ChoiceEventText3;
    public Text ChoiceEventText4;
    public GameObject BirthdayPartyPanel;
    public GameObject TheaterRolePanel;
    public GameObject BirthdayPresentPanel;
    public GameObject FamilyPicnicPanel;
    public Text BirthdayPartyTitle;
    public Text BirthdayPartyContext;
    public Text TheaterRoleTitle;
    public Text TheaterRoleContext;
    public Text BirthdayPresentTitle;
    public Text BirthdayPresentContext;
    public Text FamilyPicnicTitle;
    public Text FamilyPicnicContext;
    public ChildHood CurrentChildhood;
    void Start()
    {
        SonData = new SonData();

        if (!SaveGame.Exists("CurrentChildhood"))
        {
            SaveGame.Save<ChildHood>("CurrentChildhood", ChildHood.BirthdayParty);
        }
        SaveGame.Load<ChildHood>("CurrentChildhood");

        LoadChildhood();
    }
    public void LoadChildhood()
    {
        CurrentChildhood = SaveGame.Load<ChildHood>("CurrentChildhood");

        switch (CurrentChildhood)
        {
            case ChildHood.TheaterRole:
                GetBirthdayParty();
                break;
            case ChildHood.BirthdayPresent:
                GetBirthdayParty();
                GetTheaterRole();
                break;
            case ChildHood.FamilyPicnic:
                GetBirthdayParty();
                GetTheaterRole();
                GetBirthdayPresent();
                break;
            case ChildHood.Done:
                GetBirthdayParty();
                GetTheaterRole();
                GetBirthdayPresent();
                GetFamilyPicnic();
                NextButton.SetActive(true);
                break;
        }
    }
    public void ChoiceChildhoodEvent(int choiceNumber)
    {
        string choiceEventText = null;
        switch (choiceNumber)
        {
            case 1:
                choiceEventText = ChoiceEventText1.text;
                break;
            case 2:
                choiceEventText = ChoiceEventText2.text;
                break;
            case 3:
                choiceEventText = ChoiceEventText3.text;
                break;
            case 4:
                choiceEventText = ChoiceEventText4.text;
                break;
        }

        switch (CurrentChildhood)
        {
            case ChildHood.BirthdayParty:
                SetBirthdayParty(choiceEventText);
                break;
            case ChildHood.TheaterRole:
                SetTheaterRole(choiceEventText);
                break;
            case ChildHood.BirthdayPresent:
                SetBirthdayPresent(choiceEventText);
                break;
            case ChildHood.FamilyPicnic:
                SetFamilyPicnic(choiceEventText);
                // 다음 씬으로 이동 버튼 활성화
                NextButton.SetActive(true);
                break;
        }

        InactivePanel(ChoiceEventPanel);
    }
    public void GenerateInfoEvent()
    {
        switch (CurrentChildhood)
        {
            case ChildHood.BirthdayParty:
                InfoEventImage.sprite = BirthdayPartySprite;
                InfoEventTitle.text = "돌잔치";
                InfoEventContext.text = "아들의 첫 번째 생일. 가족과 친척들이 모여 축하해 주었고, 이제 돌잡이를 할 차례입니다. 이때, 아들이 잡은 물건은...";
                ChoiceEventTitle.text = InfoEventTitle.text;
                ChoiceEventImage1.sprite = 공;
                ChoiceEventImage2.sprite = 연필;
                ChoiceEventImage3.sprite = 바늘;
                ChoiceEventImage4.sprite = 물감;
                ChoiceEventText1.text = "공";
                ChoiceEventText2.text = "연필";
                ChoiceEventText3.text = "바늘";
                ChoiceEventText4.text = "물감";
                break;
            case ChildHood.TheaterRole:
                InfoEventImage.sprite = TheaterRoleSprite;
                InfoEventTitle.text = "연극";
                InfoEventContext.text = "유치원 연극이 있는 날입니다. 다른 부모님들도 모두 아이들의 연극을 기다리고 있습니다. 연극이 시작되고, 아들의 배역은...";
                ChoiceEventTitle.text = InfoEventTitle.text;
                ChoiceEventImage1.sprite = 왕자;
                ChoiceEventImage2.sprite = 공주;
                ChoiceEventImage3.sprite = 나무;
                ChoiceEventImage4.sprite = 개구리;
                ChoiceEventText1.text = "왕자";
                ChoiceEventText2.text = "공주";
                ChoiceEventText3.text = "나무";
                ChoiceEventText4.text = "개구리";
                break;
            case ChildHood.BirthdayPresent:
                InfoEventImage.sprite = BirthdayPresentSprite;
                InfoEventTitle.text = "생일선물";
                InfoEventContext.text = "아들의 5번째 생일입니다! 생일을 기념하며, 축하파티와 함께 선물을 준비했습니다. 그 선물은...";
                ChoiceEventTitle.text = InfoEventTitle.text;
                ChoiceEventImage1.sprite = 로봇;
                ChoiceEventImage2.sprite = 동화책;
                ChoiceEventImage3.sprite = 실로폰;
                ChoiceEventImage4.sprite = 게임기;
                ChoiceEventText1.text = "로봇";
                ChoiceEventText2.text = "동화책";
                ChoiceEventText3.text = "실로폰";
                ChoiceEventText4.text = "게임기";
                break;
            case ChildHood.FamilyPicnic:
                InfoEventImage.sprite = FamilyPicnicSprite;
                InfoEventTitle.text = "소풍";
                InfoEventContext.text = "화창한 날씨의 여름이 다가오자, 가족들과 함께 여행을 가기로 했습니다. 이번 여행의 목적지는...";
                ChoiceEventTitle.text = InfoEventTitle.text;
                ChoiceEventImage1.sprite = 캠핑장;
                ChoiceEventImage2.sprite = 박물관;
                ChoiceEventImage3.sprite = 놀이공원;
                ChoiceEventImage4.sprite = 동물원;
                ChoiceEventText1.text = "캠핑장";
                ChoiceEventText2.text = "박물관";
                ChoiceEventText3.text = "놀이공원";
                ChoiceEventText4.text = "동물원";
                break;
        }
        ActivePanel(InfoEventPanel);
    }
    public void GenerateChoiceEvent()
    {
        InactivePanel(InfoEventPanel);
        ActivePanel(ChoiceEventPanel);
    }
    public string FindContextFromTitle(string title)
    {
        switch (title)
        {
            case "공":
                return "아들은 돌잔치에서 공을 집었다. 운동에 관심이 있나?";
            case "연필":
                return "아들은 돌잔치에서 연필을 집었다. 공부에 관심이 있나?";
            case "바늘":
                return "아들은 돌잔치에서 공을 집었다. 손재주가 좋은 아이인가보다.";
            case "물감":
                return "아들은 돌잔치에서 물감을 집었다. 예술가의 재능이 있는걸까?";
            case "개구리":
                return "아들의 배역은 개구리였다. 개굴~ 개굴~ 개굴개굴~ ";
            case "왕자":
                return "아들의 배역은 왕자였다. 잘생기고 용감한 왕자 역할이 잘 어울린다.";
            case "나무":
                return "아들의 배역은 나무였다. 그저 서 있는 것만으로 제 역할을 해냈다!";
            case "공주":
                return "아들의 배역은 공주였다. 수줍은 공주의 모습도 마냥 사랑스럽다!";
            case "로봇":
                return "아들의 생일에 로봇을 선물했다.";
            case "동화책":
                return "아들의 생일에 동화책을 선물했다.";
            case "실로폰":
                return "아들의 생일에 실로폰을 선물했다.";
            case "게임기":
                return "아들의 생일에 게임기를 선물했다.";
            case "캠핑장":
                return "가족 여행으로 캠핑장에 가서, 바베큐 파티를 벌였다.";
            case "박물관":
                return "가족 여행으로 박물관에 가서, 여러가지 유물들을 구경했다.";
            case "놀이공원":
                return "가족 여행으로 놀이공원에 가서, 재미있는 놀이기구를 타고 놀았다.";
            case "동물원":
                return "가족 여행으로 동물들에 가서, 신기한 동물들을 만났다.";
        }
        return null;
    }
    public void SetBirthdayParty(string birthdayParty)
    {
        SaveGame.Save<string>("BirthdayParty", birthdayParty);
        CurrentChildhood = ChildHood.TheaterRole;
        SaveGame.Save<ChildHood>("CurrentChildhood", CurrentChildhood);
        GetBirthdayParty();
    }
    public void SetTheaterRole(string theaterRole)
    {
        SaveGame.Save<string>("TheaterRole", theaterRole);
        CurrentChildhood = ChildHood.BirthdayPresent;
        SaveGame.Save<ChildHood>("CurrentChildhood", CurrentChildhood);
        GetTheaterRole();
    }
    public void SetBirthdayPresent(string birthdayPresent)
    {
        SaveGame.Save<string>("BirthdayPresent", birthdayPresent);
        CurrentChildhood = ChildHood.FamilyPicnic;
        SaveGame.Save<ChildHood>("CurrentChildhood", CurrentChildhood);
        GetBirthdayPresent();
    }
    public void SetFamilyPicnic(string familyPicnic)
    {
        SaveGame.Save<string>("FamilyPicnic", familyPicnic);
        CurrentChildhood = ChildHood.Done;
        SaveGame.Save<ChildHood>("CurrentChildhood", CurrentChildhood);
        GetFamilyPicnic();
    }
    public void GetBirthdayParty()
    {
        BirthdayPartyTitle.text = "돌잔치";
        BirthdayPartyContext.text = FindContextFromTitle(SaveGame.Load<string>("BirthdayParty"));
        BirthdayPartyImage.sprite = BirthdayPartySprite;
        ActivePanel("BirthdayParty");
    }
    public void GetTheaterRole()
    {
        TheaterRoleTitle.text = "연극";
        TheaterRoleContext.text = FindContextFromTitle(SaveGame.Load<string>("TheaterRole"));
        TheaterRoleImage.sprite = TheaterRoleSprite;
        ActivePanel("TheaterRole");
    }
    public void GetBirthdayPresent()
    {
        BirthdayPresentTitle.text = "생일선물";
        BirthdayPresentContext.text = FindContextFromTitle(SaveGame.Load<string>("BirthdayPresent"));
        BirthdayPresentImage.sprite = BirthdayPresentSprite;
        ActivePanel("BirthdayPresent");
    }
    public void GetFamilyPicnic()
    {
        FamilyPicnicTitle.text = "소풍";
        FamilyPicnicContext.text = FindContextFromTitle(SaveGame.Load<string>("FamilyPicnic"));
        FamilyPicnicImage.sprite = FamilyPicnicSprite;
        ActivePanel("FamilyPicnic");
    }
    public void ActivePanel(string panelName)
    {
        switch (panelName)
        {
            case "BirthdayParty":
                ActivePanel(BirthdayPartyPanel);
                break;
            case "TheaterRole":
                ActivePanel(TheaterRolePanel);
                break;
            case "BirthdayPresent":
                ActivePanel(BirthdayPresentPanel);
                break;
            case "FamilyPicnic":
                ActivePanel(FamilyPicnicPanel);
                break;
        }
    }
    public void ActivePanel(GameObject panel)
    {
        panel.SetActive(true);
    }
    public void InactivePanel(string panelName)
    {
        switch (panelName)
        {
            case "BirthdayParty":
                InactivePanel(BirthdayPartyPanel);
                break;
            case "TheaterRole":
                InactivePanel(TheaterRolePanel);
                break;
            case "BirthdayPresent":
                InactivePanel(BirthdayPresentPanel);
                break;
            case "FamilyPicnic":
                InactivePanel(FamilyPicnicPanel);
                break;
        }
    }
    public void InactivePanel(GameObject panel)
    {
        panel.SetActive(false);
    }
    public void LoadScene()
    {
        // 흥미 & 재능 테스트 추가
        SonData.TalentSchedules[ScheduleName.게임하기] = -1;
        SonData.InterestSchedules[ScheduleName.게임하기] = 1;
        SonData.TalentSchedules[ScheduleName.독서하기] = 11;
        SonData.InterestSchedules[ScheduleName.독서하기] = -1;

        SonData.TalentSchedules[ScheduleName.그림그리기] = 1;
        SonData.TalentSchedules[ScheduleName.영화보기] = 1;
        SonData.InterestSchedules[ScheduleName.운동하기] = -1;
        SonData.InterestSchedules[ScheduleName.레스토랑] = 1;

        SaveGame.Save<Dictionary<ScheduleName, int>>("TalentSchedules", SonData.TalentSchedules);
        SaveGame.Save<Dictionary<ScheduleName, int>>("InterestSchedules", SonData.InterestSchedules);

        // 성격 테스트 추가
        //SaveGame.Save<PersonalityTrait>("PersonalityTrait1", PersonalityTrait.나태한);
        //SaveGame.Save<PersonalityTrait>("PersonalityTrait2", PersonalityTrait.쾌활한);

        SceneManager.LoadScene("Room");
    }
}
public enum ChildHood
{
    BirthdayParty, TheaterRole, BirthdayPresent, FamilyPicnic, Done
}