﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using BayatGames.SaveGameFree;

public class SonManager : MonoBehaviour
{
    public static SonManager Instance = null;
    public static SonData SonData = null;
    [Header("Info Text")]
    public Text NameText;
    public Text JobText;
    public Text DreamText;
    public Text EmotionText;

    [Header("Attributes Value")]
    public Text PhysicalValue;
    public Text IntelligenceValue;
    public Text SensibilityValue;
    public Text CraftValue;
    public Text SociabilityValue;
    public Text CharmValue;
    public Text NotionValue;
    public Text AthleticValue;
    public Text EducationValue;
    public Text ArtValue;
    public Text TechnicValue;

    [Header("Attributes Index")]
    public Image PhysicalIndexImage;
    public Image IntelligenceIndexImage;
    public Image SensibilityIndexImage;
    public Image CraftIndexImage;
    public Image SociabilityIndexImage;
    public Image CharmIndexImage;
    public Image NotionIndexImage;

    [Header("Attributes Index Arrow Sprites")]
    public Sprite BigUpSprite;
    public Sprite SmallUpSprite;
    public Sprite UnchangedSprite;
    public Sprite SmallDownSprite;
    public Sprite BigDownSprite;

    [Header("Battery Image")]
    public GameObject IntimacyBattery1;
    public GameObject IntimacyBattery2;
    public GameObject IntimacyBattery3;
    public GameObject IntimacyBattery4;
    public GameObject IntimacyBattery5;
    public GameObject StressBattery1;
    public GameObject StressBattery2;
    public GameObject StressBattery3;
    public GameObject StressBattery4;
    public GameObject StressBattery5;
    public GameObject WealthBattery1;
    public GameObject WealthBattery2;
    public GameObject WealthBattery3;
    public GameObject WealthBattery4;
    public GameObject WealthBattery5;
    [Header("Emotion Image")]
    public Image EmotionImage;
    [Header("Traits")]
    public Image PersonalityTraitImage1;
    public Image PersonalityTraitImage2;
    public Image PersonalityTraitImage3;
    public Image PersonalityTraitImage4;
    public Text PersonalityTraitText1;
    public Text PersonalityTraitText2;
    public Text PersonalityTraitText3;
    public Text PersonalityTraitText4;
    public Sprite 없음;
    public Sprite 나태한;
    public Sprite 성실한;
    public Sprite 쾌활한;
    public Sprite 내성적;
    public Sprite 다정한;
    public Sprite 직설적;
    public Sprite 순종적;
    public Sprite 고집센;
    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            SonData = new SonData();
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }
    void Start()
    {        
        SonData.LoadAllSonDatas();
        UpdateAllSonInfo();
        UpdateSonAttributesIndex();
    }
    public void UpdateAllSonInfo()
    {
        UpdateSonInfo();
        UpdateSonAttributes();
        UpdateSonEmotion();
        UpdateSonPersonalityTrait();
        ScheduleManager.Instance.LoadTalentAndInterestSchedule();
    }
    public void UpdateSonInfo()
    {
        NameText.text = SonData.Name;
        JobText.text = SonData.CurrentJob.ToString();
        DreamText.text = SonData.CurrentDream.ToString();
        EmotionText.text = SonData.CurrentEmotion.ToString();
    }
    public void UpdateSonAttributes()
    {
        PhysicalValue.text = ((int)(SonData.Physical * 0.01)).ToString();
        IntelligenceValue.text = ((int)(SonData.Intelligence * 0.01)).ToString();
        SensibilityValue.text = ((int)(SonData.Sensibility * 0.01)).ToString();
        CraftValue.text = ((int)(SonData.Craft * 0.01)).ToString();
        SociabilityValue.text = ((int)(SonData.Sociability * 0.01)).ToString();
        CharmValue.text = ((int)(SonData.Charm * 0.01)).ToString();
        NotionValue.text = ((int)(SonData.Notion * 0.01)).ToString();
        UpdateSonRating();
        UpdateSonBatteryAttributes();
        UpdateSonAttributesColor();
    }
    public void UpdateSonBatteryAttributes()
    {
        int sonIntimacy = Convert.ToInt32((SonData.Intimacy) * 0.005);
        switch (sonIntimacy)
        {
            case 0:
                IntimacyBattery1.SetActive(true);
                IntimacyBattery2.SetActive(false);
                IntimacyBattery3.SetActive(false);
                IntimacyBattery4.SetActive(false);
                IntimacyBattery5.SetActive(false);
                break;
            case 1:
                IntimacyBattery1.SetActive(false);
                IntimacyBattery2.SetActive(true);
                IntimacyBattery3.SetActive(false);
                IntimacyBattery4.SetActive(false);
                IntimacyBattery5.SetActive(false);
                break;
            case 2:
                IntimacyBattery1.SetActive(false);
                IntimacyBattery2.SetActive(true);
                IntimacyBattery3.SetActive(true);
                IntimacyBattery4.SetActive(false);
                IntimacyBattery5.SetActive(false);
                break;
            case 3:
                IntimacyBattery1.SetActive(false);
                IntimacyBattery2.SetActive(true);
                IntimacyBattery3.SetActive(true);
                IntimacyBattery4.SetActive(true);
                IntimacyBattery5.SetActive(false);
                break;
            case 4:
            case 5:
                IntimacyBattery1.SetActive(false);
                IntimacyBattery2.SetActive(true);
                IntimacyBattery3.SetActive(true);
                IntimacyBattery4.SetActive(true);
                IntimacyBattery5.SetActive(true);
                break;
        }

        int sonStress = Convert.ToInt32((SonData.Stress) * 0.005);
        switch (sonStress)
        {
            case 0:
                StressBattery1.SetActive(true);
                StressBattery2.SetActive(false);
                StressBattery3.SetActive(false);
                StressBattery4.SetActive(false);
                StressBattery5.SetActive(false);
                break;
            case 1:
                StressBattery1.SetActive(false);
                StressBattery2.SetActive(true);
                StressBattery3.SetActive(false);
                StressBattery4.SetActive(false);
                StressBattery5.SetActive(false);
                break;
            case 2:
                StressBattery1.SetActive(false);
                StressBattery2.SetActive(true);
                StressBattery3.SetActive(true);
                StressBattery4.SetActive(false);
                StressBattery5.SetActive(false);
                break;
            case 3:
                StressBattery1.SetActive(false);
                StressBattery2.SetActive(true);
                StressBattery3.SetActive(true);
                StressBattery4.SetActive(true);
                StressBattery5.SetActive(false);
                break;
            case 4:
            case 5:
                StressBattery1.SetActive(false);
                StressBattery2.SetActive(true);
                StressBattery3.SetActive(true);
                StressBattery4.SetActive(true);
                StressBattery5.SetActive(true);
                break;
        }

        int sonWealth = Convert.ToInt32((SonData.Wealth) * 0.005);
        switch (sonWealth)
        {
            case 1:
                WealthBattery1.SetActive(true);
                WealthBattery2.SetActive(false);
                WealthBattery3.SetActive(false);
                WealthBattery4.SetActive(false);
                WealthBattery5.SetActive(false);
                break;
            case 2:
                WealthBattery1.SetActive(false);
                WealthBattery2.SetActive(true);
                WealthBattery3.SetActive(false);
                WealthBattery4.SetActive(false);
                WealthBattery5.SetActive(false);
                break;
            case 3:
                WealthBattery1.SetActive(false);
                WealthBattery2.SetActive(true);
                WealthBattery3.SetActive(true);
                WealthBattery4.SetActive(false);
                WealthBattery5.SetActive(false);
                break;
            case 4:
                WealthBattery1.SetActive(false);
                WealthBattery2.SetActive(true);
                WealthBattery3.SetActive(true);
                WealthBattery4.SetActive(true);
                WealthBattery5.SetActive(false);
                break;
            case 5:
                WealthBattery1.SetActive(false);
                WealthBattery2.SetActive(true);
                WealthBattery3.SetActive(true);
                WealthBattery4.SetActive(true);
                WealthBattery5.SetActive(true);
                break;
        }
    }
    public void UpdateSonRating()
    {
        AthleticValue.text = (SonData.Athletic).ToString();
        EducationValue.text = (SonData.Education).ToString();
        ArtValue.text = (SonData.Art).ToString();
        TechnicValue.text = (SonData.Technic).ToString();

        // AthleticValue.text = ((int)(SonData.Athletic * 0.01)).ToString();
        // EducationValue.text = ((int)(SonData.Education * 0.01)).ToString();
        // ArtValue.text = ((int)(SonData.Art * 0.01)).ToString();
        // TechnicValue.text = ((int)(SonData.Technic * 0.01)).ToString();
    }
    public void UpdateSonEmotion()
    {
        // switch (SonData.CurrentEmotion)
        // {
        //     case Emotion.평범함:
        //         EmotionImage.sprite = Resources.Load<Sprite>("Emotions/Neutral_528px");
        //         break;
        //     case Emotion.화남:
        //         EmotionImage.sprite = Resources.Load<Sprite>("Emotions/Angry_528px");
        //         break;
        //     case Emotion.삐짐:
        //         EmotionImage.sprite = Resources.Load<Sprite>("Emotions/Question_528px");
        //         break;
        //     case Emotion.행복함:
        //         EmotionImage.sprite = Resources.Load<Sprite>("Emotions/LOL_528px");
        //         break;
        //     case Emotion.슬픔:
        //         EmotionImage.sprite = Resources.Load<Sprite>("Emotions/Sad_528px");
        //         break;
        //     case Emotion.아픔:
        //         EmotionImage.sprite = Resources.Load<Sprite>("Emotions/Vomited_528px");
        //         break;
        // }
    }
    public void UpdateSonPersonalityTrait()
    {
        string defaultTraitText = " -  -  -";
        if (SonData.CurrentPersonalityTrait1 == PersonalityTrait.없음)
        {
            PersonalityTraitText1.text = defaultTraitText;
        }
        else
        {
            PersonalityTraitText1.text = SonData.CurrentPersonalityTrait1.ToString();
        }

        if (SonData.CurrentPersonalityTrait2 == PersonalityTrait.없음)
        {
            PersonalityTraitText2.text = defaultTraitText;
        }
        else
        {
            PersonalityTraitText2.text = SonData.CurrentPersonalityTrait2.ToString();
        }

        if (SonData.CurrentPersonalityTrait3 == PersonalityTrait.없음)
        {
            PersonalityTraitText3.text = defaultTraitText;
        }
        else
        {
            PersonalityTraitText3.text = SonData.CurrentPersonalityTrait3.ToString();
        }

        if (SonData.CurrentPersonalityTrait4 == PersonalityTrait.없음)
        {
            PersonalityTraitText4.text = defaultTraitText;
        }
        else
        {
            PersonalityTraitText4.text = SonData.CurrentPersonalityTrait4.ToString();
        }
    }
    // private void UpdateSonPersonalityTraitSprite()
    // {
    //     switch (SonData.CurrentPersonalityTrait1)
    //     {
    //         case PersonalityTrait.나태한:
    //             PersonalityTraitImage1.sprite = 나태한;
    //         case PersonalityTrait.성실한:
    //             PersonalityTraitImage1.sprite = 성실한;
    //         default:
    //             PersonalityTraitImage1.sprite = 없음;
    //             break;
    //     }
    // }
    public void UpdateSonAttributesColor()
    {
        Text[] attributes = new Text[7];
        attributes[0] = PhysicalValue;
        attributes[1] = IntelligenceValue;
        attributes[2] = SensibilityValue;
        attributes[3] = CraftValue;
        attributes[4] = SociabilityValue;
        attributes[5] = CharmValue;
        attributes[6] = NotionValue;

        foreach (Text attributeText in attributes)
        {
            int attribute = (int)float.Parse(attributeText.text);
            if (attribute <= 5)
            {
                attributeText.color = Color.gray;
            }
            else if (attribute <= 10)
            {
                //attributeText.color = Color.yellow;                
                attributeText.color = new Color(0.76f, 0.52f, 0);
            }
            else if (attribute <= 15)
            {
                attributeText.color = new Color(1, 0.37f, 0);
            }
            else
            {
                attributeText.color = new Color(0.61f, 0.09f, 0.062f);
            }
        }
    }

    public void UpdateSonAttributesIndex()
    {
        //Physical
        if (SaveGame.Load<string>("PhysicalIndex") == "BigUp")
        {
            PhysicalIndexImage.sprite = BigUpSprite;
        }
        else if (SaveGame.Load<string>("PhysicalIndex") == "SmallUp")
        {
            PhysicalIndexImage.sprite = SmallUpSprite;
        }
        else if (SaveGame.Load<string>("PhysicalIndex") == "SmallDown")
        {
            PhysicalIndexImage.sprite = SmallDownSprite;
        }
        else if (SaveGame.Load<string>("PhysicalIndex") == "BigDown")
        {
            PhysicalIndexImage.sprite = BigDownSprite;
        }
        else if (SaveGame.Load<string>("PhysicalIndex") == "Unchanged")
        {
            PhysicalIndexImage.sprite = UnchangedSprite;
        }

        //Intelligence
        if (SaveGame.Load<string>("IntelligenceIndex") == "BigUp")
        {
            IntelligenceIndexImage.sprite = BigUpSprite;
        }
        else if (SaveGame.Load<string>("IntelligenceIndex") == "SmallUp")
        {
            IntelligenceIndexImage.sprite = SmallUpSprite;
        }
        else if (SaveGame.Load<string>("IntelligenceIndex") == "SmallDown")
        {
            IntelligenceIndexImage.sprite = SmallDownSprite;
        }
        else if (SaveGame.Load<string>("IntelligenceIndex") == "BigDown")
        {
            IntelligenceIndexImage.sprite = BigDownSprite;
        }
        else if (SaveGame.Load<string>("IntelligenceIndex") == "Unchanged")
        {
            IntelligenceIndexImage.sprite = UnchangedSprite;
        }

        //Sensibility
        if (SaveGame.Load<string>("SensibilityIndex") == "BigUp")
        {
            SensibilityIndexImage.sprite = BigUpSprite;
        }
        else if (SaveGame.Load<string>("SensibilityIndex") == "SmallUp")
        {
            SensibilityIndexImage.sprite = SmallUpSprite;
        }
        else if (SaveGame.Load<string>("SensibilityIndex") == "SmallDown")
        {
            SensibilityIndexImage.sprite = SmallDownSprite;
        }
        else if (SaveGame.Load<string>("SensibilityIndex") == "BigDown")
        {
            SensibilityIndexImage.sprite = BigDownSprite;
        }
        else if (SaveGame.Load<string>("SensibilityIndex") == "Unchanged")
        {
            SensibilityIndexImage.sprite = UnchangedSprite;
        }

        //Craft
        if (SaveGame.Load<string>("CraftIndex") == "BigUp")
        {
            CraftIndexImage.sprite = BigUpSprite;
        }
        else if (SaveGame.Load<string>("CraftIndex") == "SmallUp")
        {
            CraftIndexImage.sprite = SmallUpSprite;
        }
        else if (SaveGame.Load<string>("CraftIndex") == "SmallDown")
        {
            CraftIndexImage.sprite = SmallDownSprite;
        }
        else if (SaveGame.Load<string>("CraftIndex") == "BigDown")
        {
            CraftIndexImage.sprite = BigDownSprite;
        }
        else if (SaveGame.Load<string>("CraftIndex") == "Unchanged")
        {
            CraftIndexImage.sprite = UnchangedSprite;
        }

        //Sociability
        if (SaveGame.Load<string>("SociabilityIndex") == "BigUp")
        {
            SociabilityIndexImage.sprite = BigUpSprite;
        }
        else if (SaveGame.Load<string>("SociabilityIndex") == "SmallUp")
        {
            SociabilityIndexImage.sprite = SmallUpSprite;
        }
        else if (SaveGame.Load<string>("SociabilityIndex") == "SmallDown")
        {
            SociabilityIndexImage.sprite = SmallDownSprite;
        }
        else if (SaveGame.Load<string>("SociabilityIndex") == "BigDown")
        {
            SociabilityIndexImage.sprite = BigDownSprite;
        }
        else if (SaveGame.Load<string>("SociabilityIndex") == "Unchanged")
        {
            SociabilityIndexImage.sprite = UnchangedSprite;
        }

        //Charm
        if (SaveGame.Load<string>("CharmIndex") == "BigUp")
        {
            CharmIndexImage.sprite = BigUpSprite;
        }
        else if (SaveGame.Load<string>("CharmIndex") == "SmallUp")
        {
            CharmIndexImage.sprite = SmallUpSprite;
        }
        else if (SaveGame.Load<string>("CharmIndex") == "SmallDown")
        {
            CharmIndexImage.sprite = SmallDownSprite;
        }
        else if (SaveGame.Load<string>("CharmIndex") == "BigDown")
        {
            CharmIndexImage.sprite = BigDownSprite;
        }
        else if (SaveGame.Load<string>("CharmIndex") == "Unchanged")
        {
            CharmIndexImage.sprite = UnchangedSprite;
        }

        //Notion
        if (SaveGame.Load<string>("NotionIndex") == "BigUp")
        {
            NotionIndexImage.sprite = BigUpSprite;
        }
        else if (SaveGame.Load<string>("NotionIndex") == "SmallUp")
        {
            NotionIndexImage.sprite = SmallUpSprite;
        }
        else if (SaveGame.Load<string>("NotionIndex") == "SmallDown")
        {
            NotionIndexImage.sprite = SmallDownSprite;
        }
        else if (SaveGame.Load<string>("NotionIndex") == "BigDown")
        {
            NotionIndexImage.sprite = BigDownSprite;
        }
        else if (SaveGame.Load<string>("NotionIndex") == "Unchanged")
        {
            NotionIndexImage.sprite = UnchangedSprite;
        }
    }

    public void CompareLastSonAttributes()
    {
        SonData.LoadSonLastAttributes();

        SensibilityIndexImage.sprite = SmallUpSprite;
        //Physical
        if ((int)(SonData.Physical * 0.01) >= (int)((SonData.LastPhysical + 200) * 0.01))
        {
            SaveGame.Save<string>("PhysicalIndex", "BigUp");
            PhysicalIndexImage.sprite = BigUpSprite;
        }
        else if ((int)(SonData.Physical * 0.01) >= (int)((SonData.LastPhysical + 100) * 0.01))
        {
            SaveGame.Save<string>("PhysicalIndex", "SmallUp");
            PhysicalIndexImage.sprite = SmallUpSprite;
        }
        else if ((int)(SonData.Physical * 0.01) <= (int)((SonData.LastPhysical - 100) * 0.01))
        {
            SaveGame.Save<string>("PhysicalIndex", "SmallDown");
            PhysicalIndexImage.sprite = SmallDownSprite;
        }
        else if ((int)(SonData.Physical * 0.01) <= (int)((SonData.LastPhysical - 200) * 0.01))
        {
            SaveGame.Save<string>("PhysicalIndex", "BigDown");
            PhysicalIndexImage.sprite = BigDownSprite;
        }
        else
        {
            SaveGame.Save<string>("PhysicalIndex", "Unchanged");
            PhysicalIndexImage.sprite = UnchangedSprite;
        }

        //Intelligence
        if ((int)(SonData.Intelligence * 0.01) >= (int)((SonData.LastIntelligence + 200) * 0.01))
        {
            SaveGame.Save<string>("IntelligenceIndex", "BigUp");
            IntelligenceIndexImage.sprite = BigUpSprite;
        }
        else if ((int)(SonData.Intelligence * 0.01) >= (int)((SonData.LastIntelligence + 100) * 0.01))
        {
            SaveGame.Save<string>("IntelligenceIndex", "SmallUp");
            IntelligenceIndexImage.sprite = SmallUpSprite;
        }
        else if ((int)(SonData.Intelligence * 0.01) <= (int)((SonData.LastIntelligence - 100) * 0.01))
        {
            SaveGame.Save<string>("IntelligenceIndex", "SmallDown");
            IntelligenceIndexImage.sprite = SmallDownSprite;
        }
        else if ((int)(SonData.Intelligence * 0.01) <= (int)((SonData.LastIntelligence - 200) * 0.01))
        {
            SaveGame.Save<string>("IntelligenceIndex", "BigDown");
            IntelligenceIndexImage.sprite = BigDownSprite;
        }
        else
        {
            SaveGame.Save<string>("IntelligenceIndex", "Unchanged");
            IntelligenceIndexImage.sprite = UnchangedSprite;
        }

        //Sensibility
        if ((int)(SonData.Sensibility * 0.01) >= (int)((SonData.LastSensibility + 200) * 0.01))
        {
            SaveGame.Save<string>("SensibilityIndex", "BigUp");
            SensibilityIndexImage.sprite = BigUpSprite;
        }
        else if ((int)(SonData.Sensibility * 0.01) >= (int)((SonData.LastSensibility + 100) * 0.01))
        {
            SaveGame.Save<string>("SensibilityIndex", "SmallUp");
            SensibilityIndexImage.sprite = SmallUpSprite;
        }
        else if ((int)(SonData.Sensibility * 0.01) <= (int)((SonData.LastSensibility - 100) * 0.01))
        {
            SaveGame.Save<string>("SensibilityIndex", "SmallDown");
            SensibilityIndexImage.sprite = SmallDownSprite;
        }
        else if ((int)(SonData.Sensibility * 0.01) <= (int)((SonData.LastSensibility - 200) * 0.01))
        {
            SaveGame.Save<string>("SensibilityIndex", "BigDown");
            SensibilityIndexImage.sprite = BigDownSprite;
        }
        else
        {
            SaveGame.Save<string>("SensibilityIndex", "Unchanged");
            SensibilityIndexImage.sprite = UnchangedSprite;
        }

        //Craft
        if ((int)(SonData.Craft * 0.01) >= (int)((SonData.LastCraft + 200) * 0.01))
        {
            SaveGame.Save<string>("CraftIndex", "BigUp");
            CraftIndexImage.sprite = BigUpSprite;
        }
        else if ((int)(SonData.Craft * 0.01) >= (int)((SonData.LastCraft + 100) * 0.01))
        {
            SaveGame.Save<string>("CraftIndex", "SmallUp");
            CraftIndexImage.sprite = SmallUpSprite;
        }
        else if ((int)(SonData.Craft * 0.01) <= (int)((SonData.LastCraft - 100) * 0.01))
        {
            SaveGame.Save<string>("CraftIndex", "SmallDown");
            CraftIndexImage.sprite = SmallDownSprite;
        }
        else if ((int)(SonData.Craft * 0.01) <= (int)((SonData.LastCraft - 200) * 0.01))
        {
            SaveGame.Save<string>("CraftIndex", "BigDown");
            CraftIndexImage.sprite = BigDownSprite;
        }
        else
        {
            SaveGame.Save<string>("CraftIndex", "Unchanged");
            CraftIndexImage.sprite = UnchangedSprite;
        }

        //Sociability
        if ((int)(SonData.Sociability * 0.01) >= (int)((SonData.LastSociability + 200) * 0.01))
        {
            SaveGame.Save<string>("SociabilityIndex", "BigUp");
            SociabilityIndexImage.sprite = BigUpSprite;
        }
        else if ((int)(SonData.Sociability * 0.01) >= (int)((SonData.LastSociability + 100) * 0.01))
        {
            SaveGame.Save<string>("SociabilityIndex", "SmallUp");
            SociabilityIndexImage.sprite = SmallUpSprite;
        }
        else if ((int)(SonData.Sociability * 0.01) <= (int)((SonData.LastSociability - 100) * 0.01))
        {
            SaveGame.Save<string>("SociabilityIndex", "SmallDown");
            SociabilityIndexImage.sprite = SmallDownSprite;
        }
        else if ((int)(SonData.Sociability * 0.01) <= (int)((SonData.LastSociability - 200) * 0.01))
        {
            SaveGame.Save<string>("SociabilityIndex", "BigDown");
            SociabilityIndexImage.sprite = BigDownSprite;
        }
        else
        {
            SaveGame.Save<string>("SociabilityIndex", "Unchanged");
            SociabilityIndexImage.sprite = UnchangedSprite;
        }

        //Charm
        if ((int)(SonData.Charm * 0.01) >= (int)((SonData.LastCharm + 200) * 0.01))
        {
            SaveGame.Save<string>("CharmIndex", "BigUp");
            CharmIndexImage.sprite = BigUpSprite;
        }
        else if ((int)(SonData.Charm * 0.01) >= (int)((SonData.LastCharm + 100) * 0.01))
        {
            SaveGame.Save<string>("CharmIndex", "SmallUp");
            CharmIndexImage.sprite = SmallUpSprite;
        }
        else if ((int)(SonData.Charm * 0.01) <= (int)((SonData.LastCharm - 100) * 0.01))
        {
            SaveGame.Save<string>("CharmIndex", "SmallDown");
            CharmIndexImage.sprite = SmallDownSprite;
        }
        else if ((int)(SonData.Charm * 0.01) <= (int)((SonData.LastCharm - 200) * 0.01))
        {
            SaveGame.Save<string>("CharmIndex", "BigDown");
            CharmIndexImage.sprite = BigDownSprite;
        }
        else
        {
            SaveGame.Save<string>("CharmIndex", "Unchanged");
            CharmIndexImage.sprite = UnchangedSprite;
        }

        //Notion
        if ((int)(SonData.Notion * 0.01) >= (int)((SonData.LastNotion + 200) * 0.01))
        {
            SaveGame.Save<string>("NotionIndex", "BigUp");
            NotionIndexImage.sprite = BigUpSprite;
        }
        else if ((int)(SonData.Notion * 0.01) >= (int)((SonData.LastNotion + 100) * 0.01))
        {
            SaveGame.Save<string>("NotionIndex", "SmallUp");
            NotionIndexImage.sprite = SmallUpSprite;
        }
        else if ((int)(SonData.Notion * 0.01) <= (int)((SonData.LastNotion - 100) * 0.01))
        {
            SaveGame.Save<string>("NotionIndex", "SmallDown");
            NotionIndexImage.sprite = SmallDownSprite;
        }
        else if ((int)(SonData.Notion * 0.01) <= (int)((SonData.LastNotion - 200) * 0.01))
        {
            SaveGame.Save<string>("NotionIndex", "BigDown");
            NotionIndexImage.sprite = BigDownSprite;
        }
        else
        {
            SaveGame.Save<string>("NotionIndex", "Unchanged");
            NotionIndexImage.sprite = UnchangedSprite;
        }
    }

    public void SetCurruntMonth(int month)
    {
        TimeManager.Instance.SetMonth(month);
        TimeManager.Instance.UpdateCalendar();
    }
    public void SetCurruntSonJob(Job newJob)
    {
        SonData.CurrentJob = newJob;
        UpdateSonInfo();
    }
    public void 유치원직업()
    {
        SetCurruntSonJob(Job.유치원생);
    }
    public void 초등학생직업()
    {
        SetCurruntSonJob(Job.초등학생);
    }
    public void 중학생직업()
    {
        SetCurruntSonJob(Job.중학생);
    }
    public void 고등학생직업()
    {
        SetCurruntSonJob(Job.고등학생);
    }
    public void 대학생직업()
    {
        SetCurruntSonJob(Job.대학생);
    }
    public void 군인직업()
    {
        SetCurruntSonJob(Job.군인);
    }
    public void 백수직업()
    {
        SetCurruntSonJob(Job.백수);
    }
    public void 직장인직업()
    {
        SetCurruntSonJob(Job.직장인);
    }
}
