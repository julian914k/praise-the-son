﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using BayatGames.SaveGameFree;

public class CreateSonManager : MonoBehaviour
{
    [Header("Attributes")]
    public int Physical = 300;
    public int Intelligence = 300;
    public int Sensibility = 300;
    public int Craft = 300;
    public int Sociability = 300;
    public int Charm = 300;
    public int Notion = 300;

    [Header("Battery Attributes")]
    public int Intimacy = 100;
    public int Stress = 200;
    public int Wealth = 500;

    [Header("Rating Attributes")]
    public int Athletic = 1;
    public int Education = 1;
    public int Art = 1;
    public int Technic = 1;

    [Header("Text UI")]
    public Text InputParentNameText;
    public Text InputParentBirthMonthText;
    public Text InputParentBirthDayText;
    public Text InputParentJobtext;
    public Text InputSonNameText;
    public Text InputSonBirthMonthText;
    public Text InputSonBirthDayText;
    public Text AlertMessageText;
    public void LoadScene(string sceneName)
    {
        InitParentInfo();
        InitAllSonInfo();
        InitSchedules();
        DeleteAllEventQueue();
        DeleteChildhoodData();
        if (sceneName == "Room")
        {
            SaveGame.Save<string>("SonName", "테스트");
        }
        SceneManager.LoadScene(sceneName);
    }
    private void InitAllSonInfo()
    {
        InitSonInfo();
        InitSonInfoDetail();
        InitSonAttributes();
        InitSonLastAttributes();
        InitSonAttributesIndex();
        InitSonEmotion();
        InitSonPersonalityTrait();
        InitSonDate();
    }
    private void InitParentInfo()
    {
        SaveGame.Save<string>("ParentName", InputParentNameText.text);
        SaveGame.Save<int>("ParentBirthMonth", Convert.ToInt32(InputParentBirthMonthText.text));
        SaveGame.Save<int>("ParentBirthDay", Convert.ToInt32(InputParentBirthDayText.text));
        SaveGame.Save<string>("ParentJob", InputParentJobtext.text);
    }
    private void InitSonInfo()
    {
        SaveGame.Save<string>("SonName", InputSonNameText.text);
        SaveGame.Save<int>("SonBirthMonth", Convert.ToInt32(InputSonBirthMonthText.text));
        SaveGame.Save<int>("SonBirthDay", Convert.ToInt32(InputSonBirthDayText.text));
        SaveGame.Save<string>("CurrentJob", Job.유치원생.ToString());
        SaveGame.Save<string>("CurrentDream", Dream.회사원.ToString());
        SaveGame.Delete("TalentSchedules");
        SaveGame.Delete("InterestSchedules");
    }
    private void InitSonInfoDetail()
    {
        SaveGame.Save<bool>("MilitaryServiceStatus", false);
        SaveGame.Save<bool>("HasResume", false);
        SaveGame.Save<bool>("Pass", false);
        SaveGame.Delete("TestRank");
        SaveGame.Delete("EndingTitle");
        SaveGame.Delete("EndingContext");
        SaveGame.Delete("CompanyName");
    }
    private void InitSonAttributes()
    {
        SaveGame.Save<int>("Physical", Physical);
        SaveGame.Save<int>("Intelligence", Intelligence);
        SaveGame.Save<int>("Sensibility", Sensibility);
        SaveGame.Save<int>("Craft", Craft);
        SaveGame.Save<int>("Sociability", Sociability);
        SaveGame.Save<int>("Charm", Charm);
        SaveGame.Save<int>("Notion", Notion);
        SaveGame.Save<int>("Intimacy", Intimacy);
        SaveGame.Save<int>("Stress", Stress);
        SaveGame.Save<int>("Wealth", Wealth);
        SaveGame.Save<int>("Athletic", Athletic);
        SaveGame.Save<int>("Education", Education);
        SaveGame.Save<int>("Art", Art);
        SaveGame.Save<int>("Technic", Technic);
    }
    private void InitSonLastAttributes()
    {
        SaveGame.Save<int>("LastPhysical", Physical);
        SaveGame.Save<int>("LastIntelligence", Intelligence);
        SaveGame.Save<int>("LastSensibility", Sensibility);
        SaveGame.Save<int>("LastCraft", Craft);
        SaveGame.Save<int>("LastSociability", Sociability);
        SaveGame.Save<int>("LastCharm", Charm);
        SaveGame.Save<int>("LastNotion", Notion);
    }
    private void InitSonAttributesIndex()
    {
        SaveGame.Save<string>("PhysicalIndex", "Unchanged");
        SaveGame.Save<string>("IntelligenceIndex", "Unchanged");
        SaveGame.Save<string>("SensibilityIndex", "Unchanged");
        SaveGame.Save<string>("CraftIndex", "Unchanged");
        SaveGame.Save<string>("SociabilityIndex", "Unchanged");
        SaveGame.Save<string>("CharmIndex", "Unchanged");
        SaveGame.Save<string>("NotionIndex", "Unchanged");
    }
    private void InitSonEmotion()
    {
        SaveGame.Save<string>("Emotion", Emotion.평범함.ToString());
    }
    private void InitSonPersonalityTrait()
    {
        SaveGame.Save<string>("PersonalityTrait1", PersonalityTrait.없음.ToString());
        SaveGame.Save<string>("PersonalityTrait2", PersonalityTrait.없음.ToString());
        SaveGame.Save<string>("PersonalityTrait3", PersonalityTrait.없음.ToString());
        SaveGame.Save<string>("PersonalityTrait4", PersonalityTrait.없음.ToString());
    }
    private void InitSonDate()
    {
        SaveGame.Save<int>("Year", 1);
        SaveGame.Save<int>("Month", 3);
        SaveGame.Save<int>("Day", 1);
        SaveGame.Save<int>("Phase", 0);
    }
    private void InitSchedules()
    {
        SaveGame.Delete("MorningSchedule");
        SaveGame.Delete("EveningSchedule");
        SaveGame.Delete("NightSchedule");
        SaveGame.Delete("HolidaySchedule");

        SaveGame.Save<bool>("IsMorningScheduleLockedDown", false);
        SaveGame.Save<bool>("IsEveningScheduleLockedDown", false);
        SaveGame.Save<bool>("IsNightScheduleLockedDown", false);
        SaveGame.Save<bool>("IsHolidayScheduleLockedDown", false);

        SaveGame.Delete("MorningScheduleLockedDownDay");
        SaveGame.Delete("EveningScheduleLockedDownDay");
        SaveGame.Delete("NightScheduleLockedDownDay");
        SaveGame.Delete("HolidayScheduleLockedDownDay");
    }
    private void DeleteAllEventQueue()
    {
        DeleteEventQueue();
        DeleteTriggerEventQueue();
    }
    private void DeleteEventQueue()
    {
        //SaveGame.Save<List<int>>("EventQueue", null);
        SaveGame.Delete("EventQueue");
    }
    private void DeleteTriggerEventQueue()
    {
        //SaveGame.Save<List<TriggerEventData>>("TriggerEventQueue", null);
        SaveGame.Delete("TriggerEventQueue");
    }
    private void DeleteChildhoodData()
    {
        SaveGame.Delete("CurrentChildhood");

        SaveGame.Delete("BirthdayParty");
        SaveGame.Delete("TheaterRole");
        SaveGame.Delete("BirthdayPresent");
        SaveGame.Delete("FamilyPicnic");
    }
}