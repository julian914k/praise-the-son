﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using BayatGames.SaveGameFree;

public class MainMenuManager : MonoBehaviour
{
    public void LoadScene(string sceneName)
    {
        if (sceneName == "Room" && SaveGame.Exists("!SonName"))
            return;
        SceneManager.LoadScene(sceneName);
    }
}
