﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObjectManager : MonoBehaviour
{
    public bool AllowFadeAllChildrun = false;
    public bool UseFadeIn = false;
    public bool UseFadeOut = false;
    public bool UseDestroy = false;
    public bool UseDestroyTimer = false;
    public float DestroyDelayTime;
    public float MaxAlpha = 255.0f;
    public float Alpha = 255.0f;
    public float FadeInFactor = 1.0f;
    public float FadeOutFactor = 1.0f;

    private SpriteRenderer spriteRenderer;
    private MeshRenderer meshRenderer;
    private Image image;
    private Text text;
    private bool isSprite = false;
    private bool isMesh = false;
    private bool isImage = false;
    private bool isText = false;
    private float maxAlpha;
    public float alpha;

    void Start()
    {
        meshRenderer = GetComponent<MeshRenderer>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        image = GetComponent<Image>();
        text = GetComponent<Text>();

        if (spriteRenderer != null)
        {
            isSprite = true;
        }
        if (meshRenderer != null)
        {
            isMesh = true;
        }
        if (image != null)
        {
            isImage = true;
        }
        if (text != null)
        {
            isText = true;
        }

        if (UseDestroyTimer)
        {
            Destroy(DestroyDelayTime);
        }

        maxAlpha = MaxAlpha / 255;
        alpha = Alpha / 255;
    }

    void FixedUpdate()
    {
        if (AllowFadeAllChildrun)
        {
            if (UseFadeIn)
            {
                foreach (MeshRenderer meshRenderer in GetComponentsInChildren<MeshRenderer>())
                {
                    FadeIn(meshRenderer);
                }
                foreach (SpriteRenderer spriteRenderer in GetComponentsInChildren<SpriteRenderer>())
                {
                    FadeIn(spriteRenderer);
                }
                foreach (Image image in GetComponentsInChildren<Image>())
                {
                    FadeIn(image);
                }
                foreach (Text text in GetComponentsInChildren<Text>())
                {
                    FadeIn(text);
                }
            }
            if (UseFadeOut)
            {
                foreach (MeshRenderer meshRenderer in GetComponentsInChildren<MeshRenderer>())
                {
                    FadeOut(meshRenderer);
                }
                foreach (SpriteRenderer spriteRenderer in GetComponentsInChildren<SpriteRenderer>())
                {
                    FadeOut(spriteRenderer);
                }
                foreach (Image image in GetComponentsInChildren<Image>())
                {
                    FadeOut(image);
                }
                foreach (Text text in GetComponentsInChildren<Text>())
                {
                    FadeOut(text);
                }
            }
            return;
        }

        if (UseFadeIn)
        {
            if (isSprite)
                FadeIn(spriteRenderer);
            if (isMesh)
                FadeIn(meshRenderer);
            if (isImage)
                FadeIn(image);
            if (isText)
                FadeIn(text);
        }
        if (UseFadeOut)
        {
            if (isSprite)
                FadeOut(spriteRenderer);
            if (isMesh)
                FadeOut(meshRenderer);
            if (isImage)
                FadeOut(image);
            if (isText)
                FadeOut(text);
        }
    }

    private void FadeIn(SpriteRenderer spriteRenderer)
    {
        if (alpha < maxAlpha)
        {
            Color color = spriteRenderer.color;
            alpha += (FadeInFactor * Time.deltaTime);
            color.a = alpha;
            spriteRenderer.color = color;
        }
    }

    private void FadeIn(MeshRenderer meshRenderer)
    {
        if (alpha < maxAlpha)
        {
            Color color = meshRenderer.material.GetColor("_Color");
            alpha += (FadeInFactor * Time.deltaTime);
            color.a = alpha;
            meshRenderer.material.SetColor("_Color", color);
        }
    }

    private void FadeIn(Image image)
    {
        if (alpha < maxAlpha)
        {
            Color color = image.color;
            alpha += (FadeInFactor * Time.deltaTime);
            color.a = alpha;
            image.color = color;
        }
    }

    private void FadeIn(Text text)
    {
        if (alpha < maxAlpha)
        {
            Color color = text.color;
            alpha += (FadeInFactor * Time.deltaTime);
            color.a = alpha;
            text.color = color;
        }
    }

    private void FadeOut(SpriteRenderer spriteRenderer)
    {
        if (alpha > 0)
        {
            Color color = spriteRenderer.color;
            alpha -= (FadeOutFactor * Time.deltaTime);
            color.a = alpha;
            spriteRenderer.color = color;
        }
        else if (UseDestroy)
        {
            Destroy();
        }
    }

    private void FadeOut(MeshRenderer meshRenderer)
    {
        if (alpha > 0)
        {
            Color color = meshRenderer.material.GetColor("_Color");
            alpha -= (FadeOutFactor * Time.deltaTime);
            color.a = alpha;
            meshRenderer.material.SetColor("_Color", color);
        }
        else if (UseDestroy)
        {
            Destroy();
        }
    }

    private void FadeOut(Image image)
    {
        if (alpha > 0)
        {
            Color color = image.color;
            alpha -= (FadeOutFactor * Time.deltaTime);
            color.a = alpha;
            image.color = color;
        }
        else if (UseDestroy)
        {
            Destroy();
        }
    }

    private void FadeOut(Text text)
    {
        if (alpha > 0)
        {
            Color color = text.color;
            alpha -= (FadeOutFactor * Time.deltaTime);
            color.a = alpha;
            text.color = color;
        }
        else if (UseDestroy)
        {
            Destroy();
        }
    }

    private void Destroy()
    {
        GameObject.Destroy(gameObject);
    }

    private void Destroy(float destroyDelayTime)
    {
        if (UseDestroyTimer)
        {
            GameObject.Destroy(gameObject, destroyDelayTime);
        }
    }
}
